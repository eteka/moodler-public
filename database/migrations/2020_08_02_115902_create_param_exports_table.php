<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateParamExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('param_exports', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('niv_universite')->nullable();
            $table->string('niv_entite')->nullable();
            $table->string('niv_cylce')->nullable();
            $table->string('niv_annee')->nullable();
            $table->string('niv_filiere')->nullable();
            $table->string('niv_semestre')->nullable();
            $table->string('niv_cours')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('param_exports');
    }
}
