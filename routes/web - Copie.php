<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
#Aministration
    Route::middleware(['auth'])->prefix('moodle')->group(function () {
        
        Route::get('/', 'admin\AdminController@index')->name('dashbord');

        #Configuration
        Route::get('/welcome', 'admin\AdminController@welcome')->name('welcome');
        Route::get('/configuration', 'moodler\ConfigMoodlerController@structure')->name('structure');
        Route::post('/configuration', 'moodler\ConfigMoodlerController@saveStructure')->name('saveStructure');
        #Route::get('/config/service', 'moodler\ConfigMoodlerController@configService')->name('configService');
        Route::get('/configuration/bdd', 'moodler\ConfigMoodlerController@configBdd')->name('configBdd');
        Route::post('/configuration/bdd', 'moodler\ConfigMoodlerController@saveBdd')->name('saveBdd');
        Route::get('/configuration/preferences', 'moodler\ConfigMoodlerController@preferences')->name('preferences');
        Route::post('/configuration/preferences', 'moodler\ConfigMoodlerController@savePreferences')->name('savePreferences');
        #Structure
        #Route::get('/structure/{id}', 'admin\AdminController@getStructure')->name('getStructure');
        #Profil
        Route::get('/profil', 'admin\ProfilController@profil')->name('profil');
        Route::get('/profil/edit', 'admin\ProfilController@editProfil')->name('editProfil');
        Route::get('/profil/activity', 'admin\ProfilController@activites')->name('activites');
        Route::get('/profil/settings', 'admin\ProfilController@parametres')->name('parametres');

        #Paramètres

        #Documentation
        Route::get('/documentation', 'admin\ProfilController@documentation')->name('documentation');

        #Export
        Route::get('/export', 'admin\ExportsController@export')->name('export');
        Route::get('/pdf', 'admin\ExportsController@pdf')->name('pdf');




    });

Route::resource('admin/posts', 'Admin\\PostsController');
Route::resource('moodler/config_structure', 'moodler\\config_structureController');
Route::resource('moodler/config-moodler', 'moodler\\ConfigMoodlerController');
Route::resource('t-p', 'TPController');
Route::resource('t-p', 'TPController');
Route::resource('moodler/parametre', 'moodler\\ParametreController');
Route::resource('moodler/parametre', 'moodler\\ParametreController');