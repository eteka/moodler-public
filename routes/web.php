<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
#Auth::routes();
Route::get('file/{file_name}', function($file_name = null)
{
    $pathToTheFile=storage_path('app/public/'.$file_name);
    $fileContents = Storage::disk('public')->get($pathToTheFile);
    $response = Response::make($fileContents, 200);
    $response->header('Content-Type', Storage::disk('public')->mimeType($pathToTheFile));
    return $response;
    // $path = storage_path().'/app/public/'.$file_name;
    // //dd($path);
    // if (file_exists($path)) {
    // return Response::download($path);
    // }
});
Route::get('/roles', function () {

    $role = Role::create(['name' => 'admin']);
/*
$role = Role::create(['name' => 'admin']);
$permission = Permission::create(['name' => 'create configuration']);
$role->givePermissionTo($permission);
$permission = Permission::create(['name' => 'edit configuration']);
$role->givePermissionTo($permission);
$permission = Permission::create(['name' => 'create rhebdo']);
$role->givePermissionTo($permission);
$permission = Permission::create(['name' => 'create rmensuel']);
$role->givePermissionTo($permission);
$permission = Permission::create(['name' => 'create rsemestre']);
$role->givePermissionTo($permission);
$permission = Permission::create(['name' => 'valide raport']);
$role->givePermissionTo($permission);
$permission = Permission::create(['name' => 'create user']);
$role->givePermissionTo($permission);
$permission = Permission::create(['name' => 'edit user']);
$role->givePermissionTo($permission);
$permission = Permission::create(['name' => 'delete user']);
$role->givePermissionTo($permission);
$permission = Permission::create(['name' => 'validate user']);*/

});


Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->prefix('moodlers')->group(function () {
    Route::get('/', 'moodler\ConfigMoodlerController@index')->name('dashbord2');

});
Route::get('language/{lang}', 'moodler\PageController@setLang')->name('setLang');
#Aministration
Route::middleware(['auth'])->prefix('moodle')->group(function () {




        /*Route::get('/rapport-mensuel/',function(){
            return view('moodler.rapport.rapport-mensuel');
        })->name('rapport-mensuel.create');*/

    });
/*
Route::resource('admin/posts', 'Admin\\PostsContoroller');
Route::resource('moodler/config_structure', 'moodler\\config_structureController');
Route::resource('moodler/config-moodler', 'moodler\\ConfigMoodlerController');
Route::resource('t-p', 'TPController');
Route::resource('t-p', 'TPController');
Route::resource('moodler/parametre', 'moodler\\ParametreController');
Route::resource('moodler/parametre', 'moodler\\ParametreController');

Route::resource('moodler/universite', 'moodler\\UniversiteController');
Route::resource('moodler/entite', 'moodler\\EntiteController');
Route::resource('moodler/cycle', 'moodler\\CycleController');
Route::resource('moodler/annee_academique', 'moodler\\annee_academiqueController');
Route::resource('moodler/filiere', 'moodler\\filiereController');
Route::resource('moodler/rapport', 'moodler\\RapportController');

Route::resource('moodler/avis', 'moodler\\AvisController');
Route::resource('moodler/preferences', 'moodler\\PreferencesController');
