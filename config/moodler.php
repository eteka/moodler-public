
<?php

return [
        'NIVEAU_UNIVERSITE' => '1',
        'NIVEAU_ENTITE' => '2',
        'NIVEAU_CYCLE' => '3',
        'NIVEAU_ANNEE_ACADEMIQUE' => '4',
        'NIVEAU_FILIERE' => '5',
        'NIVEAU_SEMESTRE' => '6',
        'NIVEAU_COURS' => '7',
    
];