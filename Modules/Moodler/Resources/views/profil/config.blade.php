@extends('moodler::layouts.app')

@section('title', __('Préférences') . '-' . $user->fullname())

@section('content')

    <div class="main">
        <div class="slim-pageheader">
            <ol class="breadcrumb slim-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Accueil') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('getProfil') }}">{{ __('Mon profil') }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ __('Préférences') }}</li>
            </ol>
            <h6 class="slim-pagetitle">{{ __('Préférences') }}</h6>
        </div><!-- slim-pageheader -->

        <div class="row row-sm">
            <div class="col-lg-8">
                <div class="card hidden card-profile">
                    <div class="card-body">
                        <div class="clearfix">

                        </div>
                        <div class="media">
                            <img class="user-photo-profil" src="{{ asset($user->photoProfil()) }}"
                                alt="{{ $user->fullname() }}">
                            <div class="media-body">
                                <h3 class="card-profile-name mb-0">{{ $user->fullname(1) }}</h3>
                                <ul class="list-inline text-small small float-right ">
                                    <li class="list-inline-item"><a class="text-muted" href="{{ route('editProfil') }}"><i
                                                class="fa fa-edit"></i> {{ __('Modifier mon profil') }}</a></li>
                                    <!--li class="list-inline-item"><a class="text-muted" href="#"><i class="fa fa-cog"></i> {{ __('Paramètres du compte') }}</a></li-->
                                </ul>
                                <div class="card-profile-position text-muted">
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            {{ $user->poste }}
                                            @if ($user->structure)
                                                {{ __('à') }} <a
                                                    href="{{ route('structure', $user->structure->id) }}">{{ $user->structure->nom }}</a>

                                                <p>{{ $user->structure->localisation }}</p>
                                            @endif
                                        </li>

                                        <li class="list-inline-item">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                                 {{'Inscrit, '}}{{ $user->created_at->diffforhumans() }}
                                        </li>

                                    </ul>
                                </div>

                                <p class="mg-b-0 text-italic">
                                <blockquote class="bold text-italic">

                                    @if ($user->apropos)
                                        {{ $user->apropos }}

                                        <!--a href="#">Afficher</a-->
                                    @endif
                                </blockquote>
                                </p>
                            </div><!-- media-body -->
                        </div><!-- media -->
                    </div><!-- card-body -->
                </div>
                <div class="card mt-3 border-darken-1 card-profile">
                    <div class="card card-body bg-white border-light">
                        <h2 class="h5 mb-4">{{ __('Préférences') }}</h2>
                        <ul class="list-group list-group-flush">
                             <li
                                class="list-group-item d-flex align-items-center justify-content-between px-0 border-bottom">
                                <div>
                                    <h3 class="h6 mb-1">{{ __('Activités du compte') }}</h3>
                                    <p class="small pr-4">{{ __('Enrégistrer et visualiser vos activités sur votre compte') }}
                                    </p>
                                </div>
                                <div>
                                    <div class="custom-control custom-switch"><input type="checkbox"
                                            class="custom-control-input" id="user-notification-2"> <label
                                            class="custom-control-label" for="user-notification-2"></label></div>
                                </div>
                            </li>
                            <li
                                class="list-group-item d-flex align-items-center justify-content-between px-0 border-bottom">
                                <div>
                                    <h3 class="h6 mb-1">{{ __('Nombre d\'élément à affiche') }}</h3>
                                    <p class="small pr-4">{{ __('Nombre entier d\'élement à afficher par page') }}</p>
                                </div>
                                <div>
                                    <div class="custom-controls">
                                        <input type="number" value="10" class="form-control form-control-sm" size="3">
                                        </div>
                                </div>
                            </li>

                                    <li
                                class="list-group-item d-flex align-items-center justify-content-between px-0 border-bottom">
                                <div>
                                    <h3 class="h6 mb-1">{{ __('Format d\'exportation par défault') }}</h3>
                                    <p class="small pr-4">{{ __('Choisir le format d\'exportation par défault') }}</p>
                                </div>
                                <div>
                                    <div class="custom-controls">
                                        <select name="format_default" id="format_default"class="custom-select custom-select-sm">
                                            <option value="pdf">{{ __('PDF') }}</option>
                                            <option value="word">{{ __('Word') }}</option>
                                            <option value="excel">{{ __('Excel') }}</option>
                                        </select>
                                        </div>
                                </div>
                            </li>

                            <li
                                class="list-group-item d-flex align-items-center justify-content-between px-0 border-bottom">
                                <div>
                                    <h3 class="h6 mb-1">{{ __('Langue par défaut') }}</h3>
                                    <p class="small pr-4">{{ __('Langue à utiliser quand vous vous connectez à l\'application') }}</p>
                                </div>
                                <div>
                                    <div class="custom-controls">
                                        <select name="lang_default" id="lang_default"class="custom-select custom-select-sm">
                                            <option value="fr_FR">{{ __('Français') }}</option>
                                            <option value="en_En">{{ __('Anglais') }}</option>
                                        </select>
                                        </div>
                                </div>
                            </li>


                        </ul>
                         <div class="row mt-3">
                             <div class="col-2">
                                 <input type="submit" value="Enrégistrer" class="btn btn-primary">
                             </div>
                         </div>
                    </div>

                </div><!-- card -->



            </div><!-- col-8 -->

            <div class="col-lg-4 mg-t-20 mg-lg-t-0">


            </div><!-- col-4 -->
        </div><!-- row -->

    </div>
@endsection
