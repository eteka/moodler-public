@extends('moodler::layouts.app')

@section('title',__("Changer de mot de passe")."-". $user->fullname())

@section('content')
<div class="main">
<div class="slim-pageheader">
    <ol class="breadcrumb slim-breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fa fa-home"></i> {{__('Tableau de bord')}}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('getProfil') }}">{{__('Mon profil')}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{__('Changer de mot de passe')}}</li>
    </ol>
    <h6 class="slim-pagetitle">{{ __('Changer de mot de passe') }}</h6>
</div>
<div class="card-column row mg-t-20">
           <div class="col-md-7">
            <div class="card mb-12 border shadow-sm">
                <div class="card-header bg-light page-header bold text-bold">{{ __("Veuillez renseigner l'ancien et le nouveau mot de passe") }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('change.password') }}">
                        @csrf
                        @if($errors->all())
                        <ul>
                        <div class="alert alert-danger">
                         @foreach ($errors->all() as $error)
                            <li class="text-white ml-4">{{ $error }}</li>
                         @endforeach
                        </ul>
                        </div>
                         @endif
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe actuel') }}</label>
                            <div class="col-md-6">
                                <input id="password" required type="password" class="form-control" name="current_password" autocomplete="current-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Nouveau de passe') }}</label>
                            <div class="col-md-6">
                                <input id="new_password" required type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Confirmer le nouveau de passe') }}</label>
                            <div class="col-md-6">
                                <input id="new_confirm_password" required type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Changer mot de passe') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
