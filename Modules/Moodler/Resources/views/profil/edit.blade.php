@extends('moodler::layouts.app')

@section('title',__("Editer mon compte"). $user->fullname())

@section('content')

<div class="main">
        <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">{{__('Accueil')}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{__('Editer mon compte')}}</li>
          </ol>
          <h6 class="slim-pagetitle">{{ __('Editer mon compte') }}</h6>
        </div><!-- slim-pageheader -->

        <div class="row row-sm">
          <div class="col-lg-8">
<div class="card card-profile ">
              <div class="card-body">
                <div class="clearfix">

                </div>
                <div class="media ">
                  <img class="user-photo-profil bg-white" src="{{ asset($user->photoProfil()) }}" alt="{{ $user->fullname() }}">
                  <div class="media-body">
                    <h3 class="card-profile-name mb-0">{{ $user->fullname(1) }}</h3>
                    <ul class="list-inline text-small small float-right ">
                      <li class="list-inline-item hidden"><a class="text-muted" href="{{ route('editProfil') }}"><i class="fa fa-edit"></i> {{ __('Modifier mon profil') }}</a></li>
                      <!--li class="list-inline-item"><a class="text-muted" href="#"><i class="fa fa-cog"></i> {{ __('Paramètres du compte') }}</a></li-->
                  </ul>
                      <div class="card-profile-position text-muted">
                        <ul class="list-inline">
                         <li class="list-inline-item">
                                {{ $user->poste }}
                         </li>

                         @if($user->structure)
                                <li class="list-inline-item">
                                    {{__('à')}} <a href="{{route('structure',$user->structure->id)}}">{{$user->structure->nom}}</a>

                                    <p>{{$user->structure->localisation}}</p>
                                </li>
                                <li>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li>
                         @endif
                            <li class="list-inline-item">
                                 {{'Inscrit, '}}{{ $user->created_at->diffforhumans() }}
                            </li>

                        </ul>
                    </div>

                    <p class="mg-b-0 text-italic">
                       <blockquote class="bold text-italic">

                        @if($user->apropos)
                        {{$user->apropos}}

                        <!--a href="#">Afficher</a-->
                        @endif
                         </blockquote>
                    </p>
                  </div><!-- media-body -->
                </div><!-- media -->
              </div><!-- card-body -->
            </div>
            <div class="card mt-3 border-darken-1 card-profile">
              <div class="card-footer">
                   @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    <form method="POST" action="{{ route('updateProfil') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                            <h4 class="text-gray-600 h6 bold py-2"><i class="fa fa-edit"></i> {{ __("Modification de mon profil") }}</h4>
                    <table class="table table-md">
                        <tr>
                            <th>{{ __('PHOTO :') }}</th>
                            <td>
                                <input type="file" name="photo" class="form-control">
                                </td>
                        </tr>
                        <tr>
                            <th>{{ __('NOM :') }}</th>
                            <td>
                                <input type="text" value="{{ $user->nom }}" name="nom" class="form-control">
                                </td>
                        </tr>
                        <tr>
                            <th>{{ __('PRÉNOMS :') }}</th>

                                <td>
                                <input type="text" value="{{ $user->prenom }}" name="prenom" class="form-control">
                                </td>
                        </tr>
                        <!--tr>
                            <th>{{ __('EMAIL :') }}</th>
                             <td>
                                <input type="text" value="{{ $user->email }}" name="email" class="form-control">
                                </td>
                        </tr-->
                        <tr>
                            <th>
                                <label for="sexe">{{ __('SEXE :') }}</label></th>
                             <td>
                                <label for="sexe_m"><input @if($user->sexe=="M") checked @endif type="radio" id="sexe_m" value="M" name="sexe" > {{ __('Masculin') }}</label>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<label for="sexe_f"><input  @if($user->sexe!="M") checked @endif type="radio" id="sexe_f" value="M" name="sexe" > {{ __('Féminin') }}</label>

                            </td>
                        </tr>
                        <tr>
                            <th>{{ __('TÉLÉPHONE :') }}</th>
                            <td>
                                <input type="tel" value="{{ $user->telephone }}" name="tel" class="form-control">
                                </td>
                        </tr>
                        <tr>
                            <th>{{ __('POSTE :') }}</th>
                            <td>
                                <input type="text" value="{{ $user->poste }}" name="poste" class="form-control">
                                </td>
                        </tr>
                        <tr>
                            <th>{{ __('A PROPOS :') }}</th>
                            <td>
                                <textarea name="about" id="about"  class="form-control" rows="2">{{ $user->apropos }}</textarea>
                                </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="submit" class="btn btn-behance" value="Mettre à jour">
                            </td>
                        </tr>

                    </table>
               </form>
                <div>

                </div>
              </div><!-- card-footer -->
            </div><!-- card -->


            <div class="card card-latest-activity mg-t-20">
              <div class="card-body">

              </div><!-- card-body -->

            </div>
          </div><!-- col-8 -->

          <div class="col-lg-4 mg-t-20 mg-lg-t-0">


          </div><!-- col-4 -->
        </div><!-- row -->

      </div>
@endsection
