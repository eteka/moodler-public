@extends('moodler::layouts.app')

@section('title',__("Profil - "). $user->fullname())

@section('content')
<style>
    /*.card-profile{
        min-height: 200px;
    }
    .user-photo-profil{
        border: 1px solid #eeeeee;
        height: 150px;
        padding: 15px;
        border-radius: 15px;
        margin: 0 15px;
        box-shadow: 0 0 5px #dedede;
    }*/
</style>
<div class="main">
        <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">{{__('Accueil')}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{__('Mon profil')}}</li>
          </ol>
          <h6 class="slim-pagetitle">{{ __('Mon compte') }}</h6>
        </div><!-- slim-pageheader -->

        <div class="row row-sm">
          <div class="col-lg-8">
            <div class="card card-profile">
              <div class="card-body">
                <div class="clearfix">

                </div>
                <div class="media">
                  <img class="user-photo-profil" src="{{ asset($user->photoProfil()) }}" alt="{{ $user->fullname() }}">
                  <div class="media-body">
                    <h3 class="card-profile-name mb-0">{{ $user->fullname(1) }}</h3>
                    <ul class="list-inline text-small small float-right ">
                      <li class="list-inline-item"><a class="text-muted" href="{{ route('editProfil') }}"><i class="fa fa-edit"></i> {{ __('Modifier mon profil') }}</a></li>
                      <!--li class="list-inline-item"><a class="text-muted" href="#"><i class="fa fa-cog"></i> {{ __('Paramètres du compte') }}</a></li-->
                  </ul>
                      <div class="card-profile-position text-muted">
                        <ul class="list-inline">
                         <li class="list-inline-item">
                                {{ $user->poste }}
                         </li>

                         @if($user->structure)
                                <li class="list-inline-item">
                                    {{__('à')}} <a href="{{route('structure',$user->structure->id)}}">{{$user->structure->nom}}</a>

                                    <p>{{$user->structure->localisation}}</p>
                                </li>
                                <li>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li>
                         @endif
                            <li class="list-inline-item">
                                 {{'Inscrit, '}}{{ $user->created_at->diffforhumans() }}
                            </li>

                        </ul>
                    </div>

                    <p class="mg-b-0 text-italic">
                       <blockquote class="bold text-italic">

                        @if($user->apropos)
                        {{$user->apropos}}

                        <!--a href="#">Afficher</a-->
                        @endif
                         </blockquote>
                    </p>
                  </div><!-- media-body -->
                </div><!-- media -->
              </div><!-- card-body -->
            </div>
          <div class="card mt-3 border-darken-1 card-profile">
              <div class="card-body_">
              <div class="card-footer">
                <h3 class="h6 text-muted mb-4 font-weight-normal"><i class="fa fa-info-circle"></i> {{ __('Information sur mon compte') }}</h3>
               <table class="table table-md">
                   <tr>
                       <th>{{ __('NOM :') }}</th>
                       <td>{{ $user->nom }}</td>
                   </tr>
                   <tr>
                       <th>{{ __('PRÉNOMS :') }}</th>
                       <td>{{ $user->prenom }}</td>
                   </tr>
                   <tr>
                       <th>{{ __('EMAIL :') }}</th>
                       <td>{{ $user->email }}</td>
                   </tr>
                   <tr>
                       <th>{{ __('SEXE :') }}</th>
                       <td>{{ $user->prenom }}</td>
                   </tr>
                   <tr>
                       <th>{{ __('TÉLÉPHONE :') }}</th>
                       <td>{{ $user->telephone }}</td>
                   </tr>
                   <tr>
                       <th>{{ __('POSTE :') }}</th>
                       <td>{{ $user->poste }}</td>
                   </tr>
                    <tr>
                       <th>{{ __('MOT DE PASSE :') }}</th>
                       <td>
                            <a class="btn btn-outline-primary btn-xs" href="{{ route('change.password') }}"><i class="fa fa-lock"></i> {{ __("Changer mon mot de passe") }}</a>
                        </td>
                   </tr>
                   <tr>
                       <th></th>
                       <td></td>
                   </tr>

               </table>
                <div>

                </div>
              </div><!-- card-footer -->
            </div><!-- card -->


            <div class="card card-latest-activity mg-t-20">
              <div class="card-body">

              </div><!-- card-body -->

            </div>
          </div><!-- col-8 -->

          <div class="col-lg-4 mg-t-20 mg-lg-t-0">


          </div><!-- col-4 -->
        </div><!-- row -->

      </div>
@endsection
