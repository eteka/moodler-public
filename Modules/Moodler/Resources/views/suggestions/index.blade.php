@extends('moodler::layouts.app')
@section('title', __("Suggestions envoyées"))

@section('content')
    <div class="bg-soft h-100vh">
        @include('moodler::include.top')
        <section class="">
            <main class="">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                    <div class="d-block mb-4 mb-md-0">
                        <h2 class="h3">{{ __("Suggestions") }}</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 mb-4">
                        <div class="card p-0 card-body border-light shadow-sm table-wrapper table-responsive pt-0">
                    @isset($suggestions)
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th  class="border-0">#</th>
                                <th  class="border-0">{{ __('Auteur') }}</th>
                                <th class="border-0">{{ __('Message') }}</th>
                                <th class="border-0 col-2">{{ __('Date') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                             @if($suggestions->count()==0)
                                <tr>
                                    <td colspan="6">
                                        <div class="well text-center bg-light rounded p-3">
                                            {{ __('Aucun élément à afficher') }}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @php
                                $i=1;
                            @endphp
                            @foreach ($suggestions as $s)

                            <tr>
                                <td width="15px">{{ $i++ }}</td>
                                <td class="col-3">
                                    <span class="text-bold">{{ $s->nom.' '.$s->prenom }}</span>
                                     <div class="text-muted small ">
                                         {{ $s->email }} <br>
                                          {{ $s->tel }}
                                    </div>
                                </td>
                                <td>
                                 {{ $s->message }}
                                </td>
                               <td class="small">
                                   {{ date('d/m/Y à H\H i\m\i\n',strtotime($s->created_at)) }}
                                   </td>


                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                    <hr>
                    {{ $suggestions->appends(request()->query())->links() }}
                     @endif
                </div>
                    </div>
                </div>

            </main>
        </section>
    </div>
@endsection
