@extends('moodler::layouts.app')
@section('title', 'Mes rapports')

@section('content')
    <div class="bg-soft">
        @include('moodler::include.top')
        <section class="bg-soft min-vh-100">
            <main class="">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                    <div class="d-block mb-4 mb-md-0">
                        <h2 class="h3">{{ __('Mes rapports') }}</h2>
                    </div>
                </div>
                <div class="table-settings mb-4">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-12 col-md-6 col-lg-3 align-items-center d-none d-md-flex">
                            <div class="form-group mb-0 mr-3">
                                <form action="{{ url()->full() }}" method="GET">
                                    @php
                                        $get_url=isset($_GET['type_rapport'])?$_GET['type_rapport']:'';
                                    @endphp
                                <select onchange="this.form.submit()" name="type_rapport" class="custom-select"
                                    id="inlineFormCustomSelectMesages">
                                    <option {{ $get_url==''?"selected='selected'":'' }} value="">Tout</option>
                                    <option {{ $get_url=='1'?"selected='selected'":'' }} value="1">{{ __("Rapport hebdomadaire") }}</option>
                                    <option {{ $get_url=='2'?"selected='selected'":'' }} value="2">{{ __("Rapport mensuel") }}</option>
                                    <option {{ $get_url=='3'?"selected='selected'":'' }} value="3">{{ __("Rapport par semestre") }}</option>
                                </select></div>
                                </form>
                            <!--div><a href="#" class="btn btn-md btn-white border-light">{{ __("Valider") }}</a></div-->
                        </div>
                        <div class="col col-md-4 col-lg-3 col-xl-2 ml-auto">
                            <form action="{{ url()->full() }}" method="GET">
                            <div class="input-group">

                                <div class="input-group-prepend"><span class="input-group-text"><span
                                            class="fas fa-search"></span></span></div><input name="q" class="form-control"
                                    id="searchInputdashboard1" placeholder="{{ __('Rechercher') }}" type="text"
                                    aria-label="Dashboard user search">

                            </div>
                        </form>
                        </div>
                        <div class="col-3 pl-0 d-none d-lg-flex">
                            <form action="{{ url()->full() }}" method="GET">
                            <div class="form-group mb-0">
                                <select onchange="this.form.submit()"  name="format" class="custom-select" id="inlineFormCustomSelectMesages2">
                                    <option selected="selected" value="tout">Tout format</option>
                                    <option value="pdf">PDF</option>
                                    <option value="excel">Excel</option>
                                    <option value="word">Word</option>
                                </select>

                            </div>
                            </form>
                            <form action="{{ url()->full() }}" method="GET">

                            <div class="form-group mb-0 ml-2 mr-0">
                                <select name="nb" onchange="this.form.submit()"  class="custom-select ml-2" id="">
                                    <option {{ (isset($nb) && $nb==0)?"selected='selected'":'' }}>Afficher</option>
                                    <option  {{ (isset($nb) && $nb==5)?"selected='selected'":'' }} value="5">5</option>
                                    <option  {{ (isset($nb) && $nb==10)?"selected='selected'":'' }} value="10">10</option>
                                    <option  {{ (isset($nb) && $nb==15)?"selected='selected'":'' }} value="15">15</option>
                                    <option  {{ (isset($nb) && $nb==20)?"selected='selected'":'' }} value="10">20</option>
                                    <option  {{ (isset($nb) && $nb==25)?"selected='selected'":'' }} value="25">25</option>
                                </select>

                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card card-body border-light shadow-sm table-wrapper table-responsive pt-0">
                    @isset($rapports)
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th colspan="2" class="border-0">{{ __('Nom du rapport') }}</th>
                                <th class="border-0">{{ __('Type de rapport') }}</th>
                                <th class="border-0">{{ __('Date début') }}</th>
                                <th class="border-0">{{ __('Date fin') }}</th>
                                <th class="border-0">{{ __('Etat') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                             @if($rapports->count()==0)
                                <tr>
                                    <td colspan="6">
                                        <div class="well text-center bg-light rounded p-3">
                                            {{ __('Aucun élément à afficher') }}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @foreach ($rapports as $r)

                            <tr>
                                <td>
                                    <img src="{{ $r->Icone() }}"
                                            class="user-avatar rounded-sm pull-left mr-3" alt="{{ $r->nom }}">
                                </td>
                                <td class="col-5">
                                    <!--a href="#" class="d-flex align-items-center"-->
                                        <div class="d-block">


                                            <div class="small text-black-50">{{ $r->dateAjout()}}</div>
                                            <span class="font-weight-bold text-gray-800">{{ $r->nom }}</span>
                                        </div>
                                        <a class="btn btn-success btn-xs" target="_blanck" href="{{ asset($r->fichier) }}"><i class="fa fa-save"></i> {{ __('Télécharger') }}</a>
                                    <!--/a-->
                                </td>
                                <td><span class="font-weight-normal text-gray-500 text-uppercase small">{{ $r->Type()}}</span></td>
                               <td><span class="font-weight-normal text-gray-500">{{ $r->dateDebut() }}</span></td>
                                <td nowrap><span class="font-weight-normal text-uppercase text-gray-500">{{ $r->dateDebut() }}</span></td>

                                 <td><span class="font-weight-normal small ">
                                    {{ $r->Etat() }}
                                 </td>

                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                    <hr>
                    {{ $rapports->appends(request()->query())->links() }}
                     @endif
                    <!--div class="card-footer px-3 d-flex align-items-center justify-content-between">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination mb-0">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>
                        <div class="font-weight-bold small">Showing <b>5</b> out of <b>25</b> entries</div>
                    </div-->
                </div>

            </main>
        </section>
    </div>
@endsection
