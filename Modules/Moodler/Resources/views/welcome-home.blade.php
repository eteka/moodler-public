@extends('layouts.app')

@section('title',"Bienvenue")

@section('content')
<div class="main">
    <div class="slim-pageheader">
          
          
        </div><!-- slim-pageheader -->
        <div class="row">
          <div class="col-lg-6">
            <h2 class="tx-inverse text-bold mg-b-15">{{__('Bienvenue, :name !',['name'=>Auth::user()->prenom])}}</h2>
            <p class="text-bold bold">
                {{__('welcome-info')}}
            </p>
           
            <p class="">{!!__('config-info')!!}</p>
            <p class="text-bold bold ">
                {!!__('config-info2')!!}
            </p>
            <div class="alert alert-primary mg-b-40">
             {!!__('config-info3')!!}
</div>
            <hr>
            <a href="{{route('structure')}}" class="btn btn-lg btn btn-primary btn-lg btn-block mg-b-10 text-uppercase rond3   mg-b-10">
            <i class="fa fa-cogs"></i>    
            {{__('Configurer maintenant')}}</a>
            
          </div><!-- col-6 -->
          <div class="col-lg-6 mg-t-20 mg-sm-t-30 mg-lg-t-0">
            <div class="card card-dash-headline">
              <h4>{{__('intro-faq-titre')}}</h4>
              <p>{{__('intro-faq-info')}}</p>
              <div class="row row-sm">
                <div class="col-sm-6">
                  <a href="{{route('editProfil')}}" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> {{__('Editer mon compte')}}</a>
                </div><!-- col-6 -->
                <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                  <a href="{{route('documentation')}}" class="btn btn-teal  btn-block"><i class="fa fa-docs"></i>{{__('La documentation')}}</a>
                </div><!-- col-6 -->
              </div><!-- row -->
            </div><!-- card -->
          </div><!-- col-6 -->
        </div>
        
</div>
@endsection