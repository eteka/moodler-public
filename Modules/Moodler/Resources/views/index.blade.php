@extends('moodler::layouts.app')
@section('content')
<div class="bg-soft">
   @include('moodler::include.top')
   <div class="row justify-content-md-center">
      <div class="col-12 col-sm-6 col-xl-4 mb-4">
         <div class="card border-light shadow-sm">
            <div class="card-body">
               <div class="row d-block d-xl-flex align-items-center">
                  <div class="col-12 col-xl-4 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                     <div class="icon icon-shape icon-md icon-shape-slack rounded mr-4 mr-sm-0"><span class="fas fa-calendar-week"></span></div>
                     <div class="d-sm-none">
                        <h2 class="h5">{{ __("Rapport Hebdomadaire") }}</h2>
                        <h3 class="mb-1">345,678</h3>
                     </div>
                  </div>
                  <div class="col-12 col-xl-8 px-xl-0">
                     <div class="d-none d-sm-block">
                        <h2 class="h5">{{ __("Rapport Hebdomadaire") }}</h2>
                        <h3 class="mb-1">{{ isset($count_hebdo)?$count_hebdo:0 }}</h3>
                     </div>
                     <small> <!--<span class="fas fa-calendar"></span> Fév 1 - Avr. 1, 2020<br>-->
                     <div class="small mt-2"><!--span class="fas fa-angle-up text-success"></span> <span class="text-success font-weight-bold">18.2%</span--> <span class="fas fa-file-pdf"></span> Format: PDF, Word, Excel </small> </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-12 col-sm-6 col-xl-4 mb-4">
         <div class="card border-light shadow-sm">
            <div class="card-body">
               <div class="row d-block d-xl-flex align-items-center">
                  <div class="col-12 col-xl-4 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                     <div class="icon icon-shape icon-md icon-shape-warning rounded mr-4"><span class="fas  fa-calendar"></span></div>
                     <!--div class="d-sm-none">
                        <h2 class="h5">{{ __("Rapport Mensuel==") }}</h2>
                        <h3 class="mb-1">158</h3>
                     </div-->
                  </div>
                  <div class="col-12 col-xl-8 px-xl-0">
                     <div class="d-none d-sm-block">
                        <h2 class="h5">{{ __("Rapport mensuel") }}</h2>
                        <h3 class="mb-1">{{ isset($count_mensuel)?$count_mensuel:0 }}</h3>
                     </div>
                     <small> <span class="fas fa-calendar-check"></span> {{ isset($last_m_date)?$last_m_date:'-' }} <span class="icon icon-small"></span> </small>
                     <!--div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span class="text-success font-weight-bold">28.2%</span> Il y a deux mois</div-->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-12 col-sm-6 col-xl-4 mb-4">
         <div class="card border-light shadow-sm">
            <div class="card-body">
               <div class="row d-block d-xl-flex align-items-center">
                  <div class="col-12 col-xl-4 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                   <div class="icon icon-shape icon-md icon-shape-dropbox rounded mr-4"><span class="fas  fa-calendar-check"></span></div>
                  </div>
                  <div class="col-12 col-xl-8 px-xl-0">
                     <h2 class="h5">{{ __("Rapport par semestre") }}</h2>
                     <h3 class="mb-1">{{ isset($count_trimest)?$count_trimest:0 }}</h3>
                     <div class="mb-1 small"><span class="fas fa-calendar-check"></span> {{ isset($last_t_date)?$last_t_date:'-' }}</div>
                     <!--h6 class="font-weight-normal small text-gray"><span class="icon w-20 icon-xs icon-secondary mr-1"><span class="fas fa-desktop"></span></span> 3 rapports dans ce mois</h6-->
                    </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-12 col-xl-8 mb-4">
         <div class="row">
            <!--div class="col-12 mb-4">
               <div class="card border-light shadow-sm">
                  <div class="card-body d-flex flex-row align-items-center flex-0 border-bottom">
                     <div class="d-block">
                        <div class="h6 font-weight-normal text-gray mb-2">Sales Value</div>
                        <h2 class="h3">10,567</h2>
                        <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span class="text-success font-weight-bold">$10.57%</span></div>
                     </div>
                     <div class="d-flex ml-auto"><a href="#" class="btn btn-tertiary btn-sm mr-3">Month</a> <a href="#" class="btn btn-white border-light btn-sm mr-3">Week</a></div>
                  </div>
                  <div class="card-body p-2">
                     <div class="ct-chart-sales-value ct-major-tenth ct-series-b"></div>
                  </div>
               </div>
            </div-->
            <div class="col-12">
               <div class="card border-light shadow-sm">
                  <div class="card-header">
                     <div class="row align-items-center">
                        <div class="col">
                            @if(Auth::user()->getRoleNames()=='admin')
                            <h2 class="h5">{{ __('Rapports de suivi') }}</h2>
                            @else
                            <h2 class="h5">{{ __('Mes rapports de suivi') }}</h2>
                            @endif
                        </div>
                        <div class="col text-right"><!--a href="#" class="btn btn-sm btn-secondary">{{ __('Voir tout') }}</a--></div>
                     </div>
                  </div>
                  <div class="table-responsive">
                     <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                           <tr>
                               <th scope="col">{{ __('Rapport') }}</th>
                               <th scope="col">{{ __('Format') }}</th>
                              <th scope="col">{{ __('Type') }}</th>
                              <th scope="col">{{ __('Taille') }}</th>
                              <th scope="col">{{ __("Fichier ") }}</th>
                           </tr>
                        </thead>
                        <tbody>
                            @if(isset($rapports))
                            @foreach ($rapports as $r)
                           <tr>

                              <th scope="row" class="small text-bold text-gray-800">{{ $r->nom }}</th>
                              <td nowrap>
                                  @if(strtolower($r->format)=="pdf")
                                  <i class="fas fa-file-pdf text-danger"></i> PDF
                                  @elseif(strtolower($r->format)=="excel")
                                    <i class="fas fa-file-excel text-success"></i> Excel
                                  @elseif(strtolower($r->format)=="word")
                                  <i class="fas fa-file-word text-info"></i> Word
                                  @else
                                   <i class="fas fa-file text-gray-800"></i> Autre
                                  @endif
                                </td>
                              <td  class="small">{{ __($r->type) }}</td>
                              <td>{{ $r->taille }}</td>

                              <td nowrap><a target="_blanck" class=" btn btn-xs btn-outline-success rounded-sm " href="{{ asset($r->fichier) }}"><i class="fas fa-file-download"></i> {{ __('Télécharger') }}</a></td>
                           </tr>
                            @endforeach

                            @if(count($rapports)==0)
                            <tr>
                              <th colspan="5" scope="row">
                                  <div class="p-3  rounded  bg-gray-200 text-center">
                                      {{ __('Aucun rapport disponible pour le moment') }}
                                  </div>
                              </td>
                           </tr>
                           @else
                            <hr>
                            {{ $rapports->links() }}
                            @endif
                            @endif

                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-12 col-xl-4 mb-4">
         <div class="col-12 px-0 mb-4">
            <div class="card border-light shadow-sm">
               <div class="card-body">
                  <h2 class="h5 mt-1">Astuces et Conseils </h2>
                  <p>Pour profiter de l'application, il va falloir que votre plateforme Moodle respecte la structure que vous auriez précisé dans les paramètres. Assurez-vous d'avoir également les paramètres d'accès à la base de données</p>
                  <div class="d-block">
                     <div class="d-flex align-items-center pt-3 mr-5">
                        <div class="icon icon-shape icon-sm icon-shape-primary rounded mr-3"><span class="fas fa-tasks"></span></div>
                        <div class="d-block">
                           <label class="mb-0 h5">Configuration Moodle</label>
                           <div><a class="bold"  href="{{ url('/') }}">Configurer</a></div>
                        </div>
                     </div>
                     <div class="d-flex align-items-center pt-3">
                        <div class="icon icon-shape icon-sm icon-shape-secondary rounded mr-3"><span class="fas fa-cog"></span></div>
                        <div class="d-block">
                           <label class="mb-0 h5">Paramètres d'exportations</label>
                           <div><a class="bold" href="{{ url('/') }}">Modifier</a></div>
                        </div>
                     </div>
                     <div class="d-flex mb-2 align-items-center pt-3">
                        <div class="icon icon-shape icon-sm icon-shape-success rounded mr-3"><span class="fas fa-chart-area"></span></div>
                        <div class="d-block">
                           <label class="mb-0 h5">Statistiques des activités</label>
                           <div><a class="bold" href="{{ url('/') }}">Consulter</a></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <!--footer class="footer section py-3">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">
               <hr>
            </div>
            <div class="col-12 col-lg-6 mb-4 mb-lg-0">
               <p class="mb-0 text-center text-xl-left">Copyright © {{ date('Y') }}-<span class="current-year"></span> <a class="text-primary font-weight-normal" href="{{ url('/') }}" target="_blank">{{ __("Moodle Report") }}</a></p>
            </div>
            <div class="col-12 col-lg-6">
               <ul class="list-inline list-group-flush list-group-borderless text-center text-xl-right mb-0">
                  <li class="list-inline-item px-0 px-sm-2"><a href="{{ url("about") }}">{{ __("A propos") }}</a></li>
                  <li class="list-inline-item px-0 px-sm-2"><a href="{{ url("docs") }}">{{ __("Documentation") }}</a></li>
                  <li class="list-inline-item px-0 px-sm-2"><a href="{{ url("blog") }}">{{ __("Blog") }}</a></li>
                  <li class="list-inline-item px-0 px-sm-2"><a href="{{ url("contact") }}">{{ __("Contact") }}</a></li>
               </ul>
            </div>
         </div>
      </div>
   </footer-->
</div>
@endsection
