@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">config_structure {{ $config_structure->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/moodler/config_structure') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/moodler/config_structure/' . $config_structure->id . '/edit') }}" title="Edit config_structure"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('moodler/config_structure' . '/' . $config_structure->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete config_structure" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $config_structure->id }}</td>
                                    </tr>
                                    <tr><th> Nom </th><td> {{ $config_structure->nom }} </td></tr><tr><th> Logo </th><td> {{ $config_structure->logo }} </td></tr><tr><th> Telephone </th><td> {{ $config_structure->telephone }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
