<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Rapport mensuel</title>
</head>
<body>

<style>
body{
    font-family:arial, sans-serif;
    color:#000000;
   // background:#eeeeee;
}
.page-break {
    page-break-after: always;
}
.tb{
    border:1px solid #000; border-collapse:collapse;
    width:100%;
}
.tb td,.tb>th{
    padding:5px ;
}
.tb tr th{
    font-weight:bold;
    background:#eeeeee;
}
.tb tr th, .tb tr td{
    border:1px solid #666666; 
}

</style>
@php
        $filieres=$data["filiere"];
        $semestres=$data["semestre"];
        $matieres=$data["matiere"];
@endphp

<table  class="tb">
    <tr>
        <th>FORMATIONS</th>
        <th> SEMESTRE</th>
        <th> MATIERES</th>
        <th>SEQUENCES</th>
        <th>SEQUENCES EXCECUTEES </th>
        <th>TAUX D'EXECUTION </th>
    </tr>
    
   
   @foreach($filieres as $fil)

        @php 
                $mat_all = 0;
                $fil_id=$fil['id'];
                if(isset($semestres[$fil_id])){
                    foreach($semestres[$fil_id] as $s){
                        
                        $kmat=$s['id'];
                        $mat=$matieres[$kmat];
                        $nmat= (count($mat) > 0) ?count($mat): 1;
                        $mat_all+= $nmat;
                        
                    }
                }
                // $mat_all;



        @endphp

        <tr>
                <td rowspan="{{ $mat_all+1}}"> {{ $fil['nom']}}
                </td>
                @if(!isset($semestres[$fil_id]))
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> 
                @endif
        </tr>
            @if(isset($semestres[$fil_id]))
                @foreach($semestres[$fil_id] as $s)

                @php 
              
                    $kmat=$s['id'];
                    $mat=$matieres[$kmat];
                    $nmat=count($mat);
                    $compteur=0;
                    

                 @endphp

                        <tr>
                            <td rowspan="{{ $nmat }}"> {{ $s["nom"]}}
                            </td>

                            @if($nmat == 0)
                                    <td>-</td>
                                    <td> -</td>
                                    <td> -</td>
                                    <td> -</td>
                                 </tr>
                            @endif 


                            @foreach($mat as $m)

                                @if($compteur++ ==0)

                                        <td> {{$m->fullname }}</td>
                                        <td> {{$m->newsitems}}</td>
                                        <td> -</td>
                                        <td> -</td>
                                    </tr>
                                @else

                                    <tr>
                                        <td> {{$m->fullname }}</td>
                                        <td> {{$m->newsitems}}</td>
                                        <td> -</td>
                                        <td> -</td>
                                    </tr>
                                @endif

                            @endforeach

                       

                @endforeach
                
                @endif
              

   @endforeach 
</table>
    
</body>
</html>