@php
$filieres=$data["filiere"];
$semestres=$data["semestre"];
$matieres=$data["matiere"];
@endphp
<style>
    body {
        font-family: arial, sans-serif;
        color: #000000;
        // background:#eeeeee;
    }

    .page-break {
        page-break-after: always;
    }

    .tb {
        border: 1px solid #000;
        border-collapse: collapse;
        width: 100%;
    }

    .tb td,
    .tb>th {
        padding: 5px;
    }

    .tb tr th {
        font-weight: bold;
        background: #eeeeee;
    }

    .tb tr th,
    .tb tr td {
        border: 1px solid #666666;
    }

</style>
<table class="tb" border='1px'>
    <tr>
        <th>FORMATIONS</th>
        <th> SEMESTRE</th>
        <th> MATIERES</th>
        <th>SEQUENCES</th>
        <th>SEQUENCES EXCECUTEES </th>
        <th>TAUX D'EXECUTION </th>
    </tr>


    @foreach ($filieres as $fil)

        @php
        $mat_all = -1;
        $fil_id=$fil['id'];
        if(isset($semestres[$fil_id])){
        foreach($semestres[$fil_id] as $s){
            $kmat=$s['id'];
            $mat=$matieres[$kmat];
            $nmat= count(array_filter($mat, function($x) { return !empty($x); }));
            $mat_all+= $nmat+1;

            }

        }
        // $mat_all;
        $compteur_sem=0;


        @endphp

        <tr>
            <td @if ($mat_all> 1) rowspan="{{ $mat_all }}" @endif > {{ $fil['nom'] }}
            </td>
            @if (!isset($semestres[$fil_id]))
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endif


        @if (isset($semestres[$fil_id]))

            @foreach ($semestres[$fil_id] as $s)

                @php

                $kmat=$s['id'];
                $mat=isset($matieres[$kmat])?$matieres[$kmat]:null;

                //$nmat=(count($mat)>0)?count($mat):1;
                $nmat=count(array_filter($mat, function($x) { return !empty($x); }));
                //dd($nmat);
                $compteur=0;


                @endphp
                @if ($compteur_sem++ == 0)
                   
                @endif


                <tr>
                    <td @if ($nmat> 1) rowspan="{{ $nmat }}" @endif > {{ $s['nom'] }}
                    </td>

                    @if ($nmat == 0)
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endif

                @if ($nmat)
                    @foreach ($mat as $m)

                        @if ($compteur++ == 0)

                            <td> {{ $m->fullname }}</td>
                            <td> {{ $m->newsitems }}</td>
                            <td> -</td>
                            <td> -</td>
                            </tr>
                        @else

                            <tr>
                                <td> {{ $m->fullname }}</td>
                                <td> {{ $m->newsitems }}</td>
                                <td> -</td>
                                <td> -</td>
                            </tr>
                        @endif

                    @endforeach
                @else
                <td @if ($nmat> 1) rowspan="{{ $nmat }}" @endif > {{ $s['nom'] }}
                </td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                </tr>

                @endif



            @endforeach
        @else

        @endif


    @endforeach
</table>
