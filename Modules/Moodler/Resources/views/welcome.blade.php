@extends('moodler::layouts.app')
@section('title', 'Gérez mieux vos données sur Moodle')
@section('ccontent', '')

@section('nav')

@endsection
@section('content')
    <section class="h-100vh   d-flex align-items-center justify-content-center">
        <div class="container-fluid">
            <div class="row bg-white shadow-md rounded-lg border-light py-4">
                <div class="col-12 offset-lg-1 col-lg-4 mt-5 order-2 order-lg-1 text-center text-lg-left">
                    <h1 class="display-1 mt-3"><span class="text-primary">Moodle</span><span class="text-danger">R</span>
                    </h1>
                    <h3 class="mt-3">{!! __('Bienvenue dans votre <span class="text-primary">outil</span> de suivi du <span
                            class="text-danger">tutorat</span> pour <b>Moodle</b>.') !!} </h3>
                    <p class="leads font-weight-light font-smaller my-4">
                        {{ __("Ne perdez plus du temps à gérer manuelement vos rapports de suivi des formations sur la plateforme Moodle . Voici l'outil qui vous fera gagner en productivité.") }}
                    </p>
                    <form action="{{ route('login') }}">
                        <div class="card hidden p-3">
                        <div class="row">
                            <div class="col-sm-6 mb-3">
                                <div class="form-group">
                                    <label for="city">City</label>
                                    <input class="form-control" id="city" type="text" placeholder="{{ __('Email') }}" required="">
                                </div>
                            </div>
                            <div class="col-sm-6 mb-3">
                                <div class="form-group focused"><label for="country">Country</label> <select
                                        class="form-control select2-hidden-accessible" id="country" data-toggle="select"
                                        title="Country" data-live-search="true" data-live-search-placeholder="Country"
                                        data-select2-id="1" tabindex="-1" aria-hidden="true">
                                        <option data-select2-id="3">United States</option>
                                        <option>Canada</option>
                                        <option>Germany</option>
                                        <option>Spain</option>
                                        <option>Italy</option>
                                        <option>UK</option>
                                    </select></div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="{{ __('Connexion') }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <a class="btn btn-danger  animate-hover" href="{{ route('login') }}"><i
                            class="fas fa-book-open mr-3 pl-2 animate-left-3"></i>Documentation</a>
                            </div>
                        </div>
                        </div>

                    </form>
                    @guest
                        <a class="mr-3 btn btn-sms px-2 rounded  btn-md shadow-hover  btn-outline-primary" href="{{ route('login') }}"><i
                                class="fa fa-sign-in-alt mr-3 pl-2 animate-left-3"></i>{{ __('Se connecter') }}</a>
                    @else
                        <a class="mr-3 bold btn btn-primary mb-1 mt-1 btn-lg_ animate-hover" href="{{ route('dashbord') }}"><i
                                class="fas fa-tachometer-alt"></i> {{ __('Tableau de bord') }}</a>
                    @endguest
                    <a class="btn btn-outline-danger px-2 rounded btn-md shadow-hover" href="{{ route("getDocs") }}"><i
                            class="fas fa-book-open  mr-3 pl-2 animate-left-3"></i>Documentation</a>

                    {{-- <a class="ml-3 bold"
                        href="{{ route('login') }}">{{ __('Créer un compte') }}</a> --}}
                </div>
                <div
                    class="col-12 col-lg-7 order-1 order-lg-2  text-center d-flex align-items-center mt-4 justify-content-center">
                    <img class="img-fluid w-80" src="{{ asset('storage/moodler/img/home-app.png') }}"
                        alt="{{ __('Application de gestion de suivi des formations à distance') }}"></div>
            </div>

        </div>
    </section>
@endsection
