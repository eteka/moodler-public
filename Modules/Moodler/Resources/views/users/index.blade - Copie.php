@extends('layouts.app')
@section('title', __('Les utilisateurs'))

@section('content')
    <div class="bg-soft">
        @include('moodler.include.top')
        <section class="bg-soft">
            <main class="">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                    <div class="d-block mb-4 mb-md-0">
                        <h2 class="h3">{{ __('Les utilisateurs') }}</h2>
                    </div>
                </div>
                <div class="table-settings mb-4">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-12 col-md-6 col-lg-3 align-items-center d-none d-md-flex">
                            <div class="form-group mb-0 mr-3"><select class="custom-select"
                                    id="inlineFormCustomSelectMesages">
                                    <option selected="selected">Tout</option>
                                    <option value="1">{{ __('Administrateurs') }}</option>
                                    <option value="2">{{ __('Chargé de formation') }}</option>
                                    <option value="3">{{ __('Directeur') }}</option>
                                </select></div>
                            <div><a href="#" class="btn btn-md btn-white border-light">{{ __('Valider') }}</a></div>
                        </div>
                        <div class="col col-md-4 col-lg-3 col-xl-2 ml-auto">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><span
                                            class="fas fa-search"></span></span></div><input class="form-control"
                                    id="searchInputdashboard1" placeholder="Search" type="text"
                                    aria-label="Dashboard user search">
                            </div>
                        </div>
                        <div class="col-2 pl-0 d-none d-lg-flex">
                            <div class="form-group mb-0">
                                <select class="custom-select" id="inlineFormCustomSelectMesages2">
                                    <option selected="selected">Etat du compte</option>
                                    <option value="1">Actif</option>
                                    <option value="2">Désactivé</option>
                                </select>

                            </div>
                            <div class="form-group mb-0 ml-2 mr-2">
                                <select class="custom-select ml-2" id="">
                                    <option selected="selected">Afficher</option>
                                    <option value="1">10</option>
                                    <option value="2">20</option>
                                    <option value="3">30</option>
                                    <option value="3">40</option>
                                    <option value="3">50</option>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-body border-light shadow-sm table-wrapper table-responsive pt-0">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <!--th class="border-0">
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck55"> <label class="form-check-label"
                                            for="userCheck55"></label></div>
                                </th-->
                                <th class="border-0">{{ __('Nom et prénoms') }}</th>
                                <th class="border-0">{{ __('Date d\'inscription') }}</th>
                                <th class="border-0">{{ __('Type de compte') }}</th>
                                <th class="border-0">{{ __('Etat du compte') }}</th>
                                <th class="border-0">{{ __('Actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($users))
                                @foreach ($users as $u)
                                    <tr>
                                        <!--td>
                                            <div class="form-check dashboard-check"><input class="form-check-input"
                                                    type="checkbox" value="" id="userCheck1"> <label
                                                    class="form-check-label" for="userCheck1"></label>
                                            </div>
                                        </td-->
                                        <td>
                                            <a href="#" class="d-flex align-items-center">
                                                <!--img
                                                    src="../../assets/img/team/profile-picture-1.jpg"
                                                    class="user-avatar rounded-circle mr-3" alt="Avatar"-->
                                                    <!--<div class="user-avatar bg-secondary mr-3"><span>JR</span></div>-->
                                                    <div class="user-avatar {{ $u->profilbg() }} mr-3"><span>{{ $u->sigle() }}</span></div>
                                                <div class="d-block"><span class="font-weight-bold">{{ $u->fullname(1) }}</span>
                                                    <div class="small text-gray">{{ $u->email }}</div>
                                                </div>
                                            </a>
                                        </td>
                                        <td><span class="font-weight-normal">{{ $u->created_at!=NULL?$u->created_at->diffForHumans():'' }}</span></td>
                                        <td><span class="font-weight-normal"></span>{{ $u->roles->first()?$u->roles->first()->name:'' }}</td>
                                            <td>
                                                @if($u->etat_compte==0)
                                                    <span class="font-weight-normal text-danger"><span
                                                        class="fas fa-check-circle text-danger mr-2"></span> {{ __('Désactivé') }}
                                                    </span>
                                                @endif
                                                @if($u->etat_compte==1)
                                                    <span class="font-weight-normal text-success"><span
                                                        class="fas fa-check-circle text-success mr-2"></span> {{ __('Active') }}
                                                    </span>
                                                @endif
                                                @if($u->etat_compte==-1)
                                                    <span class="font-weight-normal text-warning"><span
                                                        class="fas fa-check-circle text-warning mr-2"></span> {{ __('Suspendu') }}
                                                    </span>
                                                @endif
                                            </td>
                                        <td>
                                            <div class="btn-group"><button
                                                    class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                        class="icon icon-sm"><span
                                                            class="fas fa-ellipsis-h icon-dark"></span>
                                                    </span><span class="sr-only">Toggle Dropdown</span></button>
                                                <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                            class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                        class="dropdown-item" href="#"><span
                                                            class="fas fa-eye mr-2"></span>View
                                                        Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                            class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                            </div>
                                            <!--a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                                data-original-title="Delete"><span class="fas fa-times-circle"></span></a-->
                                        </td>
                                    </tr>
                                @endforeach
                            @endif


                        </tbody>
                    </table>
                    <div class="card-footer px-3 d-flex align-items-center justify-content-between">
                        {{ $users->links() }}
                        <!--nav aria-label="Page navigation example">
                            <ul class="pagination mb-0">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>
                        <div class="font-weight-bold small">Showing <b>5</b> out of <b>25</b> entries</div-->
                    </div>
                </div>

            </main>
        </section>
    </div>
@endsection
