@extends('moodler::layouts.app')
@section('title', __('Modification du rôle de l\'utilisateur'))

@section('content')
    <div class="bg-soft">
        @include('moodler::include.top')
        <section class="bg-soft">
            <main class="min-vh-100">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                    <div class="d-block mb-4 mb-md-0">
                        <h2 class="h6">{{ __('Modification du rôle') }}</h2>
                    </div>
                </div>
                <div class="table-settings mb-4">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-6">
                            <div class="card shadow-sm">
                                <div class="card-body">

                        <form method="POST" action="{{ route('saveRole',isset($id)??0) }}" accept-charset="UTF-8" class="form-inlines" role="search">
                                {{ csrf_field() }}

                            <div class="mt-1">
                                <!-- Title -->
                                <div class="actions-toolbar py-2 mb-4">
                                    <h5 class="mb-1">{{ __('Modification du rôle') }}</h5>
                                   </div>
                                <!-- New address form -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="nom_param" class="form-control-label text-right col-md-4 ">
                                                <span class="text-bold">{{ __('Nom de l\'Utilisateur paramètre') }} </span><br>
                                               </label>
                                            <div class="col-md-5">
                                                {{ Form::text('nom',isset($nom)?$nom:'-',['class'=>"form-control text-bold rounded-sm form-control-sm",'disabled'=>"disabled"]) }}
                                            </div>
                                        </div>
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="univ" class="form-control-label text-right col-md-4 ">
                                                <span class="text-bold">{{ __('Rôles') }} </span><br>
                                                <span class="text-info">{{ __('Sélectionnez un rôle') }}</span>
                                            </label>

                                            <div class="col-md-4">
                                                {{ Form::select('role',isset($roles)?$roles:[],null,['class'=>"custom-select custom-select-sm mt-2"]) }}
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="btn btn-link btn-sm" href="{{ route('getUsers') }}"><i class="fa fa-chevron-left"></i> {{ __('Retour') }}</a>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn  btn-primary">{{ __("Valider") }}</button>

                                    </div>
                                </div>
                            </div>

                        </form>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                

            </main>
        </section>
    </div>
@endsection
