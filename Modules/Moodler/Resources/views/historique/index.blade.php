@extends('moodler::layouts.app')
@section('title', 'Historique de mes actitivités')

@section('content')
    <div class="bg-soft min-vh-100">
        @include('moodler::include.top')
        <section class="">
            <main class="">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                    <div class="d-block mb-4 mb-md-0">
                        <h2 class="h3">{{ __('Historique des activités') }}</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 mb-4">
                        <div class="card border-light shadow-sm">
                            <div class="card-header">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h2 class="h5">{{ __('Mes activités') }}</h2>
                                    </div>
                                    <div class="col text-right"><a href="#" class="btn btn-sm btn-secondary">Tout supprimer</a>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                @if(isset($historiques))
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">{{ __('Auteur') }}</th>
                                            <!--th scope="col">{{ __('Auteur') }}</th-->
                                            <th scope="col">{{ __('Activité') }}</th>
                                            <th scope="col">{{ __('Date') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($historiques as $key => $h)
                                        <tr>
                                            <th scope="row" class="col-2" >
                                                @php
                                                    $u=$h->causer;
                                                @endphp
                                                <a href="#" class="d-flex align-items-center">
                                                <!--img
                                                    src="../../assets/img/team/profile-picture-1.jpg"
                                                    class="user-avatar rounded-circle mr-3" alt="Avatar"-->
                                                    <!--<div class="user-avatar bg-secondary mr-3"><span>JR</span></div>-->
                                                    @if($u->photo=="")
                                                    <div class="user-avatar xs-avatar {{ $u->profilbg() }} mr-3"><span>{{ $u->sigle() }}</span></div>
                                                    @else
                                                    <img src="{{ asset($u->photoProfil()) }}" class="user-avatar xs-avatar rounded-circle mr-3" alt="{{ $u->fullname() }}">
                                                    @endif
                                                <div class="d-block">
                                                    <span class="font-weight-bold small text-gray-700">{{ $u->fullname(1) }}</span>
                                                </div>
                                            </a>
                                            </th>
                                            <td class="col-5">
                                                <div class="text-bold">{{ $h->log_name }}</div>
                                                <div class="text-gray-500">{{ $h->description }}</div>
                                            </td>

                                            <td class="col-3 small">{{ date('d/m/Y H\H:i\m\i\n',strtotime($h->created_at)) }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <hr>
                                {{ $historiques->links() }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </main>
        </section>
    </div>
@endsection
