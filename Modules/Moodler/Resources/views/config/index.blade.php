@extends('moodler::layouts.app')

@section('title', __('Configurations') . '-' . $user->fullname())

@section('content')

    <div class="main min-vh-100">
        <div class="slim-pageheader">
            <ol class="breadcrumb slim-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Accueil') }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ __('Configurations') }}</li>
            </ol>
            <h6 class="slim-pagetitle">{{ __('Configurations') }}</h6>
        </div><!-- slim-pageheader -->

        <div class="row row-sm">
            <div class="col-lg-9">

                <div class="card mt-3 border-darken-1 shadow-sm">
                    <div class=" card-body bg-white border-light">
                        <h2 class="h5 mb-4">{{ __('Configurations de l\'application') }}</h2>
                        <ul class="list-group list-group-flush">
                             <li
                                class="list-group-item d-flex align-items-center justify-content-between px-3 border-bottom">
                                <div>
                                    <h3 class="h6 mb-1 "><i class="fa fa-check text-success text-bold"></i> {{ __('Configuration de votre structure') }}</h3>
                                    <p class="small pr-4">{{ __('Renseigner les informations relatives à votre centre de formation') }}
                                    </p>
                                </div>
                                <div>
                                    <div class="">
                                        <a href="{{ route('structure') }}" class="btn btn-sm btn-outline-primary rounded-sm"><i class="fa fa-cog"></i> {{ __('Configurer') }}</a>
                                    </div>
                                </div>
                            </li>
                            <li
                                class="list-group-item bg-gray-200 px-3 d-flex align-items-center justify-content-between  border-bottom">
                                <div>
                                    <h3 class="h6 mb-1 "> <i class="fa fa-check text-success text-bold"></i> {{ __('Configuration la base de données') }}</h3>
                                    <p class="small pr-4">{{ __('Renseigner les informations pour accéder à votre base de données Moodle') }}
                                    </p>
                                </div>
                                <div>
                                    <div class="">
                                        <a href="{{ route('configBdd') }}" class="btn btn-sm btn-outline-primary rounded-sm"><i class="fa fa-cog"></i> {{ __('Configurer') }}</a>
                                    </div>
                                </div>
                            </li>
                            <li
                                class="list-group-item d-flex align-items-center justify-content-between px-3 border-bottom">
                                <div>
                                    <h3 class="h6 mb-1 "><i class="fa fa-check text-success text-bold"></i> {{ __('Configuration des préférences') }}</h3>
                                    <p class="small pr-4">{{ __('Configurer les préférences d\'exportation de vos rapports de suivi') }}
                                    </p>
                                </div>
                                <div>
                                    <div class="">
                                        <a href="{{ route('preferences') }}" class="btn btn-sm btn-outline-primary rounded-sm"><i class="fa fa-cog"></i> {{ __('Configurer') }}</a>
                                    </div>
                                </div>
                            </li>



                        </ul>

                    </div>

                </div><!-- card -->



            </div><!-- col-8 -->

            <div class="col-lg-4 mg-t-20 mg-lg-t-0">


            </div><!-- col-4 -->
        </div><!-- row -->

    </div>
@endsection
