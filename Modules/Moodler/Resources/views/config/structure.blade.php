@extends('moodler::layouts.app')

@section('title', __('Configuration'))

@section('content')
    <div class="bg-soft">
        <section class="mt-3">
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                        <div class="d-block mb-4 mb-md-0">
                            <h2 class="h3 ml-4 page-header">{{ __('Configuration de centre de formation') }}</h2>
                        </div>
                    </div>

                    <div class="section-wrapper mt-3 col-sm-9">
                         <div class="card py-3  bg-white border-light shadow-sm mb-4">
                             <div class="card-body">
                        <h2 class="h5  mb-1">{{ __('Renseignements sur le centre') }}</h2>

                        <p class="mg-b-20 text-gray-800 mg-sm-b-40 mb-4">
                            {{ __("Veuillez remplir les informations sur la structure pour laquelle l'application est utilisée") }}

                        <hr></p>
                        <div class="row">
<section id="wizard6-p-0" role="tabpanel" aria-labelledby="wizard6-h-0"
                                            class="card-body current" aria-hidden="false">
                                            <!--form action="{{ route('saveStructure') }}" method="POST" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data"-->
                                            {!! Form::model($config, ['method' => 'POST', 'route' => 'saveStructure']) !!}

                                            @csrf
                                            <div class="form-layout form-layout-5">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        @if ($errors->any())
                                                            <ul class="alert alert-danger">
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </div>
                                                        <label class="col-md-3 form-control-label"><span
                                                            class="tx-danger">*</span>
                                                        {{ __('Nom de la structure') }}:</label>
                                                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                                                        {{ Form::text('nom', null, ['class' => 'form-control', 'placeholder' => __('Université du Bénin')]) }}

                                                    </div>
                                                </div><!-- row -->
                                                <div class="form-group_ mt-3 mb-4">
                                                    <!--div class="row">
                                                        <label class="col-sm-3 form-control-label">
                                                            {{ __('Image du logo') }} :
                                                        </label>
                                                        <div class="col-sm-6 mg-t-10  mg-sm-t-0">
                                                            {{ Form::file('logo', ['class' => 'form-control', 'accept' => '.png,.jpg,.jpeg']) }}

                                                        </div>

                                                    </div-->
                                                    <div class="form-group_ row mt-3 mb-4">
                                                        <label class="col-sm-3 form-control-label">
                                                            {{ __('Localisation') }} :
                                                        </label>
                                                        <div class="col-sm-6 mg-t-10  mg-sm-t-0">
                                                            {{ Form::text('localisation', null, ['class' => 'form-control', 'placeholder' => __('Bénin, Porto-Novo')]) }}
                                                        </div>
                                                    </div>
                                                    <div class="form-group_ row mt-3 mb-4">
                                                        <label class="col-sm-3 form-control-label">
                                                            {{ __('Téléphone') }} :
                                                        </label>
                                                        <div class="col-sm-6 mg-t-10  mg-sm-t-0">
                                                            {{ Form::text('tel', null, ['class' => 'form-control', 'placeholder' => __('+229 96 00 00 00')]) }}
                                                        </div>
                                                    </div>
                                                    <div class="form-group_ row mt-3 mb-4">
                                                        <label class="col-sm-3 form-control-label">
                                                            {{ __('Email') }}:</label>
                                                        <div class="col-sm-6 mg-t-10  mg-sm-t-0">
                                                            {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => __("Entrer l'adresse email")]) }}
                                                        </div>
                                                    </div>
                                                    <div class="form-group_ row mt-3 mb-4">
                                                        <label class="col-sm-3 form-control-label v-top">
                                                            {{ __('Description') }}:</label>
                                                        <div class="col-sm-6 mg-t-10  mg-sm-t-0">
                                                            {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => __('Entrer la description'), 'rows' => 2]) }}

                                                        </div>
                                                    </div><!-- row -->
                                                    <div class="row mg-t-30">
                                                        <div class="col-sm-3">
                                                        </div>
                                                        <div class="col-sm-8 ">
                                                            <div class="form-layout-footer">
                                                                <button class="btn btn-primary bd-0" type="submit"><i
                                                                        class="fa fa-save"></i>
                                                                    {{ __('Sauvegarde') }}</button>
                                                                <a href="{{ route('welcome') }}"
                                                                    class="btn btn-secondary bd-0">
                                                                    <i class="fa fa-chevron-left"></i>
                                                                    {{ __('Retour') }}</a>
                                                            </div><!-- form-layout-footer -->
                                                        </div><!-- col-8 -->
                                                    </div>
                                                </div>
                                                </form>
                                        </section>
                            </div>
                    </div>


                    </div>
                </div>

@endsection
