@extends('moodler::layouts.app')

@section('title', __('Configuration de préférences'))

@section('content')
    <div class="bg-soft">
        <section class="mt-3">
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                        <div class="d-block mb-4 mb-md-0">
                            <h2 class="h3 ml-4 page-header">{{ __('Configuration de vos préférennces') }}</h2>
                        </div>
                    </div>

                    <div class="section-wrapper mt-3 col-sm-9">
                        <div class="card py-3  bg-white border-light shadow-sm mb-4">
                            <div class="card-body">
                                <h2 class="h5  mb-1">{{ __('Préférences') }}</h2>

                                <p class="mg-b-20 text-gray-800 mg-sm-b-40 mb-4">
                                    {{ __('Choisissez les options dans chacun des cas ci-dessous') }}

                                    <hr>
                                </p>
                                <div class="forms">
                                    <section id="wizard6-p-0" role="tabpanel" aria-labelledby="wizard6-h-0"
                                        class="body current" aria-hidden="false">
                                        {!! Form::model($config, ['method' => 'POST', 'route' => 'savePreferences']) !!}
                                        @csrf
                                        <div class="form-layout form-layout-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @if ($errors->any())
                                                        <ul class="alert alert-danger">
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="list-group">
                                                        <div class="list-group-item pd-y-20">
                                                            <div class="media">
                                                                <div class="d-flex mg-r-10 wd-50">
                                                                    <i class="icon ion-earth tx-muted tx-40 tx"></i>
                                                                </div><!-- d-flex -->
                                                                <div class="media-body">
                                                                    <div class="row">
                                                                        <div class="col-md-5 text-right mt-3">
                                                                            {{ __('Langue par défaut') }}:
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p
                                                                                class="mg-b-0 {{ $errors->has('langue') ? 'has-error' : '' }}">
                                                                                {{ Form::select('langue', $langues, null, ['class' => 'custom-select custom-select-sm mt-2', 'placeholder' => __('Langue par défaut')]) }}

                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="list-group-item pd-y-20">
                                                            <div class="media">
                                                                <div class="d-flex mg-r-10 wd-50">
                                                                    <i class="icon ion-earth tx-muted tx-40 tx"></i>
                                                                </div><!-- d-flex -->
                                                                <div class="media-body">
                                                                    <div class="row">
                                                                        <div class="col-md-5 text-right mt-3">

                                                                            {{ __('Fuseau horaire') }} :

                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p
                                                                                class="mg-b-0 {{ $errors->has('fuseau_horaire') ? 'has-error' : '' }}">
                                                                                {{ Form::select('fuseau_horaire', $fuseau, null, ['class' => 'custom-select custom-select-sm mt-2', 'placeholder' => __('Fuseau horaire')]) }}

                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="list-group-item pd-y-20">
                                                            <div class="media">
                                                                <div class="d-flex mg-r-10 wd-50">
                                                                    <i class="icon ion-archive tx-muted tx-40 tx"></i>
                                                                </div><!-- d-flex -->
                                                                <div class="media-body">
                                                                    <div class="row">
                                                                        <div class="col-md-5 text-right mt-3">
                                                                            {{ __("Renommer le fichier d'exportation") }} :

                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p
                                                                                class="mg-b-0 {{ $errors->has('fichier') ? 'has-error' : '' }}">
                                                                                {{ Form::select('fichier', $fichiers, null, ['class' => 'custom-select custom-select-sm mt-2', 'placeholder' => __('Nom du fichier à exporter')]) }}

                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="list-group-item pd-y-20">
                                                            <div class="media">
                                                                <div class="d-flex mg-r-10 wd-50">
                                                                    <i class="icon ion-forward tx-muted tx-40 tx"></i>
                                                                </div><!-- d-flex -->
                                                                <div class="media-body">
                                                                    <div class="row">
                                                                        <div class="col-md-5 text-right mt-3">

                                                                            {{ __("Format d'exportation") }}:

                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p
                                                                                class="mg-b-0 {{ $errors->has('format') ? 'has-error' : '' }}">
                                                                                {{ Form::select('format', $formats, null, ['class' => 'custom-select custom-select-sm  mt-2', 'placeholder' => __('Format du fichier')]) }}

                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="list-group-item pd-y-20">
                                                            <div class="media">
                                                                <div class="d-flex mg-r-10 wd-50">
                                                                    <i class="icon ion-forward tx-muted tx-40 tx"></i>
                                                                </div><!-- d-flex -->
                                                                <div class="media-body">
                                                                    <div class="row">
                                                                        <div class="col-md-5 text-right mt-3">

                                                                            {{ __("Nombre d'élément par page") }}:

                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <p
                                                                                class="mg-b-0 {{ $errors->has('format') ? 'has-error' : '' }}">
                                                                                {{ Form::number('nb', null, ['size' => 3, 'class' => 'form-control custom-select-sm  mt-2', 'placeholder' => __('10')]) }}

                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <!-- list-group-item -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mg-t-30">
                                                <div class="col-sm-8 text-center">
                                                    <div class="form-layout-footer">
                                                        <button class="btn btn-primary bd-0" type="submit"><i
                                                                class="fa fa-save"></i> {{ __('Sauvegarde') }}</button>
                                                        <a href="{{ route('welcome') }}" class="btn btn-secondary bd-0"> <i
                                                                class="fa fa-chevron-left"></i> {{ __('Retour') }}</a>
                                                    </div><!-- form-layout-footer -->
                                                </div><!-- col-8 -->
                                            </div>
                                        </div>
                                        </form>
                                    </section>
                                </div>
                            </div>

                            <!--div id="wizard6" role="application" class="wizard wizard-style-2  clearfix">
                                    <div class="steps clearfix">
                                        <ul role="tablist">
                                            <li role="tab" class="first disabled" aria-disabled="false" aria-selected="true">
                                                <a id="wizard6-t-0" href="{{ route('structure') }}" aria-controls="wizard6-p-0">
                                                    <span class="number">1</span> <span class="title">Information sur la
                                                        structure</span></a>
                                            </li>
                                            <li role="tab" class="current" aria-disabled="true">
                                                <a id="wizard6-t-1" href="{{ route('configBdd') }}" aria-controls="wizard6-p-1">
                                                    <span class="number">2</span>
                                                    <span class="title">La base de données </span></a></li>
                                            <li role="tab" class="disabled last" aria-disabled="true">
                                                <a id="wizard6-t-2" href="{{ route('preferences') }}" aria-controls="wizard6-p-2">
                                                    <span class="number">3</span> <span class="title">Préférences</span></a></li>
                                        </ul>
                                    </div>


                                </div-->
                        </div>
                    </div>
@endsection
