@if ($message = Session::get('success'))
<div class="alert alert-success alert-block mt-3 mb-0">
	<button type="button" class="close" data-dismiss="alert">×</button>
       <span class="badge badge-white px-2 rond30">Bravo !</span> <strong> {{ $message }}</strong>
    </div>
@endif
@if ($message = Session::get('flash_message'))
<div class="alert alert-success alert-block mt-3 mb-0">
	<button type="button" class="close" data-dismiss="alert">×</button>
         <span class="badge badge-white px-2 rond30">Bravo !</span><strong>{{ $message }}</strong>
    </div>
@endif


@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block mt-3 mb-0">
	<button type="button" class="close" data-dismiss="alert">×</button>
        <span class="badge badge-white px-2 rond30">Attention !</span> <strong>{{ $message }}</strong>
</div>

@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block mt-3 mb-0">
	<button type="button" class="close" data-dismiss="alert">×</button>
	 <span class="badge badge-white px-2 rond30">Alert</span><strong> {{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-block mt-3 mb-0">
	<button type="button" class="close" data-dismiss="alert">×</button>
	 <span class="badge badge-white px-2 rond30">Information</span> <strong> {{ $message }}</strong>
</div>
@endif
@if ($errors->any())
<!--div class="alert alert-danger mt-3 mb-0">
	<button type="button" class="close" data-dismiss="alert">×</button>
	Please check the form below for errors
</div-->
@endif
