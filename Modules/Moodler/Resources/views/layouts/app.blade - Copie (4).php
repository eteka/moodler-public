<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from moodler.me/slim1.1/template/widgets.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 May 2020 12:48:43 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@moodler">
    <meta name="twitter:creator" content="@moodler">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Moodle Data Report">
    <meta name="twitter:description" content="Moodle Report">
    <meta name="twitter:image" content="./assets/app/moodler-twitter-image.jpeg">

    <!-- Facebook -->
    <meta property="og:url" content="http://moodler.me/app">
    <meta property="og:title" content="Moodle Report">
    <meta property="og:description" content="Premium Quality Best Moodle Report.">

    <meta property="og:image" content="./assets/app/moodler--image.jpeg">
    <meta property="og:image:secure_url" content="./assets/app/moodler-ssl-image.jpeg">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">
<!--
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet"> 

     Meta -->
    <meta name="description" content="Best Moodle Report Application">
    <meta name="author" content="moodler">

    <title>@yield('title') - Moodle Report</title>

    <!-- vendor css -->
    <link href="{{asset('storage/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('storage/lib/Ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <!--link href="{{asset('storage/lib/rickshaw/css/rickshaw.min.css')}}" rel="stylesheet"-->

    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('storage/css/style.css')}}">
    <style>
      body{
        font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;
        font-family: 'Montserrat', sans-serif;
        font-size: .875rem;
        font-weight: 400;
        line-height: 1.5;
        color: #23282c;
        text-align: left;
       // background-color: #f5f8ff;
      }
      .custom-control-label{
        margin-bottom: 20px;
      }
      .slim-footer {
        border-top: 0px solid #ced4da;
        background-color: #fff;
        margin-top: 30px;
        padding: 15px 0;
       /* box-shadow: 0 0 5px #e4eaf7;*/
      }
      .has-error .form-control{
        background-color: #fef9f9;
        border-color: #dc3545;
      }
      .slim-mainpanel{
        padding-top: 0vh;
      }
      .steps .current a,.steps .current a:hover{
        color:#f5f8ff;
      }
      .steps ul li{
        margin-right: 3%;
      }
      .wizard > .content{
        background:#ffffff;
      }
      .btn{
        border-radius: 45px;
      }
      .main{
        min-height: 670px;
      }
      .card{
        border-radius: 3px
        ;
        border: 1px solid #e1e4e7;
      }
      .slim-header{
        height: 65px;
      }
      .form-control{
        border: 1px solid #c8d0d9;
        border-radius: 3px;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
      }
    </style>
  </head>
  <body>
    <div class="slim-header">
      <div class="container">
        <div class="slim-header-left">
          <h2 class="slim-logo"><a href="{{url('/')}}">MoodleR<span>.</span></a></h2>

          <!--div class="search-box">
            <input type="text" class="form-control" placeholder="Search">
            <button class="btn btn-primary"><i class="fa fa-search"></i></button>
          </div--><!-- search-box -->
        </div><!-- slim-header-left -->
        <div class="slim-header-right">
         
          
         
          @guest
          @if (Route::has('register'))
          <a  class="nav-link read" href="{{ route('login') }}">{{ __('Se connecter') }}</a>
          
          
          <a class="nav-link" href="{{ route('register') }}">{{ __('Créer un compte') }}</a>
          
          @endif
          @else
          <div class="dropdown dropdown-a">
          <a href="{{route('dashbord')}}" class="header-notification" >
              <i class="icon ion-compass"></i>
              <span class="indicator"></span>
            </a>
          </div>
            <div class="dropdown dropdown-a">
            <!--a href="#" class="header-notification" data-toggle="dropdown">
              <i class="icon ion-ios-bolt-outline"></i>
            </a-->
            <div class="dropdown-menu">
              <div class="dropdown-menu-header">
                <h6 class="dropdown-menu-title">Activity Logs</h6>
                <div>
                  <a href="#">Filter List</a>
                  <a href="#">Settings</a>
                </div>
              </div><!-- dropdown-menu-header -->
              <div class="dropdown-activity-list">
                <div class="activity-label">Today, December 13, 2017</div>
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">10:15am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                    <div class="col-8">Purchased christmas sale cloud storage</div>
                  </div><!-- row -->
                </div><!-- activity-item -->
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">9:48am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-danger"></span></div>
                    <div class="col-8">Login failure</div>
                  </div><!-- row -->
                </div><!-- activity-item -->
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">7:29am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-warning"></span></div>
                    <div class="col-8">(D:) Storage almost full</div>
                  </div><!-- row -->
                </div><!-- activity-item -->
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">3:21am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                    <div class="col-8">1 item sold <strong>Christmas bundle</strong></div>
                  </div><!-- row -->
                </div><!-- activity-item -->
                <div class="activity-label">Yesterday, December 12, 2017</div>
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">6:57am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                    <div class="col-8">Earn new badge <strong>Elite Author</strong></div>
                  </div><!-- row -->
                </div><!-- activity-item -->
              </div><!-- dropdown-activity-list -->
              <div class="dropdown-list-footer">
                <a href="page-activity.html"><i class="fa fa-angle-down"></i> Show All Activities</a>
              </div>
            </div><!-- dropdown-menu-right -->
          </div><!-- dropdown -->
          <div class="dropdown dropdown-b">
         
            <a href="#" class="header-notification" data-toggle="dropdown">
              <i class="icon ion-ios-bell-outline"></i>
              <span class="indicator"></span>
            </a>
            <div class="dropdown-menu">
              <div class="dropdown-menu-header">
                <h6 class="dropdown-menu-title">Notifications</h6>
                <div>
                  <a href="#">Mark All as Read</a>
                  <a href="#">Settings</a>
                </div>
              </div><!-- dropdown-menu-header -->
              <div class="dropdown-list">
                <!-- loop starts here -->
                <a href="#" class="dropdown-link">
                  <div class="media">
                    <img src="../img/img8.jpg" alt="">
                    <div class="media-body">
                      <p><strong>Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                      <span>October 03, 2017 8:45am</span>
                    </div>
                  </div><!-- media -->
                </a>
                <!-- loop ends here -->
                <a href="#" class="dropdown-link">
                  <div class="media">
                    <img src="../img/img9.jpg" alt="">
                    <div class="media-body">
                      <p><strong>Mellisa Brown</strong> appreciated your work <strong>The Social Network</strong></p>
                      <span>October 02, 2017 12:44am</span>
                    </div>
                  </div><!-- media -->
                </a>
                <a href="#" class="dropdown-link read">
                  <div class="media">
                    <img src="../img/img10.jpg" alt="">
                    <div class="media-body">
                      <p>20+ new items added are for sale in your <strong>Sale Group</strong></p>
                      <span>October 01, 2017 10:20pm</span>
                    </div>
                  </div><!-- media -->
                </a>
                <a href="#" class="dropdown-link read">
                  <div class="media">
                    <img src="../img/img2.jpg" alt="">
                    <div class="media-body">
                      <p><strong>Julius Erving</strong> wants to connect with you on your conversation with <strong>Ronnie Mara</strong></p>
                      <span>October 01, 2017 6:08pm</span>
                    </div>
                  </div><!-- media -->
                </a>
                <div class="dropdown-list-footer">
                  <a href="page-notifications.html"><i class="fa fa-angle-down"></i> Show All Notifications</a>
                </div>
              </div><!-- dropdown-list -->
            </div><!-- dropdown-menu-right -->
          </div><!-- dropdown -->
          <div class="dropdown dropdown-c"> 
            <a href="#" class="logged-user" data-toggle="dropdown">
              <img src="{{asset(Auth::user()->photoProfil())}}" alt="{{ Auth::user()->name }}">
              <span>{{ Auth::user()->name }} </span>
              <i class="fa fa-angle-down"></i>
            </a>
            
            <div class="dropdown-menu dropdown-menu-right">
              <nav class="nav">
                <a href="{{route('profil')}}" class="nav-link"><i class="icon ion-person"></i> {{__('Mon compte')}} ({{Auth::user()->nom}})</a>
                <a href="{{route('editProfil')}}" class="nav-link"><i class="icon ion-compose"></i> Edit Profile</a>
                <a href="{{route('activites')}}" class="nav-link"><i class="icon ion-ios-bolt"></i> Activity Log</a>
                <a href="{{route('parametres')}}" class="nav-link"><i class="icon ion-ios-gear"></i> Account Settings</a>
                
                <a class="dropdown-item nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="icon ion-forward"></i> {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
              
              </nav>
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
          @endguest
        </div><!-- header-right -->
      </div><!-- container -->
    </div><!-- slim-header -->

    

    <div class="slim-mainpanel">
    <div class="container main">
      @yield('content')
    </div>
    </div><!-- slim-mainpanel -->

    <div class="slim-footer">
      <div class="container">
        <p>Copyright {{date('Y')}} &copy; Tout droit réservé. Moodle Report</p>
        <p>Designed by: <a href="#">Wilfried</a></p>
      </div><!-- container -->
    </div><!-- slim-footer -->

    <script src="{{asset('storage/lib/jquery/js/jquery.js')}}"></script>
    <script src="{{asset('storage/lib/popper.js/js/popper.js')}}"></script>
    <script src="{{asset('storage/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('storage/lib/jquery.cookie/js/jquery.cookie.js')}}"></script>
    <script src="{{asset('storage/lib/moment/js/moment.js')}}"></script>
    <script src="{{asset('storage/lib/d3/js/d3.js')}}"></script>
    <script src="{{asset('storage/lib/rickshaw/js/rickshaw.min.js')}}"></script>
    <script src="{{asset('storage/lib/peity/js/jquery.peity.js')}}"></script>

    <script src="{{asset('storage/js/slim.js')}}"></script>
    <script src="{{asset('storage/js/ResizeSensor.js')}}"></script>
    <script src="{{asset('storage/js/widgets.js')}}"></script>
  </body>

<!-- Mirrored from moodler.me/slim1.1/template/widgets.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 27 May 2020 12:48:44 GMT -->
</html>
