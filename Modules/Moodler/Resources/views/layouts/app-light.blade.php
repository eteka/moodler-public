<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <title>@yield('title',"Administration")
        {{ config('app.name') != '' ? '| ' . config('app.name') : 'Beinvenue sur MoodleR' }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="title" content="{{ config('app.name') }} - Dashboard">
    <meta name="author" content="{{ config('app.name') }}">
    <meta name="description" content="{{ config('app.description') }}">
    <meta name="keywords" content="{{ config('app.keyword') }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:title" content="{{ config('app.name') }} - Dashboard">
    <meta property="og:description" content="{{ config('app.description') }}">
    <meta property="og:image" content="{{ config('app.app-img-preview') }}">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="{{ url('/') }}">
    <meta property="twitter:title" content="{{ config('app.name') }} - Dashboard">
    <meta property="twitter:description" content="{{ config('app.description') }}">
    <meta property="twitter:image" content="{{ config('app.app-img-preview') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('./assets/assets/img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('./assets/assets/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('./assets/assets/img/favicon/favicon-16x16.png') }}">

    <link rel="mask-icon" href="{{ asset('./assets/assets/img/favicon/safari-pinned-tab.svg') }}" color="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link type="text/css" href="{{ asset('./storage/lib/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!--link type="text/css" href="./storage/moodler/admin/prism.min.css" rel="stylesheet"-->
    <link rel="stylesheet" href="{{ asset('./storage/moodler/admin/css/jqvmap.min.css') }}">
    <link type="text/css" href="{{ asset('./storage/moodler/admin/css/rocket.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('./storage/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}">

</head>

<body class="bg-soft">
    {{-- <div
        class="preloader bg-soft flex-column justify-content-center align-items-center"><img class="loader-element"
            src="./assets/assets/img/brand/dark.svg" height="50" alt="Rocket logo"></div>
    --}}
    <nav class="navbar navbar-dark navbar-theme-primary col-12">
        <div class="container">
        <a class="navbar-brand mr-lg-5" href="{{ url('') }}">
            <img class="navbar-brand-dark" src="{{ asset('storage/assets/img/app/logo_moodler2.png') }}" alt="MoodleR">
            <img class="navbar-brand-light" src="{{ asset('storage/assets/img/app/logo_moodler1.png') }}"
                alt="Pixel Logo Dark"></a>
        <div class="d-flex align-items-center"><button class="navbar-toggler d-md-none collapsed" type="button"
                data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button></div>
    </div>
  </nav>



    <div>
        @include('moodler::layouts.flash-message')
        @yield('content')
        <hr>
        <div class="container  ">
            <div class="row">
                <div class="col-12">

                    <footer class="footer section py-2 mb-4">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 col-lg-6 mb-4 mb-lg-0">
                                    <p class="mb-0 text-center text-xl-left">Copyright © {{ date('Y') }} <a
                                            class="ml-2 text-primary font-weight-normal" href="{{ url('/') }}"
                                            target="_blank">{{ __('MoodleR') }}</a></p>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <ul
                                        class="list-inline list-group-flush list-group-borderless text-center text-xl-right mb-0">
                                        <li class="list-inline-item px-0 px-sm-2"><a
                                                href="{{ route('getApropos') }}">{{ __('A propos') }}</a></li>

                                        <li class="list-inline-item px-0 px-sm-2"><a
                                                href="{{ route('getDocs') }}">{{ __('Documentation') }}</a></li>

                                        <li class="list-inline-item px-0 px-sm-2"><a
                                                href="{{ route('blog.index') }}">{{ __('Blog') }}</a></li>
                                        <li class="list-inline-item px-0 px-sm-2"><a
                                                href="{{ route('getContact') }}">{{ __('Contact') }}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>

        </div>

    </div>

    <script src="{{ asset('./storage/lib/jquery/jquery-2.1.4.js') }}"></script>

    <script src="{{ asset('./storage/lib/pjax/pjax.js') }}"></script>
    <script src="{{ asset('./storage/lib/popper.js/js/popper.js') }}"></script>
    <script src="{{ asset('./storage/lib/bootstrap/js/bootstrap.js') }}"></script>

    <!--script src="{{ asset('./storage/vendor/headroom.js/dist/headroom.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/countup.js/dist/countUp.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/jquery-countdown/dist/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/prismjs/prism.js') }}"></script>
    <script src="{{ asset('./storage/vendor/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}">
    </script>
    <script src="{{ asset('./storage/vendor/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script-->
    <script src="{{ asset('./storage/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
    <script src="{{ asset('./storage/assets/assets/js/rocket.js') }}"></script>
    <script>
        /* const ps = new PerfectScrollbar('#sidebarMenu', {
         wheelSpeed: 2,
         wheelPropagation: true,
         minScrollbarLength: 20
         });*/
        if ($.fn.perfectScrollbar) {
            // alert('')
            $('div.sidebar-sticky').perfectScrollbar({
                suppressScrollX: true
            });
            $(document).pjax('a', 'body')
        }

    </script>
    @yield('scripts')

</body>

</html>
