<div class="clearfix">
    @if(isset($chemin) && is_array($chemin))
         <ol class="breadcrumb slim-breadcrumb mb-0 pb-1">
             <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home"></i> Tableau de bord</a> </li>
             @foreach($chemin as $c)
                @php
                    $url=isset($c['url'])?$c['url']:'';
                    $nom=isset($c['nom'])?$c['nom']:'';
                    $class=$url==""?'active':'';
                @endphp
                 <li class="breadcrumb-item {{ $class }}"><a href="{{url($url)}}">{{__($nom)}}</a></li>
             @endforeach
          </ol>
    @endif
    </div>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mt-2 mb-4">

    <div class="btn-toolbar">

        <button class="btn btn-primary btn-sm mr-2 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"><span class="fas fa-plus mr-2"></span>{{ __('Nouvelle tâche') }}</button>
        <div class="dropdown-menu dashboard-dropdown dropdown-menu-left mt-2">
            <a class="dropdown-item font-weight-bold" href="{{ route('rapport-hebdo.create') }}"><span
                    class="fas fa-calendar-week mr-2"></span>{{ __('Rapport hebdomadaire') }}</a>
            <a class="dropdown-item font-weight-bold" href="{{ route('rapport-mensuel.create') }}"><span
                    class="fas fa-calendar mr-2"></span>{{ __('Rapport Mensuel') }}</a>
            <a class="dropdown-item font-weight-bold" href="{{ route('rapport-semestre.create') }}"><span
                    class="fas fa-calendar-check mr-2"></span>{{ __('Rapport par semestre') }}</a>
            <div role="separator" class="dropdown-divider"></div>
            <a class="dropdown-item font-weight-bold" href="{{ route('getParamExport') }}"><span
                    class="fas fa-clipboard-list text-danger mr-2"></span>{{ __("Paramètres d'exportation") }}</a>
            <a class="dropdown-item font-weight-bold " href="{{ route('config') }}"><span
                    class="fas fa-cog text-danger mr-2"></span>{{ __('Configuration du système ') }}</a>
        </div>
    </div>

    <div class="btn-group mr-2">
		@php
			$langues=config("locale.languages");
			//dd(session('locale'));
		@endphp
		@foreach(config('app.locales') as $locale)
			@if($locale == session('locale'))
				<a type="button" href="{{ route('setLang', $locale) }}" class="btn btn-sm btn-secondary">{{ ($langues[$locale])?$langues[$locale]:$locale }}</a>
			@else
				<a href="{{ route('setLang', $locale) }}" type="button" class="btn btn-sm btn-outline-secondary">{{ ($langues[$locale])?$langues[$locale]:$locale }}</a>
			@endif
		@endforeach
        <!--button type="button" class="btn btn-sm btn-secondary">{{ __('Français') }}</button>
        <button type="button" class="btn btn-sm btn-outline-secondary">{{ __('English') }}</button-->
    </div>
</div>
