@extends('moodler::layouts.app')

@section('title', __('Nouveau paramètre d\'exportation'))

@section('content')
    <div class="bg-soft min-vh-100">
        @include('moodler::include.top')
            <div class="col-lg-9">
                <h6 class="slim-pagetitle">{{ __('Ajout de paramètre d\'exportation') }}</h6>
                <div class="card">
                    <div class="card-body">

                        <form method="GET" action="{{ route('saveParamExport') }}" accept-charset="UTF-8" class="form-inlines" role="search">


                            <div class="mt-1">
                                <!-- Title -->
                                <div class="actions-toolbar py-2 mb-4">
                                    <h5 class="mb-1">{{ __('Nouveau paramètre') }}</h5>
                                    <p class="text-sm text-muted mb-0">{{__("Remplissez ici la structure de l'orgalisation de votre formation")}}</p>
                                </div>
                                <!-- New address form -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="nom_param" class="form-control-label text-right col-md-3 ">
                                                <span class="text-bold">{{ __('Nom du paramètre') }} </span><br>
                                               </label>
                                            <div class="col-md-5">
                                                {{ Form::text('nom_param',"Paramètre ".date('d_m_Y_H-i-s'),['class'=>"form-control form-control-sm"]) }}
                                            </div>
                                        </div>
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="univ" class="form-control-label text-right col-md-3 ">
                                                <span class="text-bold">{{ __('Niveau 1') }} </span><br>
                                                <span class="text-muted">{{ __('Université') }}</span>
                                            </label>
                                            <div class="col-md-3">
                                                <a class="btn btn-sm btn-outline-primary btn-block" href="#"><i class="fa fa-plus-square"></i> {{ __("Ajouter") }}</a>
                                            </div>
                                            <!--div class="col-md-4">
                                                {{ Form::select('univ',isset($univs)?$univs:[],null,['class'=>"custom-select custom-select-sm mt-2"]) }}
                                            </div-->

                                        </div>
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="entite" class="form-control-label text-right col-md-3 ">
                                                <span class="text-bold">{{ __('Niveau 2') }} </span><br>
                                                <span class="text-muted">{{ __('Entité/Etablissement') }}</span>
                                            </label>
                                            <div class="col-md-3">
                                                <a class="btn btn-sm btn-outline-primary btn-block" href="#"><i class="fa fa-plus-square"></i> {{ __("Ajouter") }}</a>
                                            </div>
                                        </div>
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="cylce" class="form-control-label text-right col-md-3 ">
                                                <span class="text-bold">{{ __('Niveau 3') }} </span><br>
                                                <span class="text-muted">{{ __('Cycle de formation') }}</span>
                                            </label>
                                            <div class="col-md-3">
                                                <a class="btn btn-sm btn-outline-primary btn-block" href="#"><i class="fa fa-plus-square"></i> {{ __("Ajouter") }}</a>
                                            </div>
                                        </div>
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="annee_acad" class="form-control-label text-right col-md-3 ">
                                                <span class="text-bold">{{ __('Niveau 4') }} </span><br>
                                                <span class="text-muted">{{ __('Année académique') }}</span>
                                            </label>
                                            <div class="col-md-3">
                                                <a class="btn btn-sm btn-outline-primary btn-block" href="#"><i class="fa fa-plus-square"></i> {{ __("Ajouter") }}</a>
                                            </div>
                                        </div>
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="filiere" class="form-control-label text-right col-md-3 ">
                                                <span class="text-bold">{{ __('Niveau 5') }} </span><br>
                                                <span class="text-muted">{{ __('Filières/Formations') }}</span>
                                            </label>
                                            <div class="col-md-3">
                                                <a class="btn btn-sm btn-outline-primary btn-block" href="#"><i class="fa fa-plus-square"></i> {{ __("Ajouter") }}</a>
                                            </div>
                                        </div>

                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="semestre" class="form-control-label text-right col-md-3 ">
                                                <span class="text-bold">{{ __('Niveau 6') }} </span><br>
                                                <span class="text-muted">{{ __('Semestres') }}</span>
                                            </label>
                                            <div class="col-md-3">
                                                <a class="btn btn-sm btn-outline-primary btn-block" href="#"><i class="fa fa-plus-square"></i> {{ __("Ajouter") }}</a>
                                            </div>
                                        </div>
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="semestre" class="form-control-label text-right col-md-3 ">
                                                <span class="text-bold">{{ __('Niveau 7') }} </span><br>
                                                <span class="text-muted">{{ __('Cours/Matières') }}</span>
                                            </label>
                                            <div class="col-md-3">
                                                <a class="btn btn-sm btn-outline-primary btn-block" href="#"><i class="fa fa-plus-square"></i> {{ __("Ajouter") }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="btn btn-link btn-sm" href="{{ route('getParamExport') }}"><i class="fa fa-chevron-left"></i> {{ __('Retour aux paramètres') }}</a>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn  btn-success">{{ __("Sauvegarder") }}</button>

                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
