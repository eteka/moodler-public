@extends('layouts.app')

@section('title',"Paramètres")

@section('content')
<div class="main">
    <!--div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
           
          </ol>
          <h6 class="slim-pagetitle">
            Configuration
          </h6>
          
    </div-->
    <div class="actions-toolbar py-2 mt-4 mb-4">
      <h5 class="mb-1">{{ __("Paramètres d'exportation") }}</h5>
      <p class="text-sm text-muted mb-0">Use one of your saved addresses for fast checkout.</p>
    </div>
    <div class="row">
    <div class="section-wrapper__ col-md-8">
      <div class="section-wrapper">
      <label class="section-title">{{ __("Mes paramètres d'exportation") }}</label>
      <p class="mg-b-20 mg-sm-b-40">{{ __("Vos paramètres d'exportation apparaisses ici") }}</p>

      <div class="row">
        <div class="col-lg">
          <div class="table-responsive">
            <table class="table table-cards align-items-center">
              <tbody class="list">
                @foreach ($paramexport as $p)
                <tr class="table-divider"></tr>
                <tr>
                  <th scope="row">
                    <div class="custom-control custom-checkbox">
                      <input type="radio" class="custom-control-input" name="radio-address" id="tbl-addresses-check-3">
                      <label class="custom-control-label" for="tbl-addresses-check-3"></label>
                    </div>
                  </th>
                  <td>
                    <span class="font-weight-600 text-dark">Parametres </span></td>
                  <td>
                    <p class="mb-0 text-muted text-limit text-sm"></p>
                  </td>
                  <td>
                    <div class="actions">
                      <div class="dropdown">
                        <a class="action-item" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon ion-more"></i></a>
                        <div class="dropdown-menu dropdown-menu-right">
                          <a class="dropdown-item" href="#"><i class="icon ion-compose text-muted"></i> Edit address</a>
                          <a class="dropdown-item" href="#"><i class="icon ion-trash-a text-danger"></i> Move to trash</a>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr class="table-divider"></tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div><!-- col -->
      </div><!-- row -->

     
    </div>
    </div>
    <div class="col-lg-4">
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col-12">
              
                <label class="custom-control-lasbel text-dark font-weight-bold" for="shipping-standard">{{ __("Ajouter un paramètre") }}</label>
                <p class="text-muted text-sm mt-3 mb-0">{{ __("Un paramètre d'exportation vous permet d'exporter vos données en fonction de la structure de votre plan de formation ") }}</p>
            
              <a class="btn btn-primary btn-block mt-4"  href="{{ route("param-export.create") }}">Nouveau</a>
            </div>
          </div>
         
        </div>
      </div>
    </div>
  </div>
</div>
    <div class="col-lg-8">
      
            <form>
              <!-- Table of addresses -->
              
              <div class="mt-5">
                
                <!-- New address form -->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">First name</label>
                      <input class="form-control" type="text" placeholder="Enter your first name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">Last name</label>
                      <input class="form-control" type="text" placeholder="Also your last name">
                    </div>
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label">Address</label>
                      <input class="form-control" type="text" placeholder="Address, Number">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group focused">
                      <label class="form-control-label">Country</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="Country" data-select2-id="1" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="3">Select your country</option>
                        <option value="1">Bulgaria</option>
                        <option value="2">Croatia</option>
                        <option value="3">Denmark</option>
                        <option value="4">Finland</option>
                        <option value="5">Greece</option>
                        <option value="6">Romania</option>
                        <option value="7">United Kingdom</option>
                      </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="2" style="width: 350px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" title="Country" tabindex="0" aria-labelledby="select2-np79-container"><span class="select2-selection__rendered" id="select2-np79-container" role="textbox" aria-readonly="true" title="Select your country">Select your country</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group focused">
                      <label class="form-control-label">City</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="City" data-select2-id="4" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="6">Select your city</option>
                        <option value="1">Bucharest</option>
                        <option value="2">Bacau</option>
                        <option value="3">Cluj Napoca</option>
                        <option value="4">Piatra Neamt</option>
                        <option value="5">Sibiu</option>
                        <option value="6">Timisoara</option>
                      </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="5" style="width: 350px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" title="City" tabindex="0" aria-labelledby="select2-rr47-container"><span class="select2-selection__rendered" id="select2-rr47-container" role="textbox" aria-readonly="true" title="Select your city">Select your city</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">Postal code</label>
                      <input class="form-control" type="text" placeholder="Address, Number">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">Phone</label>
                      <input class="form-control" type="text" placeholder="+40-777 245 549">
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-5">
                <!-- Title -->
                <div class="actions-toolbar py-2 mb-4">
                  <h5 class="mb-1">Shipping method</h5>
                  <p class="text-sm text-muted mb-0">Fill in your address info for upcoming orders or payments.</p>
                </div>
                <!-- Shipping method options -->
               
              </div>
              <div class="mt-4 text-right">
                <a href="shop-landing.html" class="btn btn-link text-sm text-dark font-weight-bold">Return to shop</a>
                <button type="button" class="btn btn-sm btn-success">Next step</button>
              </div>
            </form>
          </div>
</div>
@endsection