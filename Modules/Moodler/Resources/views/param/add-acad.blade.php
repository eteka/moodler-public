@extends('moodler::layouts.app')

@section('title', __('Ajouter établissement'))

@section('content')
    <div class="bg-soft min-vh-100">
        @include('moodler::include.top')
            <div class="col-lg-9">
                <h6 class="slim-pagetitle">{{ __('Ajout de paramètre d\'exportation') }}</h6>
                <div class="card">
                    <div class="card-body">

                        <form method="POST" action="{{ route('saveAddAcad',$id??0) }}" accept-charset="UTF-8" class="form-inlines" role="search">
                                {{ csrf_field() }}

                            <div class="mt-1">
                                <!-- Title -->
                                <div class="actions-toolbar py-2 mb-4">
                                    <h5 class="mb-1">{{ __('Nouveau paramètre') }}</h5>
                                    <p class="text-sm text-muted mb-0">{{__("Remplissez ici la structure de l'orgalisation de votre formation")}}</p>
                                </div>
                                <!-- New address form -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="nom_param" class="form-control-label text-right col-md-4 ">
                                                <span class="text-bold">{{ __('Nom du paramètre') }} </span><br>
                                               </label>
                                            <div class="col-md-5">
                                                {{ Form::text('snom',isset($pnom)?$pnom:'-',['class'=>"form-control text-bold rounded-sm form-control-sm",'disabled'=>"disabled"]) }}
                                            </div>
                                        </div>
                                        <div class="form-group border-bottom pb-2 row">
                                            <label for="univ" class="form-control-label text-right col-md-4 ">
                                                <span class="text-bold">{{ __('Niveau 4') }} </span><br>
                                                <span class="text-info">{{ __('Sélectionnez une année académique') }}</span>
                                            </label>

                                            <div class="col-md-4">
                                                {{ Form::select('acad',isset($univs)?$univs:[],null,['class'=>"custom-select custom-select-sm mt-2"]) }}
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="btn btn-link btn-sm" href="{{ route('editParamExport',$id) }}"><i class="fa fa-chevron-left"></i> {{ __('Retour') }}</a>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn  btn-success">{{ __("Sauvegarder") }}</button>

                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
