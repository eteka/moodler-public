@extends('moodler::layouts.app')

@section('title',__("Créer un  compte"))
@section('nav','')
@section('ccontent', '')

@section('content')
<div class="bg-softs">
    <section class="vh-100 bg-soft d-flex align-items-center">
        <div class="container">
        <div class="row justify-content-center form-bg-image" data-background="../assets/img/illustrations/signin.svg" style="background-image: url(&quot;../assets/img/illustrations/signin.svg&quot;);">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="signin-inner mt-3 mt-lg-0 bg-white shadow-soft border border-light rounded p-4 p-lg-5 w-100 fmxw-450">
                    <div class="text-center text-md-center mb-4 mt-md-0">
                        <h1 class=""><span class="text-primary">Moodle</span><span class="text-danger">R</span></h1>
                    <h3 class="mb-1 h5">{{ __('Création de compte sur MoodleR') }}</h3>
                    <p class="text-gray small">{{ __("Veuillez renseigner vos informations pour continuer") }}</p>
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
        <div class="signin-box">

        <div class="form-group row mg-b-10">
        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="far fa-user"></i></span></div>
                <input id="nom" type="text"  placeholder="{{__('Nom de famille')}}" class="form-control @error('nom') is-invalid @enderror" name="nom" value="{{ old('nom') }}" required autocomplete="nom" autofocus>
            </div>
                @error('nom')
                    <span class="invalid-feedback col-sm-12" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-user-shield"></i></span></div>
              <input id="prenom" type="text"  placeholder="{{__('Prénom')}}" class="form-control @error('prenom') is-invalid @enderror" name="prenom" value="{{ old('prenom') }}" required autocomplete="prenom" >
            </div>

                @error('prenom')
                    <span class="invalid-feedback col-sm-12" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        </div>
        <div class="form-group row row-xs mg-b-10">
            <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-envelope"></i></span></div>
                    <input id="email"  type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" class="form-control" placeholder="{{__('Courrier électronique')}}">
                </div>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        </div>

        <div class="form-group row row-xs mg-b-10">
            <div class="col-sm">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-unlock-alt"></i></span></div>
                    <input id="password" type="password" placeholder="{{__('Mot de passe')}}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                </div>
            </div>
            <div class="col-sm mg-t-10 mg-sm-t-0">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-unlock-alt"></i></span></div>
                        <input id="password-confirm" type="password" placeholder="{{__('Confirmation du mot depasse')}}" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
        </div>
        <div class="col-md-12">
              @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
        </div>
        </div><!-- row -->

        <button class="btn btn-danger btn-block btn-signin mt-4">{{__('Créer un compte')}}</button>
       <hr>


        <p class="mg-t-40 mg-b-0 text-center small">{{__('Avez-vous déjà un compte ?')}} <a href="{{url('login')}}">{{__('Connectez-vous')}}</a></p>
        </div>
        <!-- signin-box -->
    </form>
                </div>
            </div>
        </div>
        </div>
    </section>
    </div>

    <div class="signin-wrapper card-body">

</div>
@endsection
