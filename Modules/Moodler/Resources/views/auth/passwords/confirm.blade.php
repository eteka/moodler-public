@extends('moodler::layouts.app-light')

@section('title',__('Confirmation de mot de passe'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card mt-6 mb-6 card border-light shadow-sm shadow-hover">
                <div class="card-header h5 border-bottom ">{{ __('Confirmation de mot de passe') }}</div>

                <div class="card-body">
                   <p class="text-center alert alert-gray-100"> <i class="fas fa-info-circle"></i> {{ __('Veuillez confirmer votre mot de passe pour continuer') }}</p>

                    <form method="POST" action="{{ url('password.confirm') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                            <div class="col-md-7">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirmer mot de passe') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link pl-0 mt-3" href="{{ route('password.request') }}">
                                        {{ __('Mot de passe oublié  ?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
