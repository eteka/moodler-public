@extends('moodler::layouts.app-light')

@section('title',__('Réinitialisation de mot de passe'))

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-7 align-content-between">
            <div class="card shadow-hover border mt-6 mb-6">
                <div class="card-header border-bottom h5">{{ __('Réinitialisation de mot de passe') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Entrer votre adresse email') }}</label>

                            <div class="col-md-7">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Soumettre la requête') }}
                                </button>
                                <ul class="list-unstyled mt-3">
                                    <li><a href="{{ route('login') }}">Se connecter</a></li>
                                    <!--li><a href="#">Se connecter</a></li-->
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
