@extends('moodler::layouts.app')
@section('title', __('Rapport Hebdomadaire'))

@section('content')
    <div class="bg-soft">
        @include('moodler::include.top')
        <section class="">
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                        <div class="d-block mb-4 mb-md-0">
                            <h2 class="h3  page-header">{{ __('Rapport Hebdomadaire') }}</h2>
                        </div>
                    </div>
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="GET" action="{{ route('rapport-hebdo.create') }}" accept-charset="UTF-8"
                        class="form-horizontal" enctype="multipart/form-data">

                        <div class="card border-light shadow-sm">
                            <div class="card-body__ col-md-10">
                                <div class="table-responsive p-3">
                                    <table class="table table-centered table-nowrap table-sm mb-0 rounded">
                                        <thead class="thead-light__">

                                            <tr>
                                                <th class="border-0">
                                                    {{ Form::select('univ', isset($universite) ? $universite : [], null, ['class' => 'custom-select custom-select-sm ml-1']) }}

                                                </th>
                                                <th class="border-0">
                                                    {{ Form::select('entite', isset($lentite) ? $lentite : [], null, ['class' => 'custom-select custom-select-sm ml-1']) }}

                                                </th>

                                                <th class="border-0">
                                                    {{ Form::select('cycle', isset($lecycle) ? $lecycle : [], null, ['class' => 'custom-select custom-select-sm ml-1']) }}

                                                </th>
                                                <td class="border-0"></td>
                                            </tr>
                                            <tr>

                                                <th class="border-0">
                                                    {{ Form::select('acad', isset($annee_academique) ? $annee_academique : [], null, ['class' => 'custom-select custom-select-sm ml-1']) }}

                                                </th>

                                                <!--th class="border-0">
                                                          {{ Form::select('filiere', isset($filiere) ? $filiere : [], isset($_GET['filiere']) ? $_GET['filiere'] : null, ['class' => 'custom-select custom-select-sm ml-1']) }}

                                                    </th-->
                                                <th class="border-0">
                                                    <input type="text" placeholder="{{ __('Date début de la semaine') }}"
                                                        required="required"
                                                        value="{{ isset($_GET['date']) ? $_GET['date'] : null }}"
                                                        name="date"
                                                        class="form-control input-sm form-control-sm datepicker">
                                                </th>

                                                <th class="border-0" nowrap>
                                                    <button name="generer" type="submit"
                                                        class="btn btn-block ml-2 btn-sm btn-secondary"> <i
                                                            class="fa fa-database"></i> {{ __('Générer') }}</button>
                                                </th>

                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-12">
                    <div class="card card-body bg-white border-light shadow-sm mb-4">

                        <div class="h-100vhw-100">
                            <div
                                class="col-sm-12 d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                                @if (isset($html))
                                    <div class="btn-toolbars mr-2">

                                        <button class="btn btn-outline-primary font-weight-bold btn-sm mr-2 dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="fas fa-file-export mr-2"></span>{{ __('Exporter') }}</button>
                                        <div class="dropdown-menu dashboard-dropdown dropdown-menu-left mt-2">
                                            <a id="exp_xlsx" class="dropdown-item text-success" onclick="exportExcel()"
                                                href="#"><span
                                                    class="fas fa-file-excel mr-2"></span>{{ __('Format Excel') }}</a>
                                            {{-- <a class="dropdown-item font-weight-bold"
                                                href="{{ route('rapport-hebdo.create') }}"><span
                                                    class="fa fa-file-excel mr-2"></span>{{ __('Format Excel (CSV)') }}</a>
                                            --}}
                                            <a id="exp_pdf" class="dropdown-item font-weight-bold text-danger"
                                                href="#"><span
                                                    class="fa fa-file-pdf mr-2"></span>{{ __('Format PDF ') }}</a>
                                            <a id="exp_word" class="dropdown-item font-weight-bold text-info" href="#"><span
                                                    class="fa fa-file-word mr-2"></span>{{ __('Format Word') }}</a>

                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-12">
                                @if (isset($html))
                                    {!! $html !!}
                                @else
                                    <span class="text-muted text-center">
                                        Les rapports appararaissent ici
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div id="elementH"></div>

                    </div>
                </div>
            </div>

        </section>

    </div>
@endsection
@section('scripts')
    <link rel="stylesheet" href="{{ asset('storage/lib/datepicker/css/bootstrap-datepicker.min.css') }}">
    <script src="{{ asset('storage/lib/datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-7d'
        });

    </script>
    <script src="{{ asset('storage/lib/tableToExcel/js/tableToExcel.js') }}"></script>
    <script src="{{ asset('storage/lib/jsPdf/js/jspdf.js') }}"></script>
    <script src="{{ asset('storage/lib/tableExport/tableHTMLExport.js') }}"></script>
    <script>
        /******************************
         *
         *
         *
         * */
        $(document).ready(function() {
            $('#exp_pdf').click(function() {
                demoFromHTML1();
                //$("#tb_moodler").tableHTMLExport({type:'pdf',filename:'reapport_hebdomadaire.pdf'});
                /*var specialElementHandlers =
                    function(element, renderer) {
                        return true;
                    }
                var doc = new jsPDF();
                //alert("")
                doc.fromHTML($('#tb_moodler_main').html(), 15, 15, {
                    'width': 170,
                    'elementHandlers': specialElementHandlers
                });
                doc.output('datauri');*/
            });

        });

        function demoFromHTMLs() {
            var printDoc = new jsPDF();
            //alert("")
            printDoc.fromHTML($('#tb_moodler_main').get(0), 10, 10, {
                'width': 880
            });
            printDoc.autoPrint();
            printDoc.output(
            "dataurlnewwindow"); // this opens a new popup,  after this the PDF opens the print window view but there are browser inconsistencies with how this is handled

        }

        function demoFromHTML1() {
            var pdf = new jsPDF('p', 'pt', 'letter');
            // source can be HTML-formatted string, or a reference
            // to an actual DOM element from which the text will be scraped.
            source = $('#tb_moodler_main')[0];


            // we support special element handlers. Register them with jQuery-style
            // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
            // There is no support for any other type of selectors
            // (class, of compound) at this time.
            specialElementHandlers = {
                // element with id of "bypass" - jQuery style selector
                '#bypassme': function(element, renderer) {
                    // true = "handled elsewhere, bypass text extraction"
                    return true
                }
            };
            margins = {
                top: 80,
                bottom: 60,
                left: 40,
                width: 522
            };
            // all coords and widths are in jsPDF instance's declared units
            // 'inches' in this case
            pdf.fromHTML(
                source, // HTML string or DOM elem ref.
                margins.left, // x coord
                margins.top, { // y coord
                    'width': margins.width, // max width of content on PDF
                    'elementHandlers': specialElementHandlers
                },
                function(dispose) {
                    // dispose: object with X, Y of the last line add to the PDF
                    //          this allow the insertion of new lines after html
                    pdf.save('Rapport_Hebdomadaire.pdf');
                }, margins);
        }

        /* HTML to Microsoft Word Export Demo
         * This code demonstrates how to export an html element to Microsoft Word
         * with CSS styles to set page orientation and paper size.
         * Tested with Word 2010, 2013 and FireFox, Chrome, Opera, IE10-11
         * Fails in legacy browsers (IE<10) that lack window.Blob object
         */
        function exportExcel() {
            TableToExcel.convert(document.getElementById('tb_moodler_main'));
        }

        window.exp_word.onclick = function() {
            if (!window.Blob) {
                alert('Your legacy browser does not support this action.');
                return;
            }

            var html, link, blob, url, css;

            // EU A4 use: size: 841.95pt 595.35pt;
            // US Letter use: size:11.0in 8.5in;

            css = (
                '<style>' +
                '@page WordSection1{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}' +
                'div.WordSection1 {page: WordSection1;}' +
                'table{border-collapse:collapse;}td{border:1px gray solid;width:5em;padding:2px;}' +
                '</style>'
            );

            html = window.tb_moodler.innerHTML;
            blob = new Blob(['\ufeff', css + html], {
                type: 'application/msword'
            });
            url = URL.createObjectURL(blob);
            link = document.createElement('A');
            link.href = url;
            // Set default file name.
            // Word will append file extension - do not add an extension here.
            link.download = 'Document';
            document.body.appendChild(link);
            if (navigator.msSaveOrOpenBlob) navigator.msSaveOrOpenBlob(blob, 'Rapport_Moodler.doc'); // IE10-11
            else link.click(); // other browsers
            document.body.removeChild(link);
        };

    </script>
@endsection
