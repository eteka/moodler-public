@extends('layouts.app')
@section('title', 'Statistiques')

@section('content')
    <div class="bg-soft">
        @include('moodler.include.top')
        <section class="bg-soft">
            <main class="">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                    <div class="d-block mb-4 mb-md-0">
                        <h2 class="h3">{{ __('Statistiques') }}</h2>
                    </div>
                </div>
                <div class="row  h-100vh">
                    <div class="col-12 col-xl-6 mb-4">
                        <div class="card border-light shadow-sm">
                            <div class="card-body d-flex flex-row align-items-center flex-0 border-bottom">
                                <div class="d-block">
                                    <h2 class="h5">App Ranking</h2>
                                    <div class="small mt-2 mb-3"><span class="fas fa-angle-up text-success"></span> <span
                                            class="text-success font-weight-bold">155</span> <span
                                            class="font-weight-bold ml-2"><span class="icon icon-small mr-1"><span
                                                    class="fas fa-globe-europe"></span></span>WorldWide</span></div>
                                    <div class="d-flex">
                                        <div class="d-flex align-items-center mr-3 lh-130"><span
                                                class="shape-xs rounded-circle bg-tertiary mr-2"></span> <span
                                                class="font-weight-normal small">All</span></div>
                                        <div class="d-flex align-items-center mr-3 lh-130"><span
                                                class="shape-xs rounded-circle bg-secondary mr-2"></span> <span
                                                class="font-weight-normal small">Travel &amp; Local</span></div>
                                        <div class="d-flex align-items-center mr-3 lh-130"><span
                                                class="shape-xs rounded-circle bg-primary mr-2"></span> <span
                                                class="font-weight-normal small">Widgets</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-2">
                                <div class="ct-chart-app-ranking ct-major-tenth ct-series-a">
                                    <div class="chartist-tooltip" style="top: -13.0333px; left: 50.2px;"><span
                                            class="chartist-tooltip-value">4</span></div><svg
                                        xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%"
                                        class="ct-chart-bar" style="width: 100%; height: 100%;">
                                        <g class="ct-grids">
                                            <line x1="10" x2="10" y1="15" y2="197.79998779296875"
                                                class="ct-grid ct-horizontal"></line>
                                            <line x1="89.57142857142857" x2="89.57142857142857" y1="15"
                                                y2="197.79998779296875" class="ct-grid ct-horizontal"></line>
                                            <line x1="169.14285714285714" x2="169.14285714285714" y1="15"
                                                y2="197.79998779296875" class="ct-grid ct-horizontal"></line>
                                            <line x1="248.71428571428572" x2="248.71428571428572" y1="15"
                                                y2="197.79998779296875" class="ct-grid ct-horizontal"></line>
                                            <line x1="328.2857142857143" x2="328.2857142857143" y1="15"
                                                y2="197.79998779296875" class="ct-grid ct-horizontal"></line>
                                            <line x1="407.85714285714283" x2="407.85714285714283" y1="15"
                                                y2="197.79998779296875" class="ct-grid ct-horizontal"></line>
                                            <line x1="487.42857142857144" x2="487.42857142857144" y1="15"
                                                y2="197.79998779296875" class="ct-grid ct-horizontal"></line>
                                        </g>
                                        <g>
                                            <g class="ct-series ct-series-a">
                                                <line x1="34.785714285714285" x2="34.785714285714285"
                                                    y1="197.79998779296875" y2="106.39999389648438" class="ct-bar"
                                                    ct:value="5"></line>
                                                <line x1="114.35714285714286" x2="114.35714285714286"
                                                    y1="197.79998779296875" y2="124.67999267578125" class="ct-bar"
                                                    ct:value="4"></line>
                                                <line x1="193.92857142857142" x2="193.92857142857142"
                                                    y1="197.79998779296875" y2="142.95999145507812" class="ct-bar"
                                                    ct:value="3"></line>
                                                <line x1="273.5" x2="273.5" y1="197.79998779296875" y2="69.83999633789062"
                                                    class="ct-bar" ct:value="7"></line>
                                                <line x1="353.07142857142856" x2="353.07142857142856"
                                                    y1="197.79998779296875" y2="106.39999389648438" class="ct-bar"
                                                    ct:value="5"></line>
                                                <line x1="432.6428571428571" x2="432.6428571428571" y1="197.79998779296875"
                                                    y2="15" class="ct-bar" ct:value="10"></line>
                                                <line x1="512.2142857142858" x2="512.2142857142858" y1="197.79998779296875"
                                                    y2="142.95999145507812" class="ct-bar" ct:value="3"></line>
                                            </g>
                                            <g class="ct-series ct-series-b">
                                                <line x1="49.785714285714285" x2="49.785714285714285"
                                                    y1="197.79998779296875" y2="161.239990234375" class="ct-bar"
                                                    ct:value="2"></line>
                                                <line x1="129.35714285714286" x2="129.35714285714286"
                                                    y1="197.79998779296875" y2="161.239990234375" class="ct-bar"
                                                    ct:value="2"></line>
                                                <line x1="208.92857142857142" x2="208.92857142857142"
                                                    y1="197.79998779296875" y2="179.51998901367188" class="ct-bar"
                                                    ct:value="1"></line>
                                                <line x1="288.5" x2="288.5" y1="197.79998779296875" y2="106.39999389648438"
                                                    class="ct-bar" ct:value="5"></line>
                                                <line x1="368.07142857142856" x2="368.07142857142856"
                                                    y1="197.79998779296875" y2="142.95999145507812" class="ct-bar"
                                                    ct:value="3"></line>
                                                <line x1="447.6428571428571" x2="447.6428571428571" y1="197.79998779296875"
                                                    y2="124.67999267578125" class="ct-bar" ct:value="4"></line>
                                                <line x1="527.2142857142858" x2="527.2142857142858" y1="197.79998779296875"
                                                    y2="161.239990234375" class="ct-bar" ct:value="2"></line>
                                            </g>
                                            <g class="ct-series ct-series-c">
                                                <line x1="64.78571428571428" x2="64.78571428571428" y1="197.79998779296875"
                                                    y2="142.95999145507812" class="ct-bar" ct:value="3"></line>
                                                <line x1="144.35714285714286" x2="144.35714285714286"
                                                    y1="197.79998779296875" y2="161.239990234375" class="ct-bar"
                                                    ct:value="2"></line>
                                                <line x1="223.92857142857142" x2="223.92857142857142"
                                                    y1="197.79998779296875" y2="33.279998779296875" class="ct-bar"
                                                    ct:value="9"></line>
                                                <line x1="303.5" x2="303.5" y1="197.79998779296875" y2="106.39999389648438"
                                                    class="ct-bar" ct:value="5"></line>
                                                <line x1="383.07142857142856" x2="383.07142857142856"
                                                    y1="197.79998779296875" y2="124.67999267578125" class="ct-bar"
                                                    ct:value="4"></line>
                                                <line x1="462.6428571428571" x2="462.6428571428571" y1="197.79998779296875"
                                                    y2="88.1199951171875" class="ct-bar" ct:value="6"></line>
                                                <line x1="542.2142857142858" x2="542.2142857142858" y1="197.79998779296875"
                                                    y2="124.67999267578125" class="ct-bar" ct:value="4"></line>
                                            </g>
                                        </g>
                                        <g class="ct-labels">
                                            <foreignObject style="overflow: visible;" x="10" y="202.79998779296875"
                                                width="79.57142857142857" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 80px; height: 20px;">21 Apr</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="89.57142857142857"
                                                y="202.79998779296875" width="79.57142857142857" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 80px; height: 20px;">21 Ap</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="169.14285714285714"
                                                y="202.79998779296875" width="79.57142857142858" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 80px; height: 20px;">22 Ap</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="248.71428571428572"
                                                y="202.79998779296875" width="79.57142857142856" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 80px; height: 20px;">23 Ap</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="328.2857142857143"
                                                y="202.79998779296875" width="79.57142857142856" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 80px; height: 20px;">24 Ap</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="407.85714285714283"
                                                y="202.79998779296875" width="79.57142857142861" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 80px; height: 20px;">25 Ap</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="487.42857142857144"
                                                y="202.79998779296875" width="79.57142857142856" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 80px; height: 20px;">26 Ap</span></foreignObject>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-xl-6 mb-4">
                        <div class="row">
                            <div class="col-12 mb-4">
                                <div class="card border-light shadow-sm">
                                    <div class="card-body">
                                        <div class="row d-block d-md-flex align-items-center">
                                            <div class="col-12 col-md-5">
                                                <h2 class="h5 mb-1">Average Rating</h2>
                                                <h3 class="h1 mb-1">4.5</h3>
                                                <div class="mb-2"><span class="fas fa-star text-warning"></span> <span
                                                        class="fas fa-star text-warning"></span> <span
                                                        class="fas fa-star text-warning"></span> <span
                                                        class="fas fa-star text-warning"></span> <span
                                                        class="fas fa-star-half-alt text-warning"></span></div><span
                                                    class="small">Based on <span
                                                        class="font-weight-bold text-dark">103,456</span> ratings</span>
                                            </div>
                                            <div class="col-12 col-md-7 mt-3 mt-md-0">
                                                <div class="col-12">
                                                    <div class="row d-flex align-items-center mb-1">
                                                        <div class="col-2 text-gray font-weight-bold px-0 small">5<span
                                                                class="fas fa-star ml-1"></span></div>
                                                        <div class="col-10 px-0">
                                                            <div class="progress progress-lg rounded mb-0">
                                                                <div class="progress-bar bg-success rounded"
                                                                    role="progressbar" aria-valuenow="51" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width: 51%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row d-flex align-items-center mb-1">
                                                        <div class="col-2 text-gray font-weight-bold px-0 small">4<span
                                                                class="fas fa-star ml-1"></span></div>
                                                        <div class="col-10 px-0">
                                                            <div class="progress progress-lg rounded mb-0">
                                                                <div class="progress-bar bg-cyan rounded" role="progressbar"
                                                                    aria-valuenow="31" aria-valuemin="0" aria-valuemax="100"
                                                                    style="width: 31%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row d-flex align-items-center mb-1">
                                                        <div class="col-2 text-gray font-weight-bold px-0 small">3<span
                                                                class="fas fa-star ml-1"></span></div>
                                                        <div class="col-10 px-0">
                                                            <div class="progress progress-lg rounded mb-0">
                                                                <div class="progress-bar bg-warning rounded"
                                                                    role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width: 20%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row d-flex align-items-center mb-1">
                                                        <div class="col-2 text-gray font-weight-bold px-0 small">2<span
                                                                class="fas fa-star ml-1"></span></div>
                                                        <div class="col-10 px-0">
                                                            <div class="progress progress-lg rounded mb-0">
                                                                <div class="progress-bar bg-orange rounded"
                                                                    role="progressbar" aria-valuenow="10" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width: 10%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row d-flex align-items-center mb-1">
                                                        <div class="col-2 text-gray font-weight-bold px-0 small">1<span
                                                                class="fas fa-star ml-1"></span></div>
                                                        <div class="col-10 px-0">
                                                            <div class="progress progress-lg rounded mb-0">
                                                                <div class="progress-bar bg-danger rounded"
                                                                    role="progressbar" aria-valuenow="6" aria-valuemin="0"
                                                                    aria-valuemax="100" style="width: 6%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 mb-4 mb-md-0">
                                <div class="card border-light shadow-sm">
                                    <div class="card-body">
                                        <h2 class="h5">Total Installs</h2>
                                        <h3 class="h2 mb-1">367,567</h3>
                                        <div class="small mb-3">Feb 1 - Apr 1, <span class="icon icon-small"><span
                                                    class="fas fa-globe-europe"></span></span> WorldWide</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="card border-light shadow-sm">
                                    <div class="card-body">
                                        <h2 class="h5">Related Sites</h2>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-1"><span class="icon icon-small text-google w-20 mr-1"><span
                                                        class="fab fa-google"></span></span><a href="#">google.com</a></li>
                                            <li class="mb-1"><span class="icon icon-small text-facebook w-20 mr-1"><span
                                                        class="fab fa-facebook-f"></span></span><a href="#">facebook.com</a>
                                            </li>
                                            <li class="mb-1"><span class="icon icon-small text-twitter w-20 mr-1"><span
                                                        class="fab fa-twitter"></span></span><a href="#">twitter.com</a>
                                            </li>
                                            <li class="mb-1"><span class="icon icon-small text-youtube w-20 mr-1"><span
                                                        class="fab fa-youtube"></span></span><a href="#">youtube.com</a>
                                            </li>
                                            <li><span class="icon icon-small text-dribbble w-20 mr-1"><span
                                                        class="fab fa-dribbble"></span></span><a href="#">dribbble.com</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </section>
    </div>
@endsection
