@extends('layouts.app')
@section('title', 'Rapport mensuel')

@section('content')
    <div class="bg-soft">
        <section class="vh-100 d-flex align-items-center justify-content-center">
            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="card card-body bg-white border-light shadow-sm mb-4">
                        <h2 class="h5 mb-4">General information</h2>
                        <form>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group"><label for="first_name">First Name</label> <input
                                            class="form-control" id="first_name" type="text"
                                            placeholder="Enter your first name" required=""></div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group"><label for="last_name">Last Name</label> <input
                                            class="form-control" id="last_name" type="text"
                                            placeholder="Also your last name" required=""></div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group"><label for="birthday">Birthday</label> <input type="text"
                                            class="form-control flatpickr-input" id="birthday" data-toggle="date"
                                            placeholder="Select your birth date" required=""></div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group"><label for="gender">Gender</label> <select class="custom-select"
                                            id="gender">
                                            <option disabled="disabled" selected="selected">Select option</option>
                                            <option value="1">Female</option>
                                            <option value="2">Male</option>
                                        </select></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group"><label for="email">Email</label> <input class="form-control"
                                            id="email" type="email" placeholder="name@company.com" required=""></div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group"><label for="phone">Phone</label> <input class="form-control"
                                            id="phone" type="number" placeholder="+12-345 678 910" required=""></div>
                                </div>
                            </div>
                            <h2 class="h5 my-4">Adress</h2>
                            <div class="row">
                                <div class="col-sm-9 mb-3">
                                    <div class="form-group"><label for="address">Address</label> <input class="form-control"
                                            id="address" type="text" placeholder="Enter your home address" required="">
                                    </div>
                                </div>
                                <div class="col-sm-3 mb-3">
                                    <div class="form-group"><label for="number">Number</label> <input class="form-control"
                                            id="number" type="number" placeholder="No." required=""></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 mb-3">
                                    <div class="form-group"><label for="city">City</label> <input class="form-control"
                                            id="city" type="text" placeholder="City" required=""></div>
                                </div>
                                <div class="col-sm-4 mb-3">
                                    <div class="form-group focused"><label for="country">Country</label> <select
                                            class="form-control select2-hidden-accessible" id="country" data-toggle="select"
                                            title="Country" data-live-search="true" data-live-search-placeholder="Country"
                                            data-select2-id="1" tabindex="-1" aria-hidden="true">
                                            <option data-select2-id="3">United States</option>
                                            <option>Canada</option>
                                            <option>Germany</option>
                                            <option>Spain</option>
                                            <option>Italy</option>
                                            <option>UK</option>
                                        </select></div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group"><label for="zip">ZIP</label> <input class="form-control"
                                            id="zip" type="tel" placeholder="ZIP" required=""></div>
                                </div>
                            </div>
                            <div class="mt-3"><button type="submit" class="btn btn-primary">Save All</button></div>
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
