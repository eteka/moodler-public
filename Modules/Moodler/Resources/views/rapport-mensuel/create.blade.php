@extends('layouts.app')
@section('title', 'Rapport mensuel')

@section('content')
    <div class="content bg-soft">
        @include('moodler.include.top')
        <section class="">
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="card border-light shadow-sm">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-centered table-nowrap mb-0 rounded">
                                    <thead class="thead-light">
                                        <tr>
                                            <th class="border-0">
                                                <select class="custom-select"
                                                    id="gender">
                                                    <option disabled="disabled" selected="selected">Select option</option>
                                                    <option value="1">Unviversité de Parakou</option>
                                                    <option value="2">Unviversité d'abomey-calavi</option>
                                                </select>
                                            </th>
                                            <th class="border-0">
                                                <select class="custom-select"
                                                    id="gender">
                                                    <option disabled="disabled" selected="selected">Select option</option>
                                                    <option value="1">Faculté de droit et de sciences Physiques</option>
                                                    <option value="2">IUT Parakou</option>
                                                </select>
                                            </th>
                                            <th class="border-0">
                                                <select class="custom-select"
                                                    id="gender">
                                                    <option disabled="disabled" selected="selected">Select option</option>
                                                    <option value="1">Licence</option>
                                                    <option value="2">Master</option>
                                                    <option value="2">Doctorat</option>
                                                </select>
                                            </th>
                                            <th class="border-0">
                                                <select class="custom-select"
                                                    id="gender">
                                                    <option disabled="disabled" selected="selected">Select option</option>
                                                    <option value="1">2017-2018</option>
                                                    <option value="2">2018-2019</option>
                                                    <option value="2">2019-2020</option>
                                                </select>
                                            </th>
                                            <th>
                                                <button type="submit" class="btn btn-md btn-secondary">Générer</button>
                                            </th>
                                            
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-12">
                    <div class="card card-body bg-white border-light shadow-sm mb-4">
                        <div class="h-100vh w-100"></div>
                        
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
