@extends('moodler::layouts.app-light')

@section('title', __('Documentation'))

@section('content')


    <main>

        <section class="section-header bg-primary pb-9 pb-lg-11 text-white">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <h1 class="mb-3">{{ __('Toute la documentation sur MoodleR') }}</h1>
                        <p class="lead px-lg-5 mb-5">{{ __('L\'assistance Technique qu\'il vous faut pour une utilisation simple et facile') }}</p>
                        <form action="#">
                            <div class="form-group bg-white shadow-soft rounded-pill mb-4 px-3 py-2">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <div class="input-group input-group-merge shadow-none">
                                            <div class="input-group-prepend"><span
                                                    class="input-group-text bg-transparent border-0"><i
                                                        class="fas fa-search"></i></span></div><input type="text"
                                                class="form-control border-0 form-control-flush shadow-none pb-2"
                                                placeholder="{{ __('Rechercher dans la documentation...') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-auto"><button type="submit"
                                            class="btn btn-block btn-primary rounded-pill">{{ __('Rechercher') }}</button></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="pattern bottom"></div>
        </section>
        <section class="section section-md pb-0">
            <div class="container z-2 mt-n9 mt-lg-n11">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3 mb-4 mb-lg-0"> <a href="#"
                            class="card text-center shadow-soft border-light animate-up-3">
                            <div class="card-body p-5">
                                <div class="icon icon-shape icon-shape-primary rounded-circle mb-4"><span
                                        class="fas fa-list-alt"></span></div>
                                <h6 class="text-gray mb-0">{{ __('Pré requis') }}</h6>
                            </div>
                        </a></div>
                    <div class="col-12 col-md-6 col-lg-3 mb-4 mb-lg-0"> <a href="#"
                            class="card text-center shadow-soft border-light animate-up-3">
                            <div class="card-body p-5">
                                <div class="icon icon-shape icon-shape-primary rounded-circle mb-4"><span
                                        class="fas fa-cogs"></span></div>
                                <h6 class="text-gray mb-0">{{ __('Configurations') }}</h6>
                            </div>
                        </a></div>
                    <div class="col-12 col-md-6 col-lg-3 mb-4 mb-lg-0"> <a href="#"
                            class="card text-center shadow-soft border-light animate-up-3">
                            <div class="card-body p-5">
                                <div class="icon icon-shape icon-shape-primary rounded-circle mb-4"><span
                                        class="fas fa-file-excel"></span></div>
                                <h6 class="text-gray mb-0">{{ __('Exportation') }}</h6>
                            </div>
                        </a></div>
                    <div class="col-12 col-md-6 col-lg-3"> <a href="#"
                            class="card text-center shadow-soft border-light animate-up-3">
                            <div class="card-body p-5">
                                <div class="icon icon-shape icon-shape-primary rounded-circle mb-4"><span
                                        class="fas fa-hammer"></span></div>
                                <h6 class="text-gray mb-0">{{ __('Autres fonctionnalités') }}</h6>
                            </div>
                        </a></div>
                </div>
            </div>
        </section>
        <section class="section hidden section-md bg-white">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 mb-3">
                        <h5>Getting Started</h5>
                    </div>
                    <div class="col-lg-10 mb-3"><a href="./support-topic.html"
                            class="card border-light animate-up-3 shadow-soft p-0 p-lg-4">
                            <div class="card-body">
                                <h4>Account Settings, Language &amp; Dark Mode</h4>
                                <p class="lead text-gray mb-4">How to edit account settings such as email addresss, user
                                    name, language and switch to dark mode</p>
                                <div class="d-flex align-items-center">
                                    <div class="avatar-lg"><img class="rounded-circle"
                                            src="../assets/img/team/profile-picture-1.jpg" alt="avatar"></div>
                                    <div class="small text-gray ml-3">
                                        <div><span>Updated 2 days ago</span></div>
                                        <div>Written by&nbsp;<strong>Richard Thomas</strong></div>
                                    </div>
                                </div>
                            </div>
                        </a></div>
                    <div class="col-lg-10 mb-3"><a href="./support-topic.html"
                            class="card border-light animate-up-3 shadow-soft p-0 p-lg-4">
                            <div class="card-body">
                                <h4>Creating and managing projects: Project Dashboard</h4>
                                <p class="lead text-gray mb-4">Hot to create a new project and manage its settings on
                                    the project dashboard</p>
                                <div class="d-flex align-items-center">
                                    <div class="avatar-lg"><img class="rounded-circle"
                                            src="../assets/img/team/profile-picture-2.jpg" alt="avatar"></div>
                                    <div class="small text-gray ml-3">
                                        <div><span>Updated over a week ago</span></div>
                                        <div>Written by&nbsp;<strong>Richard Thomas</strong></div>
                                    </div>
                                </div>
                            </div>
                        </a></div>
                    <div class="col-md-10 mt-5 mb-3">
                        <h5>Adding & Styling Elements</h5>
                    </div>
                    <div class="col-lg-10 mb-3"><a href="./support-topic.html"
                            class="card border-light animate-up-3 shadow-soft p-0 p-lg-4">
                            <div class="card-body">
                                <h4>Add & style boxes &amp; other elements</h4>
                                <p class="lead text-gray mb-4">Hot to create a new project and manage its settings on
                                    the project dashboard</p>
                                <div class="d-flex align-items-center">
                                    <div class="avatar-lg"><img class="rounded-circle"
                                            src="../assets/img/team/profile-picture-3.jpg" alt="avatar"></div>
                                    <div class="small text-gray ml-3">
                                        <div><span>Updated over a week ago</span></div>
                                        <div>Written by&nbsp;<strong>Richard Thomas</strong></div>
                                    </div>
                                </div>
                            </div>
                        </a></div>
                    <div class="col-lg-10 mb-3"><a href="./support-topic.html"
                            class="card border-light animate-up-3 shadow-soft p-0 p-lg-4">
                            <div class="card-body">
                                <h4>Add &amp; style images</h4>
                                <p class="lead text-gray mb-4">Hot to create a new project and manage its settings on
                                    the project dashboard</p>
                                <div class="d-flex align-items-center">
                                    <div class="avatar-lg"><img class="rounded-circle"
                                            src="../assets/img/team/profile-picture-4.jpg" alt="avatar"></div>
                                    <div class="small text-gray ml-3">
                                        <div><span>Updated over a week ago</span></div>
                                        <div>Written by&nbsp;<strong>Richard Thomas</strong></div>
                                    </div>
                                </div>
                            </div>
                        </a></div>
                </div>
            </div>
        </section>
        <section class="section section-lg bg-soft pb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col">
                        <div class="text-center">
                            <h3 class="mb-4 h4">{{ __('Avez-vous des questions sans réponse ?') }} {{ __('Faites nous le savoir !') }}</h3><a
                                class="text-primary font-weight-normal h4" href="{{ route('getAvis') }}">{{ __('Soumettre une question') }} <span
                                    class="icon icon-sm icon-primary ml-1"><i class="fas fa-arrow-right"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

@endsection
