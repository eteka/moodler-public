@extends('moodler::layouts.app-light')

@section('title', __('Soumettre un avis'))

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">

                <div class="card shadow-soft mt-5 border-light p-4 p-md-5 position-relative">
                    <h3 class="page-header">{{ __('Soumettre une requête ') }}</h3>
                    <hr>
                    <div class="formulaire">
                        <form action="{{ route('SaveAvis') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="p-3">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">×</button>
                                        <ul class="list-unstyled m-0">
                                            @foreach ($errors->all() as $error)
                                                <li>- {{ $error }}</li>
                                            @endforeach
                                        </ul>

                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group form-group {{ $errors->has('nom') ? 'has-error' : ''}}"><label for="nom">{{ __('Nom') }}</label> <input
                                            class="form-control" id="nom" name="nom" type="text"
                                            placeholder="{{ __('Votre nom de fammille') }}" value="{{  old('nom') }}" required=""></div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group form-group {{ $errors->has('prenom') ? 'has-error' : ''}}"><label for="prenom">{{ __('Prénom(s)') }}</label> <input
                                            class="form-control" id="prenom" value="{{  old('prenom') }}" name="prenom" type="text"
                                            placeholder="{{ __('Votre prénom') }}" required=""></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group form-group {{ $errors->has('email') ? 'has-error' : ''}}"><label for="email">.{{ __('Email') }}</label>
                                        <input class="form-control" id="email" type="email" value="{{  old('email') }}" name="email"
                                            placeholder="nom@societe.com"></div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group form-group {{ $errors->has('tel') ? 'has-error' : ''}}"><label for="phone">{{ __('Téléphone') }}</label>
                                        <input class="form-control" id="phone" type="text" name="tel" value="{{  old('tel') }}"
                                            placeholder="{{ __('+229 68 00 00 00') }}" ></div>
                                </div>
                            </div>
                           <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group {{ $errors->has('message') ? 'has-error' : ''}}"><label for="address">{{ __('Rédigez votre message') }}</label>
                                        <textarea name="message" id="message" required="" class="form-control" cols="4"
                                            rows="3">{{  old('message') }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-3"><button type="submit" class="btn btn-primary">{{ __('Soumettre') }}</button>
                            </div>
                        </form>
                    </div>



                </div>
            </div>
        </div>
    </div>

@endsection
