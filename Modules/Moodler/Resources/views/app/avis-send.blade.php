@extends('moodler::layouts.app-light')

@section('title', __('Avis envoyé'))

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">

                <div class="card shadow-soft mt-5 border-light p-4 p-md-5 position-relative">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span class="alert-inner--icon"><i class="far fa-thumbs-up"></i></span>
                        <span class="alert-inner--text">
                            <h6 class="text-white h5"></h3>

                            <strong>{{ __('Bravo !') }}</strong><br>
                             {{ __('Votre message a été envoyé avec succèss. Nous vous enverrons une réponde par email dans peu de temps.') }}


                    </span><br>
                    <b> {{ __('Merci pour la confiance !') }}</b><br>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <hr>
                <a href="{{ url('/') }}" class="btn btn-outline-primary">{{ __('Retour à la page d\'accueil') }}</a>
            </div>
        </div>
    </div>

@endsection
