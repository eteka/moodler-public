@extends('moodler::layouts.app')

@section('title', __('Documentation'))

@section('content')
<div class="bg-soft">
    @include('moodler::include.top')
    <section class="">
        <div class="row">
            <div class="col-12 mb-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                    <div class="d-block mb-4 mb-md-0">
                        <h2 class="h3 ml-4 page-header">{{ __('Configuration') }}</h2>
                    </div>
                </div>

                <div class="section-wrapper mt-3 col-sm-9">
                    <div class="card  bg-white border-light shadow-sm mb-4">
                        <div class="card-body">
                            <h2 class="h5  mb-1">
                                {{ __('Paramètres de la base de données') }}</h2>

                            <p class="mg-b-20 mg-sm-b-40 mb-4">
                                {{ __('Veuillez remplir les champs ci-desous qui donnent plus de détails sur la base de données Moodle utilisée') }}
                            </p>
                            <div class="row">
                                <section id="wizard6-p-0" role="tabpanel" aria-labelledby="wizard6-h-0"
                                    class="col-md-10 o" aria-hidden="false">
                                    <!--form action="{{ route('saveBdd') }}" method="POST" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data"-->
                                    {!! Form::model($config, ['method' => 'POST', 'route' => 'saveBdd']) !!}
                                    @csrf
                                    <div class="form-layout form-layout-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                @if($errors->any())
                                                    <ul class="alert alert-danger">
                                                        @foreach($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </div>
                                            <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span>
                                                {{ __('Serveur de base de données') }}:</label>
                                            <div
                                                class="col-sm-8 mg-t-10 mg-sm-t-0 {{ $errors->has('serveur') ? 'has-error' : '' }}">
                                                {{ Form::text('serveur', null, ['class' => 'form-control', 'placeholder' => __('172.10.10.100'), 'required' => 'true']) }}
                                            </div>
                                        </div><!-- row -->
                                        <div class="row mt-3">
                                            <label class="col-sm-4 form-control-label">
                                                <span class="tx-danger">*</span>
                                                {{ __('Nom de la base de données') }} :
                                            </label>
                                            <div
                                                class="col-sm-8 mg-t-10 mg-sm-t-0 {{ $errors->has('base_de_donnee') ? 'has-error' : '' }}">
                                                {{ Form::text('base_de_donnee', null, ['class' => 'form-control', 'placeholder' => __('moodle'), 'required' => 'true']) }}
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <label class="col-sm-4 form-control-label "><span class="tx-danger">*</span>
                                                {{ __('Utilisateur de la base de données') }}:</label>
                                            <div
                                                class="col-sm-8 mg-t-10 mg-sm-t-0 {{ $errors->has('utilisateur') ? 'has-error' : '' }}">
                                                {{ Form::text('utilisateur', null, ['class' => 'form-control', 'placeholder' => __('root'), 'required' => 'true']) }}

                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <label class="col-sm-4 form-control-label ">
                                                {{ __('Mot de passe de la base de données') }}:</label>
                                            <div
                                                class="col-sm-8 mg-t-10 mg-sm-t-0 {{ $errors->has('mot_de_passe') ? 'has-error' : '' }}">
                                                {{ Form::text('mot_de_passe', null, ['class' => 'form-control', 'placeholder' => __('root')]) }}
                                            </div>
                                        </div>
                                        <div class="row mt-3 mb-4">
                                            <label class="col-sm-4 form-control-label ">
                                                {{ __('Port de la base de données') }}:</label>
                                            <div
                                                class="col-sm-8 mg-t-10 mg-sm-t-0 {{ $errors->has('port') ? 'has-error' : '' }}">
                                                {{ Form::text('port', null, ['class' => 'form-control', 'placeholder' => __('3306')]) }}

                                            </div>
                                        </div>
                                        <div class="row mg-t-30">
                                            <div class="col-sm-4 text-right">
                                                <button class="btn btn-success bd-3 btn" type="button"><i
                                                        class="fas fa-check"></i>
                                                    {{ __('Tester la connexion') }}</button>

                                            </div>
                                            <div class="col-sm-8 ">
                                                <div class="form-layout-footer">

                                                    <button class="btn btn-primary bd-0" type="submit"><i
                                                            class="fa fa-save"></i>
                                                        {{ __('Sauvegarde') }}</button>
                                                    <a href="{{ route('structure') }}"
                                                        class="btn btn-secondary bd-0"> <i
                                                            class="fa fa-chevron-left"></i>
                                                        {{ __('Retour') }}</a>
                                                </div><!-- form-layout-footer -->
                                            </div><!-- col-8 -->
                                        </div>
                                    </div>
                                    </form>
                                </section>
                            </div>
                        </div>

                        <!--div id="wizard6" role="application" class="wizard wizard-style-2  clearfix">
                            <div class="steps clearfix">
                                <ul role="tablist">
                                    <li role="tab" class="first disabled" aria-disabled="false" aria-selected="true">
                                        <a id="wizard6-t-0" href="{{ route('structure') }}" aria-controls="wizard6-p-0">
                                            <span class="number">1</span> <span class="title">Information sur la
                                                structure</span></a>
                                    </li>
                                    <li role="tab" class="current" aria-disabled="true">
                                        <a id="wizard6-t-1" href="{{ route('configBdd') }}" aria-controls="wizard6-p-1">
                                            <span class="number">2</span>
                                            <span class="title">La base de données </span></a></li>
                                    <li role="tab" class="disabled last" aria-disabled="true">
                                        <a id="wizard6-t-2" href="{{ route('preferences') }}" aria-controls="wizard6-p-2">
                                            <span class="number">3</span> <span class="title">Préférences</span></a></li>
                                </ul>
                            </div>


                        </div-->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
        @endsection
