<?php

namespace Modules\Moodler\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
use Modules\Moodler\Entities\Post;
use Spatie\Activitylog\Models\Activity;

class PageController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$pots=Post::all();
       // dd($pots);
      // activity()->log('Look mum, I logged something');
      //$a=Activity::all();
       //dd($a);
        if(Auth::user()){
            activity()
            ->causedBy(Auth::user())
             ->withProperties(['type' => 'Création'])
            //->performedOn($someContentModel)
            ->log('Access à la page Admin');
        return redirect(route('dashbord'));
        }
        return view('moodler::welcome');
    }
}
