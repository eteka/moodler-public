<?php

namespace App\Http\Controllers\Moodler;

use App\Http\Controllers\Controller;
#use App\Http\Requests;

use App\Models\ParamExport;
use Illuminate\Http\Request;

class ParamExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $paramexport = ParamExport::where('niv_universite', 'LIKE', "%$keyword%")
                ->orWhere('niv_entite', 'LIKE', "%$keyword%")
                ->orWhere('niv_cylce', 'LIKE', "%$keyword%")
                ->orWhere('niv_annee', 'LIKE', "%$keyword%")
                ->orWhere('niv_filiere', 'LIKE', "%$keyword%")
                ->orWhere('niv_semestre', 'LIKE', "%$keyword%")
                ->orWhere('niv_cours', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $paramexport = ParamExport::latest()->paginate($perPage);
        }
dd('');
        return view('moodler::param.index',compact('paramexport'));
        //return view('param-export.index', compact('paramexport'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('moodler.param.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'niv_universite' => 'required',
			'niv_entite' => 'required',
			'niv_annee' => 'required'
		]);
        $requestData = $request->all();

        ParamExport::create($requestData);

        return redirect('param-export')->with('flash_message', 'ParamExport added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $paramexport = ParamExport::findOrFail($id);

        return view('param-export.show', compact('paramexport'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $paramexport = ParamExport::findOrFail($id);

        return view('param-export.edit', compact('paramexport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'niv_universite' => 'required',
			'niv_entite' => 'required',
			'niv_annee' => 'required'
		]);
        $requestData = $request->all();

        $paramexport = ParamExport::findOrFail($id);
        $paramexport->update($requestData);

        return redirect('param-export')->with('flash_message', 'ParamExport updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ParamExport::destroy($id);

        return redirect('param-export')->with('flash_message', 'ParamExport deleted!');
    }
}
