<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Parametre;
use Illuminate\Http\Request;

class ParametreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function getParametres(){
        
        return view('param.parametres');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $parametre = Parametre::where('niv_universite', 'LIKE', "%$keyword%")
                ->orWhere('niv_entite', 'LIKE', "%$keyword%")
                ->orWhere('niv_cylce', 'LIKE', "%$keyword%")
                ->orWhere('niv_filiere', 'LIKE', "%$keyword%")
                ->orWhere('niv_annee', 'LIKE', "%$keyword%")
                ->orWhere('niv_semestre', 'LIKE', "%$keyword%")
                ->orWhere('niv_cours', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $parametre = Parametre::latest()->paginate($perPage);
        }

        return view('moodler.parametre.index', compact('parametre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('moodler.parametre.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'niv_universite' => 'required',
			'niv_annee' => 'required'
		]);
        $requestData = $request->all();
        
        Parametre::create($requestData);

        return redirect('moodler/parametre')->with('flash_message', 'Parametre added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $parametre = Parametre::findOrFail($id);

        return view('moodler.parametre.show', compact('parametre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $parametre = Parametre::findOrFail($id);

        return view('moodler.parametre.edit', compact('parametre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'niv_universite' => 'required',
			'niv_annee' => 'required'
		]);
        $requestData = $request->all();
        
        $parametre = Parametre::findOrFail($id);
        $parametre->update($requestData);

        return redirect('moodler/parametre')->with('flash_message', 'Parametre updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Parametre::destroy($id);

        return redirect('moodler/parametre')->with('flash_message', 'Parametre deleted!');
    }
}
