<?php

namespace Modules\Moodler\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Moodler\Entities\Models\Avis;

class PageController extends Controller
{
    public function getDocs ()
    {
        $config=[];
        return view('moodler::app.docs',compact('config'));
       // dd('getDocs');
    }

    public function getAvis(Request $request)
    {
        $get=$request->input('send');
        if($get=='yes'){
             return view('moodler::app.avis-send');
        }
        return view('moodler::app.avis');
    }
    public function SaveAvis (Request $request)
    {

         $validatedData = $request->validate([
         'nom' => 'required|max:150',
         'prenom' => 'required|max:150',
         'message' => 'required|min:10|max:5000',
         ]);
         $requestData = $request->all();
        //dd($requestData);
        Avis::create($requestData);
        return redirect(route('getAvis')."?send=yes")->with('flash_message', __('Avis envoyé avec succès'));
    }
    public function getApropos ()
    {
       return view('moodler::app.about');
    }
    public function getContact ()
    {
        return view('moodler::app.contact');
    }
    public function getBlog ()
    {
        return view('moodler::app.blog');
    }
    public function setLang(String $locale)
    {
        echo $locale = in_array($locale, config('app.locales')) ? $locale : config('app.fallback_locale');
        session(['locale' => $locale]);
        $locale = session ('locale');
        return back();
    }
}
