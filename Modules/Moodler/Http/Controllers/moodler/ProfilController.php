<?php

namespace Modules\Moodler\Http\Controllers\Moodler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\User;
use Auth;
use Image;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ProfilController extends Controller
{

    public function getUsers (Request $request){
        $users=User::orderBy('etat_compte',"DESC")->paginate(15);
        return view('moodler::users.index',compact("users"));
    }
    public function getProfil  (){
        $user= Auth::user();
       return  view('moodler::profil.index',compact('user'));
    }
    public function updateProfil (Request $request){
        $user= Auth::user();
       // dd($request);
        if(!$user){abort(404);}
        //$requestData = $request->all();
        $requestData = $request->validate([
        'nom' => 'required|min:2|max:255',
        'prenom' => 'required|min:2|max:255',
        /*'email' => 'required|email',*/
        'poste' => 'max:150',
        'photo' => 'image',
        'tel' => 'max:30',
        'about' => 'max:500',
        'sexe' => 'required|in:M,F',
        ]);
        if($request->file('photo')){
            $image = $request->file('photo');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $patch='./users/';
            $destinationPath = public_path($patch);
            if(is_dir($destinationPath)){
                @mkdir($destinationPath,0777,true);
            }
             $extension = $image->getClientOriginalExtension();
             Storage::disk('public')->put($patch.$input['imagename'], File::get($image));
            $user->photo="storage/".$patch.$input['imagename'];
            /*$img = Image::make($image->getRealPath());

            $img->resize(100, 100, function ($constraint) {

            $constraint->aspectRatio();

            })->save($destinationPath.'/'.$input['imagename']);*/

            /*After Resize Add this Code to Upload Image*/
           // $destinationPath = public_path('/');


            //$image->move($destinationPath, $input['imagename']);
        }
        $user->nom=$requestData['nom'];
        $user->prenom=$requestData['prenom'];
        $user->poste=$requestData['poste'];
        $user->telephone=$requestData['tel'];
        $user->sexe=$requestData['sexe'];
        $user->apropos=$requestData['about'];
        $user->update();
        //dd($requestData['nom']);
        return back();
        return redirect()->to('/home')->with('flash_message', __('Profil mise à jour !'));
    }
     public function editProfil (){
     $user= Auth::user();
     return view('moodler::profil.edit',compact('user'));
     }
    public function changePassword  (){
        dd("changePassword  ");
    }
    public function userPreferences (){
        $user=Auth::user();
        return view("moodler::profil.config",compact('user'));
    }
    public function editRole($id){
        $user=Auth::user();
        $u=User::findOrFail($id);
        $nom=$u->fullname();
        $roles=Role::pluck('name','id');
        //dd($roles);
        return view("moodler::users.edit-role",compact('id','user','nom','roles'));
    }
    public function saveRole($id,Request $request){
        $requestData = $request->validate([
        'role' => 'required|integer|exists:roles,id',
        ]);
        $rid=$requestData['role'];
        //dd($rid);
        $user=Auth::user();
        $u=User::findOrFail($id);
        $role=Role::findOrFail($rid);
        if(empty($role)){
            dd('Erreur interne');
        }
        //$last_role=$u-
        $role_name=$role->name;
        $u->assignRole($role_name);
        $roles=$u->getRoleNames();
        //dd($roles);
        //$u=User::role($role_name)->get();
        $nom=$u->fullname();
        //dd($role_name);
        $roles=Role::pluck('name','id');
        $request->session()->flash('info', __('Modification de rôle effectuée'));
       // dd($u);
        return redirect(route('getUsers'));
    }
    public function SavePreferences (Request $request){
        dd($request);
    }
}
