<?php
# MODEL 1
#####################################################################################
# NIVEAU 1 : UNIVERSITÉ                                                             #
# NIVEAU 2 : ENTITÉ / FORMATION                                                     #
# NIVEAU 3 : CYCLE
# NIVEAU 5 : ANNÉE ACADÉMIQUE                                                       #
# NIVEAU 4 : FILIERE                                                                #                                                  #
# NIVEAU 6 : SEMESTRE                                                               #
# NIVEAU 7 : COURS                                                                  #
#####################################################################################
# MODEL 2
#####################################################################################
# NIVEAU 1 : ENTITÉ / FORMATION                                                     #
# NIVEAU 2 : CYCLE                                                                  #
# NIVEAU 4 : ANNÉE ACADÉMIQUE                                                       #s
# NIVEAU 3 : FILIERE                                                                #
# NIVEAU 5 : SEMESTRE                                                               #
# NIVEAU 6 : COURS                                                                  #
#####################################################################################
# MODEL 3
#####################################################################################
# NIVEAU 2 : ANNÉE ACADÉMIQUE                                                       #
# NIVEAU 1 : FILIERE                                                                #
# NIVEAU 3 : SEMESTRE                                                               #
# NIVEAU 4 : COURS                                                                  #
#####################################################################################
namespace Modules\Moodler\Http\Controllers\moodler;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Modules\Moodler\Entities\Models\Rapport;
use Spatie\Activitylog\Models\Activity;

use App\Models\Db as db;
use Storage;
use Str;
use PDF;
use Excel;
use Auth;
use \PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\InvoicesExport;
use App\InvoicesRapport;
use App\Models\Universite;
use App\Models\Entite;
use App\Models\Cycle;
use App\Models\Filiere;
use App\Models\AnneeAcademique;
use App\Models\Semestre;
use App\Models\Cours;
use Modules\Moodler\Entities\Models\Avis;
use Modules\Moodler\Entities\ParamExport;

class RapportController extends Controller
{

    private $db;

    protected $table_categories="mdl_course_categories";
    protected $dossier_rhebo="moodler/rapports/hebdomadaire/";
    protected $dossier_rmensuel="moodler/rapports/mensuels/";
    protected $dossier_rsemestre="moodler/rapports/semestre/";
    private $table_cours="mdl_course";
    private $nb_paginate;
    private $Export_config=[
        "niveau1"=>"0"
    ];
    public function __construct(){


        $dbhost = '127.0.0.1';
        $dbuser = 'root';
        $dbpass = '';
        $dbname = 'bitnami_moodle';

        $db = new db($dbhost, $dbuser, $dbpass, $dbname);
        $this->db=$db;
        $this->init();
    }
    public function init(){
        $this->nb_paginate=10;
    }
    public function creerDossier(){
        if(!is_dir(storage_path($this->dossier_rhebo))){ mkdir(storage_path($this->dossier_rhebo),0777,true);}
        if(!is_dir(storage_path($this->dossier_rmensuel))){ mkdir(storage_path($this->dossier_rmensuel),0777,true);}
        if(!is_dir(storage_path($this->dossier_rsemestre))){ mkdir(storage_path($this->dossier_rsemestre),0777,true);}
    }

    public function getRapportMensuelData($univ="",$entite="",$cycle="",$annee="",$filiere=""){
        $categories=$this->getMoodleCat();
        $tab=null;
         foreach($categories as $t){
            #Vérification des niveaux
            $niveau=$t->depth;
            if($niveau==1 ){

               if($t->id==$univ){
               // echo "UNIVERSITÉ CHOISIE UP : ";
                $tab['univ'][]=[$t->path=>$t->name];
               }
            }
            if($niveau==2){
                if($t->id==3){
                   // echo "ENTITE CHOISIE (IUT): ";
                    $tab['entite'][]=['nom'=>$t->name,'path'=>$t->path];
                   }
            }

            if($niveau==3){
                if($t->id==16){
                     "CYLCE CHOISIE (LICENCE): ";
                    $tab['cycle'][]=['nom'=>$t->name,'path'=>$t->path];
                   }
            }
            if($niveau==4){
                {
                    //echo "FILIERE CHOISIE (LICENCE): ";
                    $tab['annee'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                }

            }
            if($niveau==5){
              //  if($t->parent==$acad){
                    //echo "ANNÉE ACADEMIQUE CHOISIE (2019): ";
                    $tab['filiere'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
              //     }
            }
            if($niveau==6){
                    $spath=$t->path;

                    $tab_fil=explode("/",$spath);
                    $code_filiere=isset($tab_fil[5])?$tab_fil[5]:0;
                    $tab['semestre'][$code_filiere][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                    $gmat=$this->getMoodleCours($t->id);
                    $tab['matiere'][$t->id]=$gmat;
               // }
            }
        }
      return $tab;
    }

    public function getRapportHebdoData($univ="",$entite="",$cycle="",$annee="",$filiere=""){


        return $this->getRapportMensuelData($univ,$entite,$cycle,$annee,$filiere);
    }
    public function rapport($c=""){

        //$users=$this->db->q('Select * from mdl_course');
        //echo "- UNIVERSITE <br>-ENTITE <br>ANNÉE ACADEMIQUE";
        $categories=$this->getMoodleCat();
        $tab=null;
        //$tab['univ'][]=$tab['entite'][]=$tab['cycle'][]=$tab['filiere'][]=$tab['annee'][]=$tab['semestre'][]=$tab['matiere'][]=null;
        foreach($categories as $t){
            #Vérification des niveaux
            $niveau=$t->depth;

            if($niveau==1 ){

               if($t->id==2){
               // echo "UNIVERSITÉ CHOISIE UP : ";
                $tab['univ'][]=[$t->path=>$t->name];

                // $t->name;echo"<br>";
               // dd($tab);
               }
            }
            //echo"<hr>";

            if($niveau==2){
                if($t->id==3){
                   // echo "ENTITE CHOISIE (IUT): ";
                    $tab['entite'][]=['nom'=>$t->name,'path'=>$t->path];

                   // echo $t->name;echo"<br>";
                   // dd($tab);
                   }
            }

            if($niveau==3){
                if($t->id==16){
                     "CYLCE CHOISIE (LICENCE): ";
                    $tab['cycle'][]=['nom'=>$t->name,'path'=>$t->path];

                     //$t->name;echo"<br>";
                   // dd($tab);
                   }
            }

           // echo"<hr>";
            if($niveau==4){
                {
                   // if($t->id>=0){
                    //echo "FILIERE CHOISIE (LICENCE): ";
                    $tab['annee'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                   // }
                    // $t->name;echo"<br>";
                }

            }
            $acad=19;
            //echo"<hr>";
            if($niveau==5){
                //dd($t);
               //echo $t->parent."==========".$t->name;
                if($t->parent==$acad){
                    //echo "ANNÉE ACADEMIQUE CHOISIE (2019): ";
                    $tab['filiere'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];

                     //echo "filiere=".$t->name."==".$t->id;echo"<br>";
                    //dd($tab);
                   }
            }
            //echo"<hr>";
           /* if($niveau==6){
               // dd($t);
               //echo $t->parent."==========".$t->name;
                if($t->parent==$acad){
                    //echo "ANNÉE ACADEMIQUE CHOISIE (2019): ";
                    $tab['semestre'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];

                     //$t->name;echo"<br>";
                   // dd($tab);
                   }
            }*/
            //Les semestres
            #Ici je recupère les semestres de toutes l'année académique concernée
           // $fil=
            if($niveau==6){
                //echo "SEMESTRE : ";
                //echo $t->name."==".$t->id; echo"<br>";
                //dd($t);
                //if($t->parent==$fil){
                    $spath=$t->path;//."==============". $t->id."===============<hr>";

                    $tab_fil=explode("/",$spath);
                    $code_filiere=isset($tab_fil[5])?$tab_fil[5]:0;
                   // $fil_code=
                   //print_r($tab['filiere']);
                   //dd($tab_fil);
                    $tab['semestre'][$code_filiere][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                    $gmat=$this->getMoodleCours($t->id);
                   // if($gmat!=null)
                    $tab['matiere'][$t->id]=$gmat;
                  // dd($tab['semestre']);
               // }
            }

            $path=$t->path;

            //echo($t->path); echo "<hr>";
        }
        return  $tab;

    }
    public function exportPDF(){
        $this->creerDossier();
        $data= $this->rapport("x");
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'Montserrat']);
        $pdf = PDF::loadview('moodler::export.x-mensuel', compact('data'));
        $pdf->setPaper("a4", "landscape" );
        file_put_contents($this->dossier_rmensuel.'pdf-'.Str::slug(date('Y-m-d h:i:s')).'_Rapport_MoodleR.pdf', $pdf->output());
        return $pdf->download('rapport-mensuel.pdf');
    }
    public function exportWord(){
    //     // Creating the new document...
    //     $phpWord = new \PhpOffice\PhpWord\PhpWord();

    //     /* Note: any element you append to a document must reside inside of a Section. */

    //     // Adding an empty Section to the document...
    //     $section = $phpWord->addSection();
    //     // Adding Text element to the Section having font styled by default...
    //     $data=$this->rapport();
    //     $v= view("moodler.export.x-mensuel",compact('data'))->render();
    //     $section->addText($v);
    //    // dd($phpWord);
    //     /*
    //     * Note: it's possible to customize font style of the Text element you add in three ways:
    //     * - inline;
    //     * - using named font style (new font style object will be implicitly created);
    //     * - using explicitly created font style object.
    //     */

    //     // Adding Text element with font customized inline...
    //     $section->addText(
    //     '"Great achievement is usually born of great sacrifice, '
    //     . 'and is never the result of selfishness." '
    //     . '(Napoleon Hill)',
    //     array('name' => 'Tahoma', 'size' => 10)
    //     );

    //     // Adding Text element with font customized using named font style...
    //     $fontStyleName = 'oneUserDefinedStyle';
    //     $phpWord->addFontStyle(
    //     $fontStyleName,
    //     array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
    //     );
    //     $section->addText(
    //     '"The greatest accomplishment is not in never falling, '
    //     . 'but in rising again after you fall." '
    //     . '(Vince Lombardi)',
    //     $fontStyleName
    //     );

    //     // Adding Text element with font customized using explicitly created font style object...
    //     $fontStyle = new \PhpOffice\PhpWord\Style\Font();
    //     $fontStyle->setBold(true);
    //     $fontStyle->setName('Tahoma');
    //     $fontStyle->setSize(13);
    //     $myTextElement = $section->addText('"Believe you can and you\'re halfway there." (Theodor Roosevelt)');
    //     $myTextElement->setFontStyle($fontStyle);

    //     // Saving the document as OOXML file...
    //     $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    //     $objWriter->save('helloWorld.docx');

    //     // Saving the document as ODF file...
    //     $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'ODText');
    //     $objWriter->save('helloWorld.odt');

    //     // Saving the document as HTML file...
    //     $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
    //     $objWriter->save('helloWorld.html');

        /* Note: we skip RTF, because it's not XML-based and requires a different example. */
        /* Note: we skip PDF, because "HTML-to-PDF" approach is used to create PDF documents. */
//     $pw = new PhpWord();

//     /* [THE HTML] */
//     $section = $pw->addSection();
//     $m=new InvoicesRapport();
//     $data=$this->rapport();
//     $v= view("moodler.export.x-mensuel",compact('data'))->render();
//     $html = $v;

//     \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html, false, false);

// /* [SAVE FILE ON THE SERVER] */
//  $pw->save("html-to-doc.docx", "Word2007");

// /* [OR FORCE DOWNLOAD] */
// header('Content-Type: application/octet-stream');
// header('Content-Disposition: attachment;filename="convert.docx"');
// $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($pw, 'Word2007');
// $objWriter->save('php://output');

// $data = file_get_contents('helloWorld.html');
$data=$this->rapport();
//  //dd($data);
$data= view("moodler.export.x-mensuel-excel",compact('data'))->render();
$dom = new \DOMDocument();
$dom->loadHTML($data);

// Now, extract the content we want to insert into our docx template

// 1 - The page title
$documentTitle = $dom->getElementById('title')->nodeValue;

// 2 - The article body content
$documentContent = $dom->getElementById('content')->nodeValue;

// Load the template processor
$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(asset('storage/template.docx'));

// Swap out our variables for the HTML content
$templateProcessor->setValue('author', "Robin Metcalfe");
$templateProcessor->setValue('title', $documentTitle);
$templateProcessor->setValue('content', $documentContent);

header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="generated.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
$templateProcessor->saveAs("php://output");
// $file_name="Rapport_MoodleR";
// $data=$this->rapport();

// $view = view("moodler.export.x-mensuel",compact('data'))->render();

// $file_name = strtotime(date('Y-m-d H:i:s')) . '_'.$file_name.'.docx';
// $headers = array(
// "Content-type"=>"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
// "Content-Disposition"=>"attachment;Filename=$file_name",
// );
// return response()->make($view, 200, $headers);

    }
    public function exportHTML(){
        return $this->rapport();
    }
    public function rapportHebdo(){
        return $this->db->q("select * from mdl_course_categories order by depth ASC");
    }
    public function rapportMensuele(){
        /*$u=new Universite(1,"UNIV PARAKOU");
        $u->entite=new Entite(7,"IG","UP-IG",$u->id);
        $u->entite=new Entite(8,"GC","UP-GC",$u->id);
        $ts=$u->entite;
        dd($ts);*/
        return $this->rapport();
    }public function exportExcel()
    {
        //return Excel::download(new InvoicesRapport, 'invoices.xls');
        //return (new InvoicesRapport)->download('invoices.xlsx', \Maatwebsite\Excel\Excel::XLSX);

        $this->creerDossier();
        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
         $data=$this->rapport();

         $view = view("moodler.export.x-mensuel-excel",compact('data'))->render();
        $htmlString = $view;

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($htmlString);

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $Excel_name="Excel-".Str::slug(date('Y-m-d h:i:s'))."_Rapport_MoodleR.xls";
        $file=$this->dossier_rmensuel.$Excel_name;
        $fsave=storage_path($file);
        //dd($fsave);
        //$save=$writer->save($fsave);
        //dd($fsave);
        //echo $file=storage_path($file);;
        //return Storage::download(($file), $Excel_name);
        $filename=$file;
        //return Storage::download($file);
        //echo ($file);
        //$fichier="moodler/rapports/mensuels/Excel-2020-08-07-125605_Rapport_MoodleR.xlsx";
        //$f=($fichier);
        //dd($f);
        $fwile="hello_world.xlsx";
        $response = response()->streamDownload(function() use ($spreadsheet) {
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        });
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment; filename="your_file.xls"');
        $response->send();

    }
    public function exportExcele(){


        $data=$this->rapport("x");

        Excel::create('ALL', function($excel) use ($data) {

          // Set the title
          $excel->setTitle(__('Rapport Mensuel'));

          // Chain the setters
          $excel->setCreator('APP')
          ->setCompany('Odacesoft');
          $excel->setDescription(__('Fichier de mes biens par SécuritAppend'));
          $entete=[
            0=>__("Formation"),
            1=>__("SEMESTRE"),
            2=>__("SÉQUENCES"),
            3=>__("MATIERES"),
            4=>__("SEQUENCES EXCECUTÉES"),
            5=>__("TAUX D'EXECUTION")
          ];

          $excel->sheet('Rapport Mensuel', function($sheet) use ($entete,$biens)
          {

                //$sheet->fromArray($b);
                //dd($biens->get());

                $l=1;
                $tab=["A1","B1","C1","D1","E1","F1","G1","H1","I1","J1","K1","L1"];
                $j=0;
                foreach ($tab as $column) {
                  $v=isset($entete[$j])?$entete[$j]:'';

                  $sheet->cell($column, function($cell) use ($v) {
                      $cell->setValue($v);
                      $cell->setFontWeight('bold');
                      $cell->setAlignment('center');
                  });
              $j++;
            }
            #Set h
            $i=2;
            //$sheet->fromArray($biens,NULL, 'A2');
        /*
          foreach ($biens as  $b) {
             /* $mode=$b->mode?$b->mode->nom:"-";
              $type=$b->type?$b->type->nom:"-";
              $marque=$b->marque?$b->marque->nom:"-";
              $modele=$b->modele?$b->modele->nom:"-";
              $image=$b->media?$b->media->url:"-";
              $imei=$b->imei1;
              $imei=$b->imei2!=""?$imei.="-".$b->imei2:$imei;
              $imei=$imei!=""?"'".$imei."'":'-';
              $data=[
                $imei,
                $b->nom,
                $b->visibilite(), $mode,$type,
                $marque,$modele,$b->vendeur,
                $b->lieu_achat,$b->prix_achat,
                $b->infos,$image=="-"?$image:asset($image)
              ];

              $sheet->appendRow($i,$data);
              $i++;
            }*/
            $sheet->setAutoSize(true);
            $sheet->freezeFirstRow();
          });

          //})->download('csv');
        })->download('xlsx');

    }
    public function rapportSemestriel(){
        return $this->db->q("select * from mdl_course_categories order by depth ASC");
    }
    public function getMoodleCat(){
        return $this->db->q("select * from ".$this->table_categories." order by depth ASC");
    }
    public function findMoodleCat($id){
        return $this->db->q("select * from ".$this->table_categories." where id=".$id);
    }
    public function getMoodleCours($id){
        return $this->db->q("select * from ".$this->table_cours." where category=".$id);
    }

    public function createRapportMensuel()
    {
        dd('');
       return view('moodler::rapport-mensuel.create');
    }
    public function getRapportMensuel (){
        $univ_id=2; $uent_id=3;  $cycle_id=16; $annee_id=17;$fil_id=4;
        $data=$this->getRapportHebdoData($univ_id,$uent_id,$cycle_id,$annee_id);
        //dd($data);
        #Traiement des paramètres fixés
        $universite=$this->getCatName($univ_id);
        $lentite=$this->getCatName($uent_id);
        $lecycle=$this->getCatName($cycle_id);
        $annee_academique=$this->getCatName($annee_id);


        $html = view('moodler::export.x-hebdomadaire',compact('data',"universite","lentite","lecycle","annee_academique"))->render();
        return view('moodler::rapport.rapport-mensuel',compact("html"));
    }
    public function getCatName($id){
        $n="";
        $cat_get=$this->findMoodleCat($id);
        if(!empty($cat_get)){
            $cat_o=$cat_get[0];
            //dd($univ_o);
            $n=isset($cat_o->name)?$cat_o->name:"-";
        }
        return $n;
    }
    public function getRapportHebdo(Request $request){
         $uid=$this->getUid();
         $get_activ_config=ParamExport::where('user_id',$uid)->where('active',1)->first();
        // dd($get_activ_config);
         #Universite
         $universite=$filiere=$lentite=$lecycle=$annee_academique=NULL;

         if($get_activ_config!=NULL){
             #UNIV
            $d_univ=$get_activ_config->niv_universite;
            //dd($d_univ);
            if($d_univ)
            $universite=$this->getsCatList($d_univ);

            #ETAB
            $d_etab=$get_activ_config->niv_entite;
            if($d_etab)
            $lentite=$this->getsCatList($d_etab);

            #CYCLE
            $c_etab=$get_activ_config->niv_cylce;
            if($c_etab)
            $lecycle=$this->getsCatList($c_etab);

             #ACAD
             $a_etab=$get_activ_config->niv_annee;
             if($a_etab)
             $annee_academique=$this->getsCatList($a_etab);
             #FILIERE
             $f_etab=$get_activ_config->niv_filiere;
             if($f_etab)
             $filiere=$this->getsCatList($f_etab);
              //dd($annee_academique);

         }else{
         $request->session()->flash('warning', "Aucun paramètre activé");
         return redirect()->back();
         }

         //dd($data);
         //$univ_id=2; $uent_id=3; $cycle_id=16; $annee_id=17;$fil_id=4;
         $html=null;
         $univ_id=$uent_id=$cycle_id=$annee_id=$fil_id=0;

         $data=$this->getRapportHebdoData($univ_id,$uent_id,$cycle_id,$annee_id);
         if(isset($_GET['generer'])){
             $univ_id=(int)trim($_GET['univ']);
             $uent_id=(int)trim($_GET['entite']);
             $cycle_id=(int)trim($_GET['cycle']);
             $annee_id=(int)trim($_GET['acad']);
             $date=trim($_GET['date']);

             $univ=$this->getCatName($univ_id);
             $ent=$this->getCatName($uent_id);
             $cycl=$this->getCatName($cycle_id);
             $acad=$this->getCatName($annee_id);
              $html =view('moodler::export.x-hebdomadaire',compact('data',"univ","ent","cycl","acad"))->render();
            $rapport=[
                "nom"=>__("Rapport du").$date.__('généré le ').date('d/m/Y à H\h i\m\i\n s\s\e\c\o\n\d\e\s'),
                "format"=>'PDF',
                "date_debut"=>$date,
                "date_fin"=>$date,
                "fichier"=>'',
                "taille"=>'',
                "etat"=>0,
                "user_id"=>$uid,
            ];
              Rapport::create($rapport);
            }
        #Traiement des paramètres fixés
       /* $univ_=$this->getCatName($univ_id);
        $lentite=$this->getCatName($uent_id);
        $lecycle=$this->getCatName($cycle_id);
        $annee_academique=$this->getCatName($annee_id);*/


        //$html = view('moodler::export.x-hebdomadaire',compact('data',"universite","lentite","lecycle","annee_academique"))->render();
        return
        view('moodler::rapport.rapport-hebdomadaire',compact('html','data',"universite","lentite","lecycle","annee_academique",'filiere'));
    }
    public function getRapportSemestre(){
        $univ_id=2; $uent_id=3;  $cycle_id=16; $annee_id=17;$fil_id=4;
        $data=$this->getRapportHebdoData($univ_id,$uent_id,$cycle_id,$annee_id);
        //dd($data);
        #Traiement des paramètres fixés
        $universite=$this->getCatName($univ_id);
        $lentite=$this->getCatName($uent_id);
        $lecycle=$this->getCatName($cycle_id);
        $annee_academique=$this->getCatName($annee_id);


        $html = view('moodler::export.x-par-semestre',compact('data',"universite","lentite","lecycle","annee_academique"))->render();
        return view('moodler::rapport.rapport-par-semestre',compact("html"));
    }

    public function getListeRapport(Request $request){
        #Recherche
        $keyword = $request->get('q');
        #Nombre
        $gnb=(int)$request->get('nb');
        $nb=$gnb>1?$gnb:8;
        #Id de l'utilisateur
        $uid=Auth::user()?Auth::user()->id:0;//dd($uid);
        #Format
        $format = $request->get('format')!=''?$request->get('format'):'tout';
        #Type de rapport
        $type = (int)$request->get('type_rapport');
        $tab_types=['0'=>'*',1=>'rapport hebdomadaire',2=>'rapport mensuel',3=>'rapport semestriel'];
        $type=isset($tab_types[$type])?$tab_types[$type]:'*';
        $perPage = 25;
        $r=$rapports=null;
         if (!empty($keyword)) {
            $r = Rapport::where('nom', 'LIKE', "%$keyword%")
            ->orWhere('fichier', 'LIKE', "%$keyword%")
           // ->orWhere('type', 'LIKE', "%$type%")
            ->orWhere('description', 'LIKE', "%$keyword%");

         } else {
            $r = Rapport::latest();
            //dd($r);
         }
         if($type!='*'){
           $r=$r->orWhere('type','LIKE', "%$type");
         }
        // dd($type);
         if($format!='tout'){

            $r=$r->orWhere('format', "$format");
         }


         if($r){
             $r=$r->where('user_id', $uid);
             $rapports=$r->paginate($nb);
         }
         //dd($rapports);

        //$rapports=Rapport::latest()->paginate($nb);
        //dd($rapports);
        return view('moodler::liste-rapport.tout',compact('rapports','nb'));
    }
    public function getStatistiques (){
        return view('moodler::statistique.index');
    }
    public function getHistorique  (){
        //dd('getHistorique ');
        $nb=10;
        $uid=Auth::user()?Auth::user()->id:0;/*
        activity($uid)->log("hi");
        activity($uid)->log("Bonjour");
        activity($uid)->log("Bonsoir");
        activity($uid)->log("Démarrer");
        activity($uid)->log("Arrêter");*/
        $historiques=Activity::latest()->where('causer_id' , $uid)->paginate($nb);
        //dd($historiques);
        return view('moodler::historique.index',compact('historiques'));
    }
    public function getParamExport(){
        $uid=$this->getUid();
        $n=$this->getNbAfficher();
       $params=ParamExport::where('user_id',$uid)->paginate($n);
        //dd($params);
        return view('moodler::param.export',compact('params'));
    }
    public function getNbAfficher(){
        $n=10;
        return $n;
    }
    public function newParamExport(){
        $structure=[""=>"Non disponible",
            1=>__("Université"),2=>__("Entité/Établissement"),3=>__('Cycle de formation'),
            4=>__("Année académique"),5=>__("Filière/Formation"),6=>__('Semestre'),7=>__("Cours/Matière")
        ];
        return view('moodler::param.new',compact('structure'));
    }
    public function saveParamExport(Request $request){
        $requestData = $request->all();
        $this->validate($request, [
        'nom_param' => 'required|max:255',
        ]);

        $requestData = $request->all();
        $uid=Auth::user()?Auth::user()->id:0;
        $data=[
            'nom_configuration'=> $requestData['nom_param'],
            'user_id'=> $uid,
        ];
        $p=ParamExport::create($data);
        //dd($p->id);
        return redirect(route('editParamExport',$p->id));
    }
    public function activeConfig(Request $request){
        $requestData = $request->all();
        $this->validate($request, [
        'param_id' => 'required|integer',
        ]);

        $uid=$this->getUid();
        $requestData = $request->all();
        $pid=$requestData['param_id'];
        $param=ParamExport::where("id",$pid)->where("user_id",$uid)->first();
        if(empty($param)){
            abort(404);
        }
        $data=[
            'active'=> TRUE
        ];
        //dd($p->id);
        ParamExport::where("user_id",$uid)->update(array('active' => 0));
        $param->active=1;
        $param->save();
        //dd($param);
        $request->session()->flash('info', __("Activation éffectuée avec succès"));
        return redirect(route('getParamExport'));

    }
    public function getUid(){
       return $uid=Auth::user()?Auth::user()->id:0;
    }
    public function getConsultation   (){
        //dd('getConsultation  ');
        return view('moodler::consultation.index');
    }
    public function editParamExport($id){
        //dd('getConsultation  ');
        $param=ParamExport::find($id);
        if(empty($param)){
            abort(404);
        }
        $univ_depth=(int)$param->niv_universite;
        $list_univ=[];
        if($univ_depth>0){
            $list_univ=$this->getCatList($univ_depth);
        }
         $list_etab=[];
         $etab_depth=(int)$param->niv_entite ;
         if($etab_depth>0){
         $list_etab=$this->getCatList($etab_depth);
         }
         $list_cycle=[];
         $cycle_depth=(int)$param->niv_cylce ;
         if($cycle_depth>0){
         $list_cycle=$this->getCatList($cycle_depth);
         }
         $list_acad=[];
         $acad_depth=(int)$param->niv_annee ;
         if($cycle_depth>0){
         $list_acad=$this->getCatList($acad_depth);
         }
         $list_form=[];
         $form_depth=(int)$param->niv_filiere ;
         if($form_depth>0){
         $list_form=$this->getCatList($form_depth);
         }
         $list_sem=[];
         $sem_depth=(int)$param->niv_semestre ;
         if($sem_depth>0){
         $list_sem=$this->getCatList($sem_depth);
         }
         //dd($list_sem);
        $pnom=$param->nom_configuration;
         return
         view('moodler::param.edit',compact('pnom','id','list_univ','list_etab','list_cycle','list_acad','list_form','list_sem'));
    }
    public function showAddUniv($id){

        //dd('getConsultation  ');
        $param=ParamExport::find($id);
        if(empty($param)){
            abort(404);
        }
        $pnom=$param->nom_configuration;
        $univ_id=$param->niv_universite;
        $list="0";
        $uid=$this->getUid();
        $tab_id_used=$this->getListInv($id,$uid);
        if(!empty($tab_id_used))
        $list=implode(",",$tab_id_used);

        $tab=$this->db->q("select id,name,idnumber,depth from ".$this->table_categories." WHERE depth NOT IN
        (".$list.") order by depth ASC");
        $univs=[];
        foreach($tab as $et){
        $univs[$et->depth]=$et->name;
        }
       // dd($univs);
         return view('moodler::param.add-univ',compact('pnom','id','univs'));
    }
    public function getCatList ($depth){
     $data= $this->db->q("select id,name,idnumber,depth from ".$this->table_categories." WHERE depth=".$depth." order by
     depth ASC" );
return $data;
    }
     public function getsCatList ($depth){
     $data= $this->db->q("select id,name from ".$this->table_categories." WHERE depth=".$depth." order by
     depth ASC" );
     $all=[];
     foreach($data as $d){
        $all[$d->id]=$d->name;
     }

     return $all;
     }
    public function getListInv ($id,$uid){
     $param= ParamExport::where("id",$id)->where("user_id",$uid)->first();
     $tab_data_used=[];
     if($param->niv_universite>0){
         $tab_data_used[]=$param->niv_universite ;
     }
     if($param->niv_entite >0){
         $tab_data_used[]=$param->niv_entite ;
     }
     if($param->niv_cylce >0){
         $tab_data_used[]=$param->niv_cylce ;
     }
     if($param->niv_annee >0){
         $tab_data_used[]=$param->niv_annee ;
     }
     if($param->niv_filiere >0){
         $tab_data_used[]=$param->niv_filiere ;
     }
     if($param->niv_semestre >0){
         $tab_data_used[]=$param->niv_semestre ;
     }
    return $tab_data_used;
    }
    public function showAddEtab ($id){

        //dd('getConsultation  ');
         $uid=$this->getUid();
        $param=ParamExport::where("id",$id)->where("user_id",$uid)->first();
        if(empty($param)){
            abort(404);
        }
        $pnom=$param->nom_configuration;
        $univ_id=$param->niv_universite;
        $list="0";
        if(!empty($tab_id_used))
        $list=implode(",",$tab_id_used);

        $tab=$this->db->q("select id,name,idnumber,depth from ".$this->table_categories." WHERE depth NOT IN (".$list.") order by depth ASC");
        $univs=[];
        foreach($tab as $et){
            $univs[$et->depth]=$et->name;
        }
        //dd($univs);
         return view('moodler::param.add-etab',compact('pnom','id','univs'));
    }
    public function showAddCycle ($id){

        //dd('getConsultation  ');
        $param=ParamExport::find($id);
        if(empty($param)){
            abort(404);
        }
        $pnom=$param->nom_configuration;
        $univ_id=$param->niv_universite;
        $list="0";
        $uid=$this->getUid();
        $tab_id_used=$this->getListInv($id,$uid);
        $list=implode(",",$tab_id_used);

        $tab=$this->db->q("select id,name,idnumber,depth from ".$this->table_categories." WHERE depth NOT IN
        (".$list.") order by depth ASC");
        $univs=[];
        foreach($tab as $et){
        $univs[$et->depth]=$et->name;
        }
        //dd($univs);
         return view('moodler::param.add-cycle',compact('pnom','id','univs'));
    }
    public function showAddAcad ($id){

        //dd('getConsultation  ');
        $param=ParamExport::find($id);
        if(empty($param)){
            abort(404);
        }
        $pnom=$param->nom_configuration;
        $univ_id=$param->niv_universite;
        $list="0";
        $uid=$this->getUid();
        $tab_id_used=$this->getListInv($id,$uid);
        $list=implode(",",$tab_id_used);

        $tab=$this->db->q("select id,name,idnumber,depth from ".$this->table_categories." WHERE depth NOT IN
        (".$list.") order by depth ASC");
        $univs=[];
        foreach($tab as $et){
        $univs[$et->depth]=$et->name;
        }
        //dd($univs);
         return view('moodler::param.add-acad',compact('pnom','id','univs'));
    }
    public function showAddForm ($id){

        //dd('getConsultation  ');
        $param=ParamExport::find($id);
        if(empty($param)){
            abort(404);
        }
        $pnom=$param->nom_configuration;
        $univ_id=$param->niv_universite;
        $list="0";
        $uid=$this->getUid();
        $tab_id_used=$this->getListInv($id,$uid);
        $list=implode(",",$tab_id_used);

        $tab=$this->db->q("select id,name,idnumber,depth from ".$this->table_categories." WHERE depth NOT IN
        (".$list.") order by depth ASC");
        $univs=[];
        foreach($tab as $et){
        $univs[$et->depth]=$et->name;
        }
        //dd($univs);
         return view('moodler::param.add-form',compact('pnom','id','univs'));
    }
    public function showAddSemestre ($id){

        //dd('getConsultation  ');
        $param=ParamExport::find($id);
        if(empty($param)){
            abort(404);
        }
        $pnom=$param->nom_configuration;
        $univ_id=$param->niv_universite;
        $list="0";
        $uid=$this->getUid();
        $tab_id_used=$this->getListInv($id,$uid);
        $list=implode(",",$tab_id_used);

        $tab=$this->db->q("select id,name,idnumber,depth from ".$this->table_categories." WHERE depth NOT IN
        (".$list.") order by depth ASC");
        $univs=[];
        foreach($tab as $et){
        $univs[$et->depth]=$et->name;
        }
        //dd($univs);
         return view('moodler::param.add-semestre',compact('pnom','id','univs'));
    }
    /*public function showAddCours ($id){

        //dd('getConsultation  ');
        $param=ParamExport::find($id);
        if(empty($param)){
            abort(404);
        }
        $pnom=$param->nom_configuration;
        $univ_id=$param->niv_universite;
        $tab=$this->db->q("select id,name,idnumber,depth from ".$this->table_categories." order by depth ASC");
        $univs=[];
        foreach($tab as $et){
            $univs[$et->id]=$et->name;
        }
        //dd($univs);
         return view('moodler::param.add-cours',compact('pnom','id','univs'));
    }*/

    public function saveAddUniv(Request $request,$id){
        $this->validate($request, [
          'univ' => 'required|integer',
          ]);
          $uid=$this->getUid();
          $param=ParamExport::where("id",$id)->where("user_id",$uid)->first();

          if(empty($param)){
              abort(404);
          }
          $requestData = $request->all();
            $param->niv_universite= $requestData['univ'];
            $param->save();
            $request->session()->flash('success', __("Modification effectuée"));
         // dd($requestData);
          return redirect(route('editParamExport',$id));
    }
    public function saveAddEtab(Request $request,$id){
        $this->validate($request, [
          'etab' => 'required|integer',
          ]);
          $uid=$this->getUid();
          $param=ParamExport::where("id",$id)->where("user_id",$uid)->first();

          if(empty($param)){
              abort(404);
          }
          $requestData = $request->all();
            $param->niv_entite = $requestData['etab'];
            $param->save();
            $request->session()->flash('success', __("Modification effectuée"));
         // dd($requestData);
          return redirect(route('editParamExport',$id));
    }
    public function saveAddCycle(Request $request,$id){
        $this->validate($request, [
          'cycle' => 'required|integer',
          ]);
          $uid=$this->getUid();
          $param=ParamExport::where("id",$id)->where("user_id",$uid)->first();

          if(empty($param)){
              abort(404);
          }
          $requestData = $request->all();
            $param->niv_cylce = $requestData['cycle'];
            $param->save();
            $request->session()->flash('success', __("Modification effectuée"));
         // dd($requestData);
          return redirect(route('editParamExport',$id));
    }
    public function saveAddAcad(Request $request,$id){
        $this->validate($request, [
          'acad' => 'required|integer',
          ]);
          $uid=$this->getUid();
          $param=ParamExport::where("id",$id)->where("user_id",$uid)->first();
          if(empty($param)){
              abort(404);
          }
            $requestData = $request->all();
            $param->niv_annee = $requestData['acad'];
            $param->save();
            $request->session()->flash('success', __("Modification effectuée"));
         // dd($requestData);
          return redirect(route('editParamExport',$id));
    }
    public function saveAddForm(Request $request,$id){
        $this->validate($request, [
          'filiere' => 'required|integer',
          ]);
          $uid=$this->getUid();
          $param=ParamExport::where("id",$id)->where("user_id",$uid)->first();
          if(empty($param)){
              abort(404);
          }
            $requestData = $request->all();
            $param->niv_filiere = $requestData['filiere'];
            $param->save();
            $request->session()->flash('success', __("Modification effectuée"));
         // dd($requestData);
          return redirect(route('editParamExport',$id));
    }
    public function saveAddSemestre(Request $request,$id){
        $this->validate($request, [
          'sem' => 'required|integer',
          ]);
          $uid=$this->getUid();
          $param=ParamExport::where("id",$id)->where("user_id",$uid)->first();
          if(empty($param)){
              abort(404);
          }
            $requestData = $request->all();
            $param->niv_semestre = $requestData['sem'];
            $param->save();
            $request->session()->flash('success', __("Modification effectuée"));
         // dd($requestData);
          return redirect(route('editParamExport',$id));
    }
    public function getSuggestions(){
        $n=$this->nb_paginate;
        $suggestions=Avis::latest()->paginate($n);
        return view('moodler::suggestions.index',compact('suggestions'));
    }
}
