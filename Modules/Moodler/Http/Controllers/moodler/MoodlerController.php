<?php
# MODEL 1
#####################################################################################
# NIVEAU 1 : UNIVERSITÉ                                                             #
# NIVEAU 2 : ENTITÉ / FORMATION                                                     #
# NIVEAU 3 : CYCLE                                                                  #
# NIVEAU 4 : FILIERE                                                                #
# NIVEAU 5 : ANNÉE ACADÉMIQUE                                                       #
# NIVEAU 6 : SEMESTRE                                                               #
# NIVEAU 7 : COURS                                                                  #
#####################################################################################
# MODEL 2
#####################################################################################
# NIVEAU 1 : ENTITÉ / FORMATION                                                     #
# NIVEAU 2 : CYCLE                                                                  #
# NIVEAU 3 : FILIERE                                                                #
# NIVEAU 4 : ANNÉE ACADÉMIQUE                                                       #
# NIVEAU 5 : SEMESTRE                                                               #
# NIVEAU 6 : COURS                                                                  #
#####################################################################################
# MODEL 3
#####################################################################################
# NIVEAU 1 : FILIERE                                                                #
# NIVEAU 2 : ANNÉE ACADÉMIQUE                                                       #
# NIVEAU 3 : SEMESTRE                                                               #
# NIVEAU 4 : COURS                                                                  #
#####################################################################################
namespace Modules\Moodler\Http\Controllers\Moodler;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Db as db;
use Modules\Moodler\Entities\Models\Rapport;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;

class MoodlerController extends Controller
{
    private $db;
    public $no_rapport,$nb_rapport_home;
    public function __construct(){
        $dbhost = '127.0.0.1';
        $dbuser = 'root';
        $dbpass = '';
        $dbname = 'bitnami_moodle';
        $this->initialiser();
        $this->createPermission();
        $db = new db($dbhost, $dbuser, $dbpass, $dbname);
        $this->db=$db;
    }
    public function initialiser(){
        $this->no_rapport=__('Aucun rapport');
        $this->nb_rapport_home=10;
    }
    public function index(Request $request)
    {
    //$value="Mot de passe modifié avec succès !";
    //$request->session()->flash('flash_message', $value);
    //dd('');
    $chemin=[
    ];
    $count_hebdo=$this->getNbRapportHebdo();
    $count_mensuel=$this->getNbRapportMensuel();
    $count_trimest=$this->getNbRapportTrimestriel();
    $last_m_date=$this->getHMLastdate();
    //$last_t_date='';
    $last_t_date=$this->getHTMLastdate();
    $rapports=$this->getLastRapport($this->nb_rapport_home);

    return view("moodler::index",compact('chemin','count_hebdo','count_mensuel','count_trimest','last_m_date','last_t_date','rapports'));
    }
    public function createPermission(){
       // $roleAdmin = Role::create(['name' => 'admin']);
        //$roleRespo = Role::create(['name' => 'responsable']);
        //$roleRespo = Role::create(['name' => 'Directeur']);
       // $permission = Permission::create(['name' => 'edit articles']);
    }
     public function getLastRapport($n){
         $n=intval($n)>0?$n:10;
         $user=Auth::user();
         $rap=NULL;
         if($user){
             $rap=Rapport::where('user_id',$user->id)->latest()->paginate($n);
        }

         return $rap;
     }
    public function getNbRapportHebdo(){
        $n=Rapport::where('type','Rapport hebdomadaire')->count();
        return $n;
    }
    public function getHLastdate(){
        $r=Rapport::latest()->where('type','Rapport hebdomadaire')->first();
        //$rdate=(date('D m Y, H:i', strtotime($r->created_at)));
         $rdate=$this->no_rapport;
         if($r){
         $rdate=(date('d M Y, H:i', strtotime($r->created_at)));
         }
         return $rdate;
    }
    public function getNbRapportMensuel(){
        $n=Rapport::where('type','Rapport mensuel')->count();
        return $n;
    }

    public function getHMLastdate(){
        $r=Rapport::latest()->where('type','Rapport mensuel')->first();
          $rdate=$this->no_rapport;
         if($r){
         $rdate=(date('d M Y, H:i', strtotime($r->created_at)));
         }
         return $rdate;
    }
    public function getNbRapportTrimestriel(){
    $n=Rapport::where('type','Rapport trimestriel')->count();
    return $n;
    }
    public function getHTMLastdate(){
        $r=Rapport::latest()->where('type','Rapport trimestriel')->first();
        $rdate=$this->no_rapport;
        if($r){
            $rdate=(date('D m Y, H:i', strtotime($r->created_at)));
        }
        return $rdate;
    }
    /**
     * Fonction de formatage des données
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Array
     */
    public function DbFormatData(){

        $categories=$this->getMoodleCat();
        $tab=null;
        foreach($categories as $t){
            #Vérification des niveaux
            $niveau=$t->depth;

            if($niveau==1){
               // echo "UNIVERSITÉ : ";
                $tab['uni'][]=[$t->path=>$t->name];

                //echo $t->name;echo"<br>";
               // dd($tab);
            }
            //echo"<hr>";
            if($niveau==2){
                //echo "ENTITÉ / FORMATION :";
                //echo $t->name;echo"<br>";
                $tab['entite'][]=[$t->path=>$t->name];
            }
            //echo"<hr>";
            if($niveau==3){
                //echo "CYCLE :";
                $tab['cycle'][]=['nom'=>$t->name,'path'=>$t->path];
                //echo $t->name;echo"<br>";
            }
           // echo"<hr>";
            if($niveau==4){
               // echo "FILIERE :";
                //echo $t->name;echo"<br>";
                $tab['filiere'][]=['nom'=>$t->name,'path'=>$t->path];
            }
            //echo"<hr>";
            if($niveau==5){
                //echo "ANNÉE ACADÉMIQUE : ";
                //echo $t->name;echo"<br>";
                $tab['annee'][]=[$t->path=>$t->name];
            }
            //echo"<hr>";
            if($niveau==6){
               // echo "SEMESTRE : ";
               // echo $t->name; echo"<br>";
               $tab['semestre'][]=[$t->path=>$t->name];
            }

            $path=$t->path;

            //echo($t->path); echo "<hr>";
        }
       // dd($tab);
       $data=$tab;
       return view('moodler.export.rapport-mensuel',compact('data'));

    }
    public function getMoodleCat(){
        return $this->db->q("select * from mdl_course_categories order by depth ASC");
    }
}
