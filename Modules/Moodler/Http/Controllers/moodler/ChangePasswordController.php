<?php

namespace Modules\Moodler\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Rules\MatchOldPassword;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
       /**
       * Create a new controller instance.
       *
       * @return void
       */
       public function __construct()
       {
            $this->middleware('auth');
       }



       /**
       * Show the application dashboard.
       *
       * @return \Illuminate\Contracts\Support\Renderable
       */

       public function index()
       {
            $user=Auth::user();
            return view('moodler::profil.change-password',compact('user'));
       }
       /**
       * Show the application dashboard.
       *
       * @return \Illuminate\Contracts\Support\Renderable
       */
       public function store(Request $request)
       {
            $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
            ]);
            User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
            $value="Mot de passe modifié avec succès !";
            $request->session()->flash('flash_message', $value);
             return redirect()->to('/home')->with('flash_message', __('OK !'));

        }
}
