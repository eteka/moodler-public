<?php

namespace Modules\Moodler\Entities;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable,HasRoles, LogsActivity;
//factory(Modules\Moodler\Entities\User::class)->create())
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom', 'email', 'password','sexe','pseudo','telephone','apropos','poste','photo', 'structure_id','etat_compte'
    ];

    protected static $logAttributes=['nom','prenom','email'];
    protected static $ignoreChangedAttributes=['password','updated_at'];
    protected static $logOnlyDirty=true;
    protected static $recordEvents=['created','updated'];
    public function getDescriptionForEvent(string $eventName):string{
        return __("Vous avez {$eventName} ");
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'date_inscription'=> 'datetime',
    ];
    public function fullname($v=0){
        if($v>0){
            return $this->prenom ." ".$this->nom;
        }
        return $this->nom ." ".$this->prenom;
    }
    public function sigle(){
        $mp=ucwords(substr($this->nom,0,1)).ucwords(substr($this->prenom,0,1));
        //$a=substr($this->nom,0,1);
        return $mp;
    }
    public function profilbg(){
        $bg="bg-primary";
        $nrand=rand(0,5);
        if($nrand==1){
            $bg="bg-secondary";
        }
        if($nrand==2){ $bg="bg-tertiary" ; }
        if($nrand==3){ $bg="bg-info" ; }
        if($nrand==4){ $bg="bg-warning" ; }
        if($nrand==5){ $bg="bg-danger" ; }
        return $bg;
    }

    /**
     * Afficher la structure de l'utilisateur
     */
    public function structure()
    {
        return $this->belongsTo('App\Models\Structure');
    }

    public function photoProfil(){
        $photo=$this->photo;
        //'storage/users/default-img.JPEG'
        if($photo==NULL){
            $photo='storage/moodler/users/default-img.jpg';
        }
        return $photo;
    }
}
