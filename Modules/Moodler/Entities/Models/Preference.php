<?php

namespace Modules\Moodler\Entities\Models;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'preferences';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['lang_default', 'nb_affichage', 'format_default', 'nom_default'];


}
