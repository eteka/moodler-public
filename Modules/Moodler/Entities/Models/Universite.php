<?php

namespace Modules\Moodler\Entities;

use Illuminate\Database\Eloquent\Model;

class Universite extends Model
{
    public function __construct($id,$nom,$sigle='',$u_id=''){
        $this->id=$id;
        $this->nom=$nom;
        $this->sigle=$nom;
        $this->universite_id=$nom;
    }
    protected $fillable = [
        'nom', 'sigle'
    ];

    public function entites()
    {
        return $this->hasMany('App\Models\Entite');
    }
}
