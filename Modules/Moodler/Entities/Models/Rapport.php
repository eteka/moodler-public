<?php

namespace Modules\Moodler\Entities\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Rapport extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rapports';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom','format','fichier', 'type', 'date_debut', 'date_fin', 'description','user_id','etat','taille'];

    public function dateAjout(){
        return Date("d/m/Y H\H:i\M\i\\n",strtotime($this->attributes['created_at']));
        //return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']) )->diffForHumans();
    }
    public function dateDebut(){
        #return Carbon::createFromTimeStamp(strtotime($this->attributes['date_debut']) )->diffForHumans();
        return Date("d/m/Y",strtotime($this->attributes['date_fin']));
    }
    public function dateFin(){

        //return Carbon::createFromTimeStamp(strtotime($this->attributes['date_fin']) )->diffForHumans();
        return Date("d/m/Y",strtotime($this->attributes['date_fin']));
    }
    public function Type(){
        return __($this->attributes['type']);
    }
    public function Etat(){
        $etat="-";
         $tab=[0=>"Non validé",1=>"Validé"];
        $get_icone=$this->attributes['etat'];
        $etat=isset($tab[$get_icone])?$tab[$get_icone]:$etat;
        return __($etat);
    }

    public function Icone(){
        $format="./storages/assets/img/icones/pdf.pngs";
        $tab=['pdf'=>"./storage/assets/img/icones/pdf.png","word"=>"./storage/assets/img/icones/word.png","excel"=>"./storage/assets/img/icones/excel.png",];
        $get_icone=strtolower($this->attributes['format']);
        $format=isset($tab[$get_icone])?$tab[$get_icone]:$format;
        return asset($format);
    }

}
