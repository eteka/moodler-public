<?php

namespace Modules\Moodler\Entities\Models;

use Illuminate\Database\Eloquent\Model;

class Avis extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'avis';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'prenom', 'email', 'tel', 'message'];


}
