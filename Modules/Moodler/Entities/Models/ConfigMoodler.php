<?php

namespace Modules\Moodler\Entities;

use Illuminate\Database\Eloquent\Model;

class ConfigMoodler extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'config_moodlers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom_structure', 'tel_structure', 'tel_structure', 'email_structure', 'description_structure', 'localisation_structure', 'server_bdd', 'nom_bdd', 'user_bdd', 'pwd_bdd', 'port_bdd', 'lang_default', 'fichier_default', 'format_default', 'type_secure', 'pays_id', 'timezone','user_id'];


}
