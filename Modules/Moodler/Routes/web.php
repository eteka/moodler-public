<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
#Auth::routes();

use Illuminate\Support\Facades\Auth;

include('auth.php');
#Auth::routes();

Route::get('/', 'PageController@index')->name('index');
//change-password
Route::prefix('moodler')->middleware(['auth'])->group(function() {
    Route::get('/', 'moodler\MoodlerController@index')->name('dashbord');
    #Profil
    Route::get('/profil/edit', 'moodler\ProfilController@editProfil')->name('editProfil');
    Route::patch('/profil/edit', 'moodler\ProfilController@updateProfil')->name('updateProfil');
    Route::get('/profil', 'moodler\ProfilController@profil')->name('profile');
    //Route::get('/profil', 'moodler\ProfilController@profil')->name('myprofil');
    #Route::get('/profil/activity', 'moodler\ProfilController@activites')->name('activites');
    #Route::get('/profil/settings', 'moodler\ProfilController@parametres')->name('parametres');
    #ChangePassword
    Route::get('change-password', 'moodler\ChangePasswordController@index')->name('getchangePassword');;
    Route::post('change-password', 'moodler\ChangePasswordController@store')->name('change.password');
    #Configuration
    Route::get('/welcome', 'admin\AdminController@welcome')->name('welcome');
    Route::get('/configuration', 'moodler\ConfigMoodlerController@configService')->name('configService');
    Route::get('/configuration/structure', 'moodler\ConfigMoodlerController@structure')->name('structure');
    Route::post('/configuration/structure', 'moodler\ConfigMoodlerController@saveStructure')->name('saveStructure');
    Route::get('/configuration/bdd', 'moodler\ConfigMoodlerController@configBdd')->name('configBdd');
    Route::post('/configuration/bdd', 'moodler\ConfigMoodlerController@saveBdd')->name('saveBdd');
    Route::get('/configuration/preferences', 'moodler\ConfigMoodlerController@preferences')->name('preferences');
    Route::post('/configuration/preferences',
    'moodler\ConfigMoodlerController@savePreferences')->name('savePreferences');
    Route::get('/configurations', 'moodler\ConfigMoodlerController@getConfig')->name('config');
    #Structure
    #Route::get('/structure/{id}', 'admin\AdminController@getStructure')->name('getStructure');

    #Universite
    Route::get('universite', 'moodler\RapportController@index');
    Route::post('universite', 'moodler\RapportController@index');
    #Paramètres
    Route::get('rapport', 'moodler\RapportController@rapport');
    Route::get('rapport/hebdomadaire', 'moodler\RapportController@rapportHebdo')->name('rapportHebdo');
    Route::get('rapport/mensuel', 'moodler\RapportController@rapportMensuel')->name('rapportMensuel');;
    Route::get('rapport/semestriel', 'moodler\RapportController@rapportSemestriel')->name('rapportSemestriel');;
    Route::get('parametres', 'moodler\ParametreController@getParametres')->name('getParametres');

    #Parametre d'exportation
    #Route::get('param-export', 'moodler\ParamExportController@index')->name('param-export.index');
    Route::get('param-export/news', 'moodler\ParamExportController@create')->name('param-export.create');;
    Route::post('param-export/store', 'moodler\ParamExportController@store')->name('param-export.store');;
    Route::get('param-export/edit/{id}', 'moodler\ParamExportController@edit')->name('param-export.edit');;
    Route::patch('param-export/update/{id}', 'moodler\ParamExportController@update')->name('param-export.update');;
    //Route::resource('param-export', 'ParamExportController');
    #Documentation
    Route::get('/documentation', 'admin\ProfilController@documentation')->name('documentation');

    #Export
    Route::get('/export', 'moodler\RapportController@export')->name('export');
    Route::get('/export/pdf', 'moodler\RapportController@exportPDF')->name('export.pdf');
    Route::get('/export/excel', 'moodler\RapportController@exportExcel')->name('export.excel');
    Route::get('/export/html', 'moodler\RapportController@exportHTML')->name('export.html');
    Route::get('/export/word', 'moodler\RapportController@exportWord')->name('export.word');
    Route::get('/export/dot', 'moodler\RapportController@exportOdt')->name('export.odt');
    Route::get('/exporter', 'moodler\RapportController@exporter')->name('exporter');

    Route::get('/export/excel', 'moodler\RapportController@exportExcel')->name('exporter');
    //exporter
    Route::get('/pdf', 'admin\ExportsController@pdf')->name('pdf');

    #Rapports
    Route::get('/rapport-hebdomadaire', 'moodler\RapportController@getRapportHebdo')->name('rapport-hebdo.create');
    Route::get('/rapport-mensuel', 'moodler\RapportController@getRapportMensuel')->name('rapport-mensuel.create');
    Route::get('/rapport-par-semestre','moodler\RapportController@getRapportSemestre')->name('rapport-semestre.create');
    #Liste des rapports
    Route::get('/rapport/liste-rapport', 'moodler\RapportController@getListeRapport')->name('liste-rapport');
    Route::get('/rapport/liste-hebdomadaire', 'moodler\RapportController@getListeHebdo')->name('liste-hebdo');
    Route::get('/rapport/liste-mensuel', 'moodler\RapportController@getListeMensuel')->name('liste-mensuel');
    Route::get('/rapport/liste-par-semestre', 'moodler\RapportController@getListeSemestre')->name('liste-semestre');

    #Statistiques
    Route::get('/statistiques', 'moodler\RapportController@getStatistiques')->name('getStatistiques');
    Route::get('/utilisateurs', 'moodler\ProfilController@getUsers')->name('getUsers');
    Route::get('/historique', 'moodler\RapportController@getHistorique')->name('getHistorique');
    Route::get('/consultation', 'moodler\RapportController@getConsultation')->name('getConsultation');
    Route::get('/suggestions', 'moodler\RapportController@getSuggestions')->name('getSuggestions');
    Route::get('/param-export', 'moodler\RapportController@getParamExport')->name('getParamExport');
    Route::get('/param-export/new', 'moodler\RapportController@newParamExport')->name('newParamExport');
    Route::post('/param-export/new', 'moodler\RapportController@saveParamExport')->name('saveParamExport');
    Route::get('/param-export/{id}', 'moodler\RapportController@editParamExport')->name('editParamExport');
    #Add Param
    Route::get('/param-export/{id}/add-univ', 'moodler\RapportController@showAddUniv')->name('showAddUniv');
    Route::post('/param-export/{id}/add-univ', 'moodler\RapportController@saveAddUniv')->name('saveAddUniv');
    Route::get('/param-export/{id}/add-etab', 'moodler\RapportController@showAddEtab')->name('showAddEtab');
    Route::post('/param-export/{id}/add-etab', 'moodler\RapportController@saveAddEtab')->name('saveAddEtab');
    Route::get('/param-export/{id}/add-cycle', 'moodler\RapportController@showAddCycle')->name('showAddCycle');
    Route::post('/param-export/{id}/add-cycle', 'moodler\RapportController@saveAddCycle')->name('saveAddCycle');
    Route::get('/param-export/{id}/add-annee-academique', 'moodler\RapportController@showAddAcad')->name('showAddAcad');
    Route::post('/param-export/{id}/add-annee-academique', 'moodler\RapportController@saveAddAcad')->name('saveAddAcad');
    Route::get('/param-export/{id}/add-formation', 'moodler\RapportController@showAddForm')->name('showAddForm');
    Route::post('/param-export/{id}/add-formation', 'moodler\RapportController@saveAddForm')->name('saveAddForm');
    Route::get('/param-export/{id}/add-semestre', 'moodler\RapportController@showAddSemestre')->name('showAddSemestre');
    Route::post('/param-export/{id}/add-semestre', 'moodler\RapportController@saveAddSemestre')->name('saveAddSemestre');
    Route::post('/param-export/active-config', 'moodler\RapportController@activeConfig')->name('activeConfig');

    Route::get('/configuration', 'moodler\RapportController@getConfig')->name('getConfig');
    Route::get('/profil', 'moodler\ProfilController@getProfil')->name('getProfil');
    Route::get('/password/resset', 'moodler\ProfilController@changePassword')->name('changePassword');
    Route::get('/user/preferences', 'moodler\ProfilController@userPreferences')->name('userConfig');
    Route::post('/user/preferences', 'moodler\ProfilController@SavePreferences')->name('saveConfig');

    Route::get('/user/{id}/edit-role', 'moodler\ProfilController@editRole')->name('editRole');
    Route::post('/user/{id}/edit-role', 'moodler\ProfilController@saveRole')->name('saveRole');


    #Blog
    //Route::get('/blog', 'moodler\BlogController@index')->name('blog.index');
});
Route::prefix('moodler')->group(function() {
#Les autres pages
Route::get('/docs', 'moodler\PageController@getDocs')->name('getDocs');
Route::get('/soumettre/avis', 'moodler\PageController@getAvis')->name('getAvis');
Route::post('/soumettre/avis', 'moodler\PageController@SaveAvis')->name('SaveAvis');
Route::get('/a-propos', 'moodler\PageController@getApropos')->name('getApropos');
Route::get('/blog', 'moodler\PageController@getblog')->name('blog.index');
Route::get('/contact', 'moodler\PageController@getContact')->name('getContact');
});
