<?php

namespace App;

use App\Invoice;
use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Database\Eloquent\Model;

class InvoicesExport implements FromView
{

    public function view(): View
    {
        return view('moodler.export.users', [
            'users' => User::all()
        ]);
    }
}
