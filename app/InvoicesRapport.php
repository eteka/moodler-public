<?php

namespace App;

use App\Invoice;
use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;
use App\Models\Db as db;
use PDF;
use Excel;
use App\InvoicesExport;
use App\InvoicesRapport;
use App\Models\Universite;
use App\Models\Entite;
use App\Models\Cycle;
use App\Models\Filiere;
use App\Models\AnneeAcademique;
use App\Models\Semestre;
use App\Models\Cours;

class InvoicesRapport implements FromView
{
    private $db;
    protected $table_categories="mdl_course_categories";
        private $table_cours="mdl_course";
    public function __construct(){
        $dbhost = '127.0.0.1';
        $dbuser = 'root';
        $dbpass = '';
        $dbname = 'bitnami_moodle';       

        $db = new db($dbhost, $dbuser, $dbpass, $dbname);
        $this->db=$db;
    }
    public function getData(){
        
        //$users=$this->db->q('Select * from mdl_course');
        //echo "- UNIVERSITE <br>-ENTITE <br>ANNÉE ACADEMIQUE";
        $categories=$this->getMoodleCat();
        $tab=null;
        foreach($categories as $t){
            #Vérification des niveaux
            $niveau=$t->depth;
            
            if($niveau==1 ){
               
               if($t->id==2){
               // echo "UNIVERSITÉ CHOISIE UP : ";
                $tab['univ'][]=[$t->path=>$t->name];
                
                // $t->name;echo"<br>";
               // dd($tab);
               }
            }
            //echo"<hr>";
            if($niveau==2){
                if($t->id==3){
                   // echo "ENTITE CHOISIE (IUT): ";
                    $tab['entite'][]=['nom'=>$t->name,'path'=>$t->path];
                    
                   // echo $t->name;echo"<br>";
                   // dd($tab);
                   }
            }
            
            if($niveau==3){
                if($t->id==16){
                     "CYLCE CHOISIE (LICENCE): ";
                    $tab['cycle'][]=['nom'=>$t->name,'path'=>$t->path];
                    
                    // $t->name;echo"<br>";
                   // dd($tab);
                   }
            }
           
           // echo"<hr>";
            if($niveau==4){
                {
                    //if($t->id!=4){
                        //echo "FILIERE CHOISIE (LICENCE): ";
                        $tab['filiere'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                    //    }
                    // $t->name;echo"<br>";
                    
                }
               
                
            }
            //echo"<hr>";
            if($niveau==5){
               
                if($t->id==17){
                    //echo "ANNÉE ACADEMIQUE CHOISIE (2019): ";
                    $tab['annee'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                    
                    // $t->name;echo"<br>";
                   // dd($tab);
                   }
            }
            //Les semestres
            #Ici je recupère les semestres de toutes l'année académique concernée
            if($niveau==6){
                //echo "SEMESTRE : ";
                //echo $t->name."==".$t->id; echo"<br>";
                $acad=17;
                if($t->parent==$acad){
                    $spath=$t->path;//."==============". $t->id."===============<hr>";
                    $t->id;
                    $tab_fil=explode("/",$spath);
                    $code_filiere=isset($tab_fil[4])?$tab_fil[4]:0;
                   // $fil_code=
                    $tab['semestre'][$code_filiere][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                    $tab['matiere'][$t->id]=$this->getMoodleCours($t->id);
                  // dd($tab['semestre']);
                }
            }

            $path=$t->path;
           
            //echo($t->path); echo "<hr>";
        }
        //dd($tab);
       return $tab;
       

      // return view('moodler.export.rapport-mensuel',compact('data'));
       
    }

    public function view(): View
    {
        
        $data=$this->rapport("x");
        //dd($data);
        return view("moodler.export.x-mensuel-excel",[
        'data' => $data
        ]);
        
    }
    public function getMoodleCat(){
        return $this->db->q("select * from ".$this->table_categories." order by depth ASC");
    }
    public function getMoodleCours($id){
        return $this->db->q("select * from ".$this->table_cours." where category=".$id);
    }
    public function rapport($c=""){
        
        //$users=$this->db->q('Select * from mdl_course');
        //echo "- UNIVERSITE <br>-ENTITE <br>ANNÉE ACADEMIQUE";
        $categories=$this->getMoodleCat();
        $tab=null;
        //$tab['univ'][]=$tab['entite'][]=$tab['cycle'][]=$tab['filiere'][]=$tab['annee'][]=$tab['semestre'][]=$tab['matiere'][]=null;
        foreach($categories as $t){
            #Vérification des niveaux
            $niveau=$t->depth;
            
            if($niveau==1 ){
               
               if($t->id==2){
               // echo "UNIVERSITÉ CHOISIE UP : ";
                $tab['univ'][]=[$t->path=>$t->name];
                
                // $t->name;echo"<br>";
               // dd($tab);
               }
            }
            //echo"<hr>";

            if($niveau==2){
                if($t->id==3){
                   // echo "ENTITE CHOISIE (IUT): ";
                    $tab['entite'][]=['nom'=>$t->name,'path'=>$t->path];
                    
                   // echo $t->name;echo"<br>";
                   // dd($tab);
                   }
            }
            
            if($niveau==3){
                if($t->id==16){
                     "CYLCE CHOISIE (LICENCE): ";
                    $tab['cycle'][]=['nom'=>$t->name,'path'=>$t->path];
                    
                     //$t->name;echo"<br>";
                   // dd($tab);
                   }
            }
           
           // echo"<hr>";
            if($niveau==4){
                {
                   // if($t->id>=0){
                    //echo "FILIERE CHOISIE (LICENCE): ";
                    $tab['annee'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                   // }
                    // $t->name;echo"<br>";                    
                }             
                
            }
            $acad=19;
            //echo"<hr>";
            if($niveau==5){
                //dd($t);
               //echo $t->parent."==========".$t->name;
                if($t->parent==$acad){
                    //echo "ANNÉE ACADEMIQUE CHOISIE (2019): ";
                    $tab['filiere'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                    
                     //echo "filiere=".$t->name."==".$t->id;echo"<br>";
                    //dd($tab);
                   }
            }
            //echo"<hr>";
           /* if($niveau==6){
               // dd($t);
               //echo $t->parent."==========".$t->name;
                if($t->parent==$acad){
                    //echo "ANNÉE ACADEMIQUE CHOISIE (2019): ";
                    $tab['semestre'][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                    
                     //$t->name;echo"<br>";
                   // dd($tab);
                   }
            }*/
            //Les semestres
            #Ici je recupère les semestres de toutes l'année académique concernée
           // $fil=
            if($niveau==6){
                //echo "SEMESTRE : ";
                //echo $t->name."==".$t->id; echo"<br>";
                //dd($t);
                //if($t->parent==$fil){
                    $spath=$t->path;//."==============". $t->id."===============<hr>";
                    
                    $tab_fil=explode("/",$spath);
                    $code_filiere=isset($tab_fil[5])?$tab_fil[5]:0;
                   // $fil_code=
                   //print_r($tab['filiere']);
                   //dd($tab_fil);
                    $tab['semestre'][$code_filiere][]=['id'=>$t->id,'nom'=>$t->name,'path'=>$t->path];
                    $gmat=$this->getMoodleCours($t->id);
                   // if($gmat!=null)
                    $tab['matiere'][$t->id]=$gmat;
                  // dd($tab['semestre']);
               // }
            }

            $path=$t->path;
           
            //echo($t->path); echo "<hr>";
        }
       //dd($tab);
       $data=$tab;
       if($c=="x"){
        return  $data;
       }
       //dd($data);
       //return (new InvoicesExport)->download('invoices.html', \Maatwebsite\Excel\Excel::HTML);

       //dd($data['semestre']);
       //$data = ['title' => 'Laravel 7 Generate PDF From View Example Tutorial'];

      

       return view('moodler.export.x-mensuel',compact('data'));
       
    }
}
