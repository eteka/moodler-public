<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class annee_academique extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'annee_academiques';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'description'];

    
}
