<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfilController extends Controller
{
    public function profil($id=0){
        if($id!=0){
            $user=User::where('username',$id)->first();
        }elseif(Auth::user()){
            $user=Auth::user();
        }
        if($user==null){
          return  abort(404);
        }
        return view('moodler.profil.index',compact('user'));
    }
    public function editProfil(){
        return view('moodler.profil.edit');
    }
    public function activites(){
        return view('moodler.profil.activites');
    }
    public function parametres(){
        return view('modler.profil.parametres');
    }
}
