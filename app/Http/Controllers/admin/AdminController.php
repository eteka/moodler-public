<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function rapport()
    {
        
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view("moodler.index");
    }

    /**
     * Page de Bienvenue si l'application n'est pas configurée.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function welcome()
    {
        return view("moodler.welcome");
    }
     /**
     * Configurer l'application.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function config()
    {
        return view("moodler.configuration");
    }
    /**
     * Configurer l'application.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function configBdd()
    {
        return view("moodler.configuration-bdd");
    }
    /**
     * Configurer l'application.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function preferences()
    {
        return view("moodler.configuration-preferences");
    }
}
