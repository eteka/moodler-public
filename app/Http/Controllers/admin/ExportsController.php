<?php

namespace App\Http\Controllers\admin;

use App\InvoicesExport;

use App\Http\Controllers\Controller;
use App\User;
use App\UsersExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
//use PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf;
use Dompdf\Dompdf;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;

class ExportsController extends Controller
{
    private $exporter;

    public function __construct(Exporter $exporter)
    {
        $this->exporter = $exporter;
    }
    
    public function exporte()
    {
        /*$u=User::all();
        dd($u);*/
        return $this->exporter->download(new UsersExport(), 'users_'.time().'.xlsx');
    }
    function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function pdf()
    {

        $dompdf = new Dompdf();
        $dompdf->loadHtml('hello world');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();
        //return (new UsersExport())->download('invoices.pdf', \Maatwebsite\Excel\Excel::DOMPDF);

        /*$mpdf = new Mpdf(['orientation' => 'L']);
        $mpdf->WriteHTML('<h1>Hello world!</h1>');
        return $mpdf->Output();*/
    }

    
    public function exporter() 
    {
        return Excel::download(new InvoicesExport, 'invoices.xlsx');
    }

}
