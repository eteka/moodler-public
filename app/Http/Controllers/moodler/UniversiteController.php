<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Universite;
use Illuminate\Http\Request;

class UniversiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $universite = Universite::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->orWhere('contact', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('localisation', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $universite = Universite::latest()->paginate($perPage);
        }

        return view('moodler.universite.index', compact('universite'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('moodler.universite.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Universite::create($requestData);

        return redirect('moodler/universite')->with('flash_message', 'Universite added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $universite = Universite::findOrFail($id);

        return view('moodler.universite.show', compact('universite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $universite = Universite::findOrFail($id);

        return view('moodler.universite.edit', compact('universite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $universite = Universite::findOrFail($id);
        $universite->update($requestData);

        return redirect('moodler/universite')->with('flash_message', 'Universite updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Universite::destroy($id);

        return redirect('moodler/universite')->with('flash_message', 'Universite deleted!');
    }
}
