<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class ProfilController extends Controller
{

    public function getUsers (Request $request){
        $users=User::orderBy('id',"DESC")->paginate(15);
        return view('moodler.users.index',compact("users"));
    }
    public function getProfil  (){
        $user= Auth::user();
       return  view('profil.index',compact('user'));
    }
    public function changePassword  (){
        dd("changePassword  ");
    }
}
