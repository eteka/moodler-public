<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\annee_academique;
use Illuminate\Http\Request;

class annee_academiqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $annee_academique = annee_academique::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $annee_academique = annee_academique::latest()->paginate($perPage);
        }

        return view('moodler.annee_academique.index', compact('annee_academique'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('moodler.annee_academique.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        annee_academique::create($requestData);

        return redirect('moodler/annee_academique')->with('flash_message', 'annee_academique added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $annee_academique = annee_academique::findOrFail($id);

        return view('moodler.annee_academique.show', compact('annee_academique'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $annee_academique = annee_academique::findOrFail($id);

        return view('moodler.annee_academique.edit', compact('annee_academique'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $annee_academique = annee_academique::findOrFail($id);
        $annee_academique->update($requestData);

        return redirect('moodler/annee_academique')->with('flash_message', 'annee_academique updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        annee_academique::destroy($id);

        return redirect('moodler/annee_academique')->with('flash_message', 'annee_academique deleted!');
    }
}
