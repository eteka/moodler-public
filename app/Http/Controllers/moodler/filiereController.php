<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\filiere;
use Illuminate\Http\Request;

class filiereController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $filiere = filiere::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $filiere = filiere::latest()->paginate($perPage);
        }

        return view('moodler.filiere.index', compact('filiere'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('moodler.filiere.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        filiere::create($requestData);

        return redirect('moodler/filiere')->with('flash_message', 'filiere added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $filiere = filiere::findOrFail($id);

        return view('moodler.filiere.show', compact('filiere'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $filiere = filiere::findOrFail($id);

        return view('moodler.filiere.edit', compact('filiere'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $filiere = filiere::findOrFail($id);
        $filiere->update($requestData);

        return redirect('moodler/filiere')->with('flash_message', 'filiere updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        filiere::destroy($id);

        return redirect('moodler/filiere')->with('flash_message', 'filiere deleted!');
    }
}
