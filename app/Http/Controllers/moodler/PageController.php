<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function getDocs ()
    {
        dd('getDocs');
    }

    public function getAvis()
    {
        dd('getDocs');
    }
    public function getApropos ()
    {
        dd('getApropos ');
    }
    public function getContact ()
    {
        dd('getContact ');
    }
    public function setLang(String $locale)
    {
        echo $locale = in_array($locale, config('app.locales')) ? $locale : config('app.fallback_locale');
        session(['locale' => $locale]);
        $locale = session ('locale');
        return back();
    }
}
