<?php
# MODEL 1
#####################################################################################
# NIVEAU 1 : UNIVERSITÉ                                                             #
# NIVEAU 2 : ENTITÉ / FORMATION                                                     #
# NIVEAU 3 : CYCLE                                                                  #
# NIVEAU 4 : FILIERE                                                                #
# NIVEAU 5 : ANNÉE ACADÉMIQUE                                                       #
# NIVEAU 6 : SEMESTRE                                                               #
# NIVEAU 7 : COURS                                                                  #
#####################################################################################
# MODEL 2
#####################################################################################
# NIVEAU 1 : ENTITÉ / FORMATION                                                     #
# NIVEAU 2 : CYCLE                                                                  #
# NIVEAU 3 : FILIERE                                                                #
# NIVEAU 4 : ANNÉE ACADÉMIQUE                                                       #
# NIVEAU 5 : SEMESTRE                                                               #
# NIVEAU 6 : COURS                                                                  #
#####################################################################################
# MODEL 3
#####################################################################################
# NIVEAU 1 : FILIERE                                                                #
# NIVEAU 2 : ANNÉE ACADÉMIQUE                                                       #
# NIVEAU 3 : SEMESTRE                                                               #
# NIVEAU 4 : COURS                                                                  #
#####################################################################################
namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Db as db;

class MoodlerController extends Controller
{
    private $db;
    public function __construct(){
        $dbhost = '127.0.0.1';
        $dbuser = 'root';
        $dbpass = '';
        $dbname = 'bitnami_moodle';

        $db = new db($dbhost, $dbuser, $dbpass, $dbname);
        $this->db=$db;
    }
    public function rapport(){
        
        //$users=$this->db->q('Select * from mdl_course');
        $categories=$this->getMoodleCat();
        $tab=null;
        foreach($categories as $t){
            #Vérification des niveaux
            $niveau=$t->depth;
            
            if($niveau==1){
               // echo "UNIVERSITÉ : ";
                $tab['uni'][]=[$t->path=>$t->name];
                
                //echo $t->name;echo"<br>";
               // dd($tab);
            }
            //echo"<hr>";
            if($niveau==2){
                //echo "ENTITÉ / FORMATION :";
                //echo $t->name;echo"<br>";
                $tab['entite'][]=[$t->path=>$t->name];
            }
            //echo"<hr>";
            if($niveau==3){
                //echo "CYCLE :";
                $tab['cycle'][]=['nom'=>$t->name,'path'=>$t->path];
                //echo $t->name;echo"<br>";
            }
           // echo"<hr>";
            if($niveau==4){
               // echo "FILIERE :";
                //echo $t->name;echo"<br>";
                $tab['filiere'][]=['nom'=>$t->name,'path'=>$t->path];
            }
            //echo"<hr>";
            if($niveau==5){
                //echo "ANNÉE ACADÉMIQUE : ";
                //echo $t->name;echo"<br>";
                $tab['annee'][]=[$t->path=>$t->name];
            }
            //echo"<hr>";
            if($niveau==6){
               // echo "SEMESTRE : ";
               // echo $t->name; echo"<br>";
               $tab['semestre'][]=[$t->path=>$t->name];
            }

            $path=$t->path;
           
            //echo($t->path); echo "<hr>";
        }
       // dd($tab);
       $data=$tab;
       return view('moodler.export.rapport-mensuel',compact('data'));
       
    }
    public function getMoodleCat(){
        return $this->db->q("select * from mdl_course_categories order by depth ASC");
    }
}
