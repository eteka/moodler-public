<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Preference;
use Illuminate\Http\Request;

class PreferencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $preferences = Preference::where('lang_default', 'LIKE', "%$keyword%")
                ->orWhere('nb_affichage', 'LIKE', "%$keyword%")
                ->orWhere('format_default', 'LIKE', "%$keyword%")
                ->orWhere('nom_default', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $preferences = Preference::latest()->paginate($perPage);
        }

        return view('moodler.preferences.index', compact('preferences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('moodler.preferences.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Preference::create($requestData);

        return redirect('moodler/preferences')->with('flash_message', 'Preference added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $preference = Preference::findOrFail($id);

        return view('moodler.preferences.show', compact('preference'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $preference = Preference::findOrFail($id);

        return view('moodler.preferences.edit', compact('preference'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $preference = Preference::findOrFail($id);
        $preference->update($requestData);

        return redirect('moodler/preferences')->with('flash_message', 'Preference updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Preference::destroy($id);

        return redirect('moodler/preferences')->with('flash_message', 'Preference deleted!');
    }
}
