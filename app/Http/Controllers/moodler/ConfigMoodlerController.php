<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use Auth;
use App\Models\ConfigMoodler;
use Illuminate\Http\Request;
use Redirect;


class ConfigMoodlerController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function structure(){
        $user=Auth::user();
        $uid=$user?$user->id:0;

        $config= ConfigMoodler::where('user_id',$uid)->first();

        if(!empty($config)){

            $config->nom= $config->nom_structure;
            $config->localisation= $config->localisation_structure;
            $config->tel=$config->tel_structure;
            $config->email=$config->email_structure;
            $config->description=$config->description_structure;
            //$config->serveur="admin@localhost";
        }
        //dd($config);
        return view('moodler.config.structure',compact('config'));
    }
    public function saveStructure(Request $request){
        $requestData = $request->all();
        $this->validate($request, [
            'nom' => 'required',
            //'logo' => 'image|max:4096',
			'localisation_structure' => 'string;max:150',
			'email' => 'email|max:200',
            'description_structure' => 'max:1525',

            ]);

        $requestData = $request->all();
        $fichier="";
        if ($request->hasFile('logo')) {
        $requestData['logo'] = $request->file('logo')
            ->store('uploads', 'public');
            $fichier= $requestData['logo'];
        }
        if(Auth::user() ){
            $uid=Auth::user()->id;
            $data=[
                'nom_structure'=>$requestData['nom'],
                'localisation_structure'=>$requestData['localisation'],
                'tel_structure'=>$requestData['tel'],
                'email_structure'=>$requestData['email'],
                'description_structure'=>$requestData['description'],
                "user_id"=>$uid
            ];
            $data["logo"]=$fichier;
           //dd($data);
            #Vérifier si un enrégistrement existe
            $configExist=ConfigMoodler::where('user_id',$uid)->first();
            if($configExist!=NULL){
                #Mise à jour
                $configExist->update($data);
                return redirect(route('configBdd'));
            }else{
                #Enrégistrement

                $data["logo"]=$fichier;

                //array_push($data, "", "raspberry");
                //dd($data);
                ConfigMoodler::create($data);
                return redirect(route('configBdd'));
            }

        }

       return back()->withInputs();
    }
    /**
     * Affichage du formulaire de modification des paramètres de la base de données.
     *
     * @return \Illuminate\View\View
     */
    public function configBdd()
    {
        # code...
        $user=Auth::user();
        $uid=$user?$user->id:0;

        $config= ConfigMoodler::where('user_id',$uid)->first();
        if(!empty($config)){

            $config->serveur= $config->server_bdd;
            $config->base_de_donnee= $config->nom_bdd;
            $config->utilisateur=$config->user_bdd;
            $config->port=$config->port_bdd;
            $config->mot_de_passe=$config->pwd_bdd;
            //$config->serveur="admin@localhost";
        }

        return view('moodler.config.bdd',compact('config'));
    }

    /**
     * Sauvegarder les paramètres de la base de données.
     *
     * @return \Illuminate\View\View
     */
    public function saveBddParam(Request $request)
    {
        $requestData = $request->all();
        $this->validate($request, [
            'serveur' => 'required|max:50',
            'base_de_donnee' => 'required|max:150',
			'utilisateur' => 'required|max:100',
            ]);
            #Vérifier si l'utilisateur est connecté
            $user=Auth::user();
           if($user && $user->hasRole("admin"))
           {
            $uid=Auth::user()->id;
            $data=[
                'server_bdd'=>$requestData['serveur'],
                'nom_bdd'=>$requestData['base_de_donnee'],
                'user_bdd'=>$requestData['utilisateur'],
                'port_bdd'=>$requestData['port'],
                'pwd_bdd'=>$requestData['mot_de_passe'],
                "user_id"=>$uid
            ];
            #Vérifier si un enrégistrement existe
            $configExist=ConfigMoodler::where('user_id',$uid)->first();
            if($configExist!=NULL){
                #Mise à jour
                $configExist->update($data);
                return redirect(route('savePreferences'));
            }else{
                #Enrégistrement
                $fichier="";
                ConfigMoodler::create($data);
                return redirect(route('savePreferences'));
            }
        }
        return  Redirect::back()->withInput();
    }

    public function preferences(){
        # code...
        $user=Auth::user();
        $uid=$user?$user->id:0;

        $config= ConfigMoodler::where('user_id',$uid)->first();
        if(!empty($config)){

            $config->langue= $config->lang_default;
            $config->fuseau_horaire= $config->timezone;
            $config->format=$config->format_default;
            $config->fichier=$config->fichier_default;
            //$config->mot_de_passe=$config->pwd_bdd;
            //$config->serveur="admin@localhost";
        }
        $langues=["fr"=>"Français","en"=>"Anglais"];
        $fuseau=["bdd"=>"Fuseau horaire de la base de données","local"=>"Fuseau horaire local"];
        $fichiers=["nom"=>"Utiliser le nom de la structure","date"=>"Utiliser la date d'exportation"];
        $formats=["csv"=>"Format Excel CSV","xlsx"=>"Format Excel (xls)","pdf"=>"Format PDF "];

        return view('moodler.config.preferences',compact('config',"langues","fuseau","fichiers","formats"));
    }

    public function savePreferences(Request $request){
        # code...
        $requestData = $request->all();
        $this->validate($request, [
            'langue' => 'required|in:fr,en',
            'fuseau_horaire' => 'required|in:bdd, local',
			'format' => 'required|in:csv,xlsx,pdf',
			'fichier' => 'required|in:date,nom',
            //'mot_de_passe' => 'required|max:50',
            ]);

           if(Auth::user() ){
            $uid=Auth::user()->id;
            $data=[
                'lang_default'=>$requestData['langue'],
                'timezone'=>$requestData['fuseau_horaire'],
                'format_default'=>$requestData['format'],
                'fichier_default'=>$requestData['fichier'],
                "user_id"=>$uid
            ];
            #Vérifier si un enrégistrement existe
            $configExist=ConfigMoodler::where('user_id',$uid)->first();
            if($configExist!=NULL){
                #Mise à jour
                $configExist->update($data);
                return redirect(route('dashbord'));
            }else{
                #Enrégistrement
                $fichier="";

                ConfigMoodler::create($data);
                return redirect(route('dashbord'));
            }
        }
        return  Redirect::back()->withInput();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $configmoodler = ConfigMoodler::where('nom_structure', 'LIKE', "%$keyword%")
                ->orWhere('logo_tsructure', 'LIKE', "%$keyword%")
                ->orWhere('tel_tsructure', 'LIKE', "%$keyword%")
                ->orWhere('email_tructure', 'LIKE', "%$keyword%")
                ->orWhere('description_structure', 'LIKE', "%$keyword%")
                ->orWhere('localisation_structure', 'LIKE', "%$keyword%")
                ->orWhere('server_bdd', 'LIKE', "%$keyword%")
                ->orWhere('nom_bdd', 'LIKE', "%$keyword%")
                ->orWhere('user_bdd', 'LIKE', "%$keyword%")
                ->orWhere('pwd_bdd', 'LIKE', "%$keyword%")
                ->orWhere('port_bdd', 'LIKE', "%$keyword%")
                ->orWhere('lang_default', 'LIKE', "%$keyword%")
                ->orWhere('fichier_default', 'LIKE', "%$keyword%")
                ->orWhere('format_default', 'LIKE', "%$keyword%")
                ->orWhere('type_secure', 'LIKE', "%$keyword%")
                ->orWhere('pays', 'LIKE', "%$keyword%")
                ->orWhere('timezone', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $configmoodler = ConfigMoodler::latest()->paginate($perPage);
        }

        //return view('moodler.config-moodler.index', compact('configmoodler'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('moodler.config-moodler.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        ConfigMoodler::create($requestData);

        return redirect('moodler/config-moodler')->with('flash_message', 'ConfigMoodler added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $configmoodler = ConfigMoodler::findOrFail($id);

        return view('moodler.config-moodler.show', compact('configmoodler'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $configmoodler = ConfigMoodler::findOrFail($id);

        return view('moodler.config-moodler.edit', compact('configmoodler'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $configmoodler = ConfigMoodler::findOrFail($id);
        $configmoodler->update($requestData);

        return redirect('moodler/config-moodler')->with('flash_message', 'ConfigMoodler updated!');
    }
    public function welcome()
    {
    return view("moodler.welcome");
    }
    /**
    * Configurer l'application.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function config()
    {
    return view("moodler.configuration");
    }
}
