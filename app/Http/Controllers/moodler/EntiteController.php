<?php

namespace App\Http\Controllers\moodler;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entite;
use Illuminate\Http\Request;

class EntiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $entite = Entite::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->orWhere('contact', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('localisation', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $entite = Entite::latest()->paginate($perPage);
        }

        return view('moodler.entite.index', compact('entite'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('moodler.entite.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Entite::create($requestData);

        return redirect('moodler/entite')->with('flash_message', 'Entite added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $entite = Entite::findOrFail($id);

        return view('moodler.entite.show', compact('entite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $entite = Entite::findOrFail($id);

        return view('moodler.entite.edit', compact('entite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $entite = Entite::findOrFail($id);
        $entite->update($requestData);

        return redirect('moodler/entite')->with('flash_message', 'Entite updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Entite::destroy($id);

        return redirect('moodler/entite')->with('flash_message', 'Entite deleted!');
    }
}
