<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rapport extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rapports';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'type', 'date_debut', 'date_fin', 'description'];

    
}
