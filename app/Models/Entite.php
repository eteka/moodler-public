<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entite extends Model
{
    public function __construct($id,$nom,$sigle='',$u_id=''){
        $this->id=$id;
        $this->nom=$nom;
        $this->sigle=$nom;
        $this->universite_id=$nom;
    }
    protected $fillable = [
        'nom', 'sigle','universite_id'
    ];
    public function universite()
    {
        return $this->belongsTo('App\Models\Universite');
    }
    public function cycle()
    {
        return $this->hasMany('App\Models\Entite');
    }
}
