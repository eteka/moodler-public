<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
   /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "structures";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','logo', 'image', 'description','localisation', 'user_id'
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
