<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParamExport extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'param_exports';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom_configuration','description','niv_universite', 'niv_entite', 'niv_cylce', 'niv_annee', 'niv_filiere', 'niv_semestre', 'niv_cours','user_id'];

    
}
