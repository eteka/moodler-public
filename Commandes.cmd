php artisan crud:generate config_structure --fields='nom#string; logo#string;telephone#string;email#string;description#text;localisation#string' --view-path=moodler --controller-namespace=moodler --route-group=moodler
php artisan crud:generate ConfigMoodler --fields='nom_structure#string; logo_tsructure#string;tel_tsructure#string;email_tructure#string;description_structure#text;localisation_structure#string;server_bdd#string;nom_bdd#string;user_bdd#string;pwd_bdd#string;port_bdd#integer;lang_default#string;fichier_default#string;format_default#string;type_secure#string;pays#string;timezone#datetime' --view-path=moodler --controller-namespace=moodler --route-group=moodler
    php artisan crud:generate TP  --fields='nom_structure#file;pays#select#options={}' --validations="nom_structure#required;nom_structure#mimes:jpeg,jpg,png;nom_structure#max:4000;pays#min:1;pays#max:15"

php artisan crud:generate Universite --fields='nom#string;logo#string;contact#string;email#string;description#text;localisation#string' --view-path=moodler --controller-namespace=moodler --route-group=moodler
php artisan crud:generate Universite --fields='nom#string;logo#string;contact#string;email#string;description#text;localisation#string' --view-path=moodler --controller-namespace=moodler --route-group=moodler
php artisan crud:generate Entite --fields='nom#string;logo#string;contact#string;email#string;description#text;localisation#string' --view-path=moodler --controller-namespace=moodler --route-group=moodler
php artisan crud:generate Cycle --fields='nom#string;description#text' --view-path=moodler --controller-namespace=moodler --route-group=moodler
php artisan crud:generate annee_academique --fields='nom#string;description#text' --view-path=moodler --controller-namespace=moodler --route-group=moodler
php artisan crud:generate filiere --fields='nom#string;description#text' --view-path=moodler --controller-namespace=moodler --route-group=moodler

php artisan crud:generate Rapport --fields='nom#string;type#string;date_debut#timestamp;date_fin#timestamp;description#text' --view-path=moodler --controller-namespace=moodler --route-group=moodler
php artisan crud:generate Avis --fields='nom#string;prenom#string;email#timestamp;tele#string;message#text' --view-path=moodler --controller-namespace=moodler --route-group=moodler

php artisan crud:generate Preferences --fields='lang_default#string;nb_affichage#integer;format_default#string;nom_default#string' --view-path=moodler --controller-namespace=moodler --route-group=moodler
