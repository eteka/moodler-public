@extends('layouts.app')

@section('title',"Configuration")

@section('content')
<div class="main">
    <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
           
          </ol>
          <h6 class="slim-pagetitle">
            Configuration
          </h6>
    </div>
    
   
    <div class="col-lg-8">
            <form>
              <!-- Title -->
              <div class="actions-toolbar py-2 mb-4">
                <h5 class="mb-1">Saved addresses</h5>
                <p class="text-sm text-muted mb-0">Use one of your saved addresses for fast checkout.</p>
              </div>
              <!-- Table of addresses -->
              <div class="table-responsive">
                <table class="table table-cards align-items-center">
                  <tbody class="list">
                    <tr>
                      <th scope="row">
                        <div class="custom-control custom-checkbox">
                          <input type="radio" class="custom-control-input" name="radio-address" id="tbl-addresses-check-1" checked="">
                          <label class="custom-control-label" for="tbl-addresses-check-1"></label>
                        </div>
                      </th>
                      <td>
                        <span class="font-weight-600 text-dark">Address 1</span><span class="badge badge-pill badge-soft-info ml-2">Primary</span></td>
                      <td>
                        <p class="mb-0 text-muted text-limit text-sm">1333 Deerfield, State College PA, 16803</p>
                      </td>
                      <td>
                        <div class="actions">
                          <div class="dropdown">
                            <a class="action-item" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-ellipsis-v"></i></a>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(11px, 36px, 0px);">
                              <a class="dropdown-item" href="#"><i class="far fa-edit text-muted"></i>Edit address</a>
                              <a class="dropdown-item" href="#"><i class="far fa-trash-alt text-danger"></i>Move to trash</a>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr class="table-divider"></tr>
                    <tr>
                      <th scope="row">
                        <div class="custom-control custom-checkbox">
                          <input type="radio" class="custom-control-input" name="radio-address" id="tbl-addresses-check-2">
                          <label class="custom-control-label" for="tbl-addresses-check-2"></label>
                        </div>
                      </th>
                      <td>
                        <span class="font-weight-600 text-dark">Address 2</span></td>
                      <td>
                        <p class="mb-0 text-muted text-limit text-sm">2047 Main Street, State Chicago CH, 20067</p>
                      </td>
                      <td>
                        <div class="actions">
                          <div class="dropdown">
                            <a class="action-item" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-ellipsis-v"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                              <a class="dropdown-item" href="#"><i class="far fa-edit text-muted"></i>Edit address</a>
                              <a class="dropdown-item" href="#"><i class="far fa-trash-alt text-danger"></i>Move to trash</a>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr class="table-divider"></tr>
                    <tr>
                      <th scope="row">
                        <div class="custom-control custom-checkbox">
                          <input type="radio" class="custom-control-input" name="radio-address" id="tbl-addresses-check-3">
                          <label class="custom-control-label" for="tbl-addresses-check-3"></label>
                        </div>
                      </th>
                      <td>
                        <span class="font-weight-600 text-dark">Address 3</span></td>
                      <td>
                        <p class="mb-0 text-muted text-limit text-sm">5078 Third Street, State New York NY, 33006</p>
                      </td>
                      <td>
                        <div class="actions">
                          <div class="dropdown">
                            <a class="action-item" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-ellipsis-v"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                              <a class="dropdown-item" href="#"><i class="far fa-edit text-muted"></i>Edit address</a>
                              <a class="dropdown-item" href="#"><i class="far fa-trash-alt text-danger"></i>Move to trash</a>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="mt-5">
                <!-- Title -->
                <div class="actions-toolbar py-2 mb-4">
                  <h5 class="mb-1">Add new address</h5>
                  <p class="text-sm text-muted mb-0">Fill in your address info for upcoming orders or payments.</p>
                </div>
                <!-- New address form -->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">First name</label>
                      <input class="form-control" type="text" placeholder="Enter your first name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">Last name</label>
                      <input class="form-control" type="text" placeholder="Also your last name">
                    </div>
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label">Address</label>
                      <input class="form-control" type="text" placeholder="Address, Number">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group focused">
                      <label class="form-control-label">Country</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="Country" data-select2-id="1" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="3">Select your country</option>
                        <option value="1">Bulgaria</option>
                        <option value="2">Croatia</option>
                        <option value="3">Denmark</option>
                        <option value="4">Finland</option>
                        <option value="5">Greece</option>
                        <option value="6">Romania</option>
                        <option value="7">United Kingdom</option>
                      </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="2" style="width: 350px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" title="Country" tabindex="0" aria-labelledby="select2-np79-container"><span class="select2-selection__rendered" id="select2-np79-container" role="textbox" aria-readonly="true" title="Select your country">Select your country</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group focused">
                      <label class="form-control-label">City</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="City" data-select2-id="4" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="6">Select your city</option>
                        <option value="1">Bucharest</option>
                        <option value="2">Bacau</option>
                        <option value="3">Cluj Napoca</option>
                        <option value="4">Piatra Neamt</option>
                        <option value="5">Sibiu</option>
                        <option value="6">Timisoara</option>
                      </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="5" style="width: 350px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" title="City" tabindex="0" aria-labelledby="select2-rr47-container"><span class="select2-selection__rendered" id="select2-rr47-container" role="textbox" aria-readonly="true" title="Select your city">Select your city</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">Postal code</label>
                      <input class="form-control" type="text" placeholder="Address, Number">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">Phone</label>
                      <input class="form-control" type="text" placeholder="+40-777 245 549">
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-5">
                <!-- Title -->
                <div class="actions-toolbar py-2 mb-4">
                  <h5 class="mb-1">Shipping method</h5>
                  <p class="text-sm text-muted mb-0">Fill in your address info for upcoming orders or payments.</p>
                </div>
                <!-- Shipping method options -->
                <div class="row row-grid mt-4">
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-body">
                        <div class="row align-items-center">
                          <div class="col-8">
                            <div class="custom-control custom-checkbox">
                              <input type="radio" name="shipping-method" class="custom-control-input" id="shipping-standard">
                              <label class="custom-control-label text-dark font-weight-bold" for="shipping-standard">Standard Delivery</label>
                            </div>
                          </div>
                          <div class="col-4 text-right">
                            <span class="h6">Free</span>
                          </div>
                        </div>
                        <p class="text-muted text-sm mt-3 mb-0">Estimated 10-20 days shipping. Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-body">
                        <div class="row align-items-center">
                          <div class="col-8">
                            <div class="custom-control custom-checkbox">
                              <input type="radio" name="shipping-method" class="custom-control-input" id="shipping-fast">
                              <label class="custom-control-label text-dark font-weight-bold" for="shipping-fast">Fast Delivery</label>
                            </div>
                          </div>
                          <div class="col-4 text-right">
                            <span class="h6">$25 USD</span>
                          </div>
                        </div>
                        <p class="text-muted text-sm mt-3 mb-0">Estimated 3-5 days shipping. Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-4 text-right">
                <a href="shop-landing.html" class="btn btn-link text-sm text-dark font-weight-bold">Return to shop</a>
                <button type="button" class="btn btn-sm btn-success">Next step</button>
              </div>
            </form>
          </div>
</div>
@endsection