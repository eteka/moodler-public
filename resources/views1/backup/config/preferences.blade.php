@extends('layouts.app')

@section('title',__("Configuration des préférences"))

@section('content')
<div class="main">
    <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
           
          </ol>
          <h6 class="slim-pagetitle">
           {{__('Configuration')}} 
          </h6>
    </div>
    
    <div class="section-wrapper mg-t-20 col-sm-9">
          <label class="section-title">{{__('Préférences')}}</label>
          <p class="mg-b-20 mg-sm-b-40">{{__("Veuillez renseigner les vos préférences")}}</p>

          <div id="wizard6"  role="application" class="wizard wizard-style-2  clearfix">
              <div class="steps clearfix"><ul role="tablist">
                  <li role="tab" class="first disabled" aria-disabled="false" aria-selected="true">
                      <a id="wizard6-t-0" href="{{route('structure')}}" aria-controls="wizard6-p-0">
                          <span class="number">1</span> <span class="title">Information sur la structure</span></a>
                        </li>
                          <li role="tab" class="disabled" aria-disabled="true">
                              <a id="wizard6-t-1" href="{{route('configBdd')}}" aria-controls="wizard6-p-1">
                                  <span class="number">2</span> 
                                  <span class="title">La base de données </span></a></li>
                                  <li role="tab" class="current last" aria-disabled="true">
                                      <a id="wizard6-t-2" href="{{route('preferences')}}" aria-controls="wizard6-p-2">
                                          <span class="number">3</span> <span class="title">Préférences</span></a></li>
                                        </ul></div>
                         
                          <div class="content clearfix">
                               <section id="wizard6-p-0" role="tabpanel" aria-labelledby="wizard6-h-0" class="body current" aria-hidden="false">
                    {!! Form::model($config, ['method' => 'POST', 'route' => 'savePreferences']) !!}
                         @csrf
                    <div class="form-layout form-layout-4">
                        <div class="row">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                            <div class="col-sm-12">
                                <div class="list-group">
                                    <div class="list-group-item pd-y-20">
                                    <div class="media">
                                        <div class="d-flex mg-r-10 wd-50">
                                        <i class="icon ion-earth tx-muted tx-40 tx"></i>
                                        </div><!-- d-flex -->
                                        <div class="media-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    
                                                <h6 class="tx-inverse mt-3">
                                                    <b>{{__('Langue par défaut')}}</b></h6>
                                                
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="mg-b-0 {{ $errors->has('langue') ? 'has-error' : ''}}">
                                                    {{ Form::select('langue',$langues,null,['class'=>"form-control form-control-sm mt-2","placeholder"=>__("Langue par défaut")]) }}
                                
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="list-group-item pd-y-20">
                                    <div class="media">
                                        <div class="d-flex mg-r-10 wd-50">
                                        <i class="icon ion-earth tx-muted tx-40 tx"></i>
                                        </div><!-- d-flex -->
                                        <div class="media-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    
                                                <h6 class="tx-inverse mt-3">
                                                    <b>{{__('Fuseau horaire')}}</b></h6>
                                                
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="mg-b-0 {{ $errors->has('fuseau_horaire') ? 'has-error' : ''}}">
                                                    {{ Form::select('fuseau_horaire',$fuseau,null,['class'=>"form-control form-control-sm mt-2","placeholder"=>__("Fuseau horaire")]) }}
                                
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="list-group-item pd-y-20">
                                    <div class="media">
                                        <div class="d-flex mg-r-10 wd-50">
                                        <i class="icon ion-archive tx-muted tx-40 tx"></i>
                                        </div><!-- d-flex -->
                                        <div class="media-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    
                                                <h6 class="tx-inverse mt-3">
                                                    <b>{{__("Renommer le fichier d'exportation")}}</b></h6>
                                                
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="mg-b-0 {{ $errors->has('fichier') ? 'has-error' : ''}}">
                                                    {{ Form::select('fichier',$fichiers,null,['class'=>"form-control form-control-sm mt-2","placeholder"=>__("Nom du fichier à exporter")]) }}
                                
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="list-group-item pd-y-20">
                                    <div class="media">
                                        <div class="d-flex mg-r-10 wd-50">
                                        <i class="icon ion-forward tx-muted tx-40 tx"></i>
                                        </div><!-- d-flex -->
                                        <div class="media-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    
                                                <h6 class="tx-inverse mt-3">
                                                    <b>{{__("Format d'exportation")}}</b></h6>
                                                
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="mg-b-0 {{ $errors->has('format') ? 'has-error' : ''}}">
                                                    {{ Form::select('format',$formats,null,['class'=>"form-control form-control-sm  mt-2","placeholder"=>__("Format du fichier")]) }}
                                
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    </div><!-- list-group-item -->
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-30">
                        <div class="col-sm-8 text-center">
                            <div class="form-layout-footer">
                            <button class="btn btn-primary bd-0" type="submit"><i class="fa fa-save"></i>   {{__('Sauvegarde')}}</button>
                            <a href="{{route('welcome')}}" class="btn btn-secondary bd-0"> <i class="fa fa-chevron-left"></i> {{__('Retour')}}</a>
                            </div><!-- form-layout-footer -->
                        </div><!-- col-8 -->
                        </div>
                    </div>
</form>
            </section>
          </div>
     </div>
    </div>
</div>
@endsection