<div class="form-group {{ $errors->has('nom_structure') ? 'has-error' : ''}}">
    <label for="nom_structure" class="control-label">{{ 'Nom Structure' }}</label>
    <input class="form-control" name="nom_structure" type="file" id="nom_structure" value="{{ isset($tp->nom_structure) ? $tp->nom_structure : ''}}" required>
    {!! $errors->first('nom_structure', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pays') ? 'has-error' : ''}}">
    <label for="pays" class="control-label">{{ 'Pays' }}</label>
    <select name="pays" class="form-control" id="pays" required>
    @foreach (json_decode('{}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($tp->pays) && $tp->pays == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('pays', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
