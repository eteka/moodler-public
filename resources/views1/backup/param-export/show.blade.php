@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">ParamExport {{ $paramexport->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/param-export') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/param-export/' . $paramexport->id . '/edit') }}" title="Edit ParamExport"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('paramexport' . '/' . $paramexport->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete ParamExport" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $paramexport->id }}</td>
                                    </tr>
                                    <tr><th> Niv Universite </th><td> {{ $paramexport->niv_universite }} </td></tr><tr><th> Niv Entite </th><td> {{ $paramexport->niv_entite }} </td></tr><tr><th> Niv Cylce </th><td> {{ $paramexport->niv_cylce }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
