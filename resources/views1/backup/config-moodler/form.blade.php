<div class="form-group {{ $errors->has('nom_structure') ? 'has-error' : ''}}">
    <label for="nom_structure" class="control-label">{{ 'Nom Structure' }}</label>
    <input class="form-control" name="nom_structure" type="text" id="nom_structure" value="{{ isset($configmoodler->nom_structure) ? $configmoodler->nom_structure : ''}}" >
    {!! $errors->first('nom_structure', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('logo_tsructure') ? 'has-error' : ''}}">
    <label for="logo_tsructure" class="control-label">{{ 'Logo Tsructure' }}</label>
    <input class="form-control" name="logo_tsructure" type="text" id="logo_tsructure" value="{{ isset($configmoodler->logo_tsructure) ? $configmoodler->logo_tsructure : ''}}" >
    {!! $errors->first('logo_tsructure', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tel_tsructure') ? 'has-error' : ''}}">
    <label for="tel_tsructure" class="control-label">{{ 'Tel Tsructure' }}</label>
    <input class="form-control" name="tel_tsructure" type="text" id="tel_tsructure" value="{{ isset($configmoodler->tel_tsructure) ? $configmoodler->tel_tsructure : ''}}" >
    {!! $errors->first('tel_tsructure', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email_tructure') ? 'has-error' : ''}}">
    <label for="email_tructure" class="control-label">{{ 'Email Tructure' }}</label>
    <input class="form-control" name="email_tructure" type="text" id="email_tructure" value="{{ isset($configmoodler->email_tructure) ? $configmoodler->email_tructure : ''}}" >
    {!! $errors->first('email_tructure', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description_structure') ? 'has-error' : ''}}">
    <label for="description_structure" class="control-label">{{ 'Description Structure' }}</label>
    <textarea class="form-control" rows="5" name="description_structure" type="textarea" id="description_structure" >{{ isset($configmoodler->description_structure) ? $configmoodler->description_structure : ''}}</textarea>
    {!! $errors->first('description_structure', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('localisation_structure') ? 'has-error' : ''}}">
    <label for="localisation_structure" class="control-label">{{ 'Localisation Structure' }}</label>
    <input class="form-control" name="localisation_structure" type="text" id="localisation_structure" value="{{ isset($configmoodler->localisation_structure) ? $configmoodler->localisation_structure : ''}}" >
    {!! $errors->first('localisation_structure', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('server_bdd') ? 'has-error' : ''}}">
    <label for="server_bdd" class="control-label">{{ 'Server Bdd' }}</label>
    <input class="form-control" name="server_bdd" type="text" id="server_bdd" value="{{ isset($configmoodler->server_bdd) ? $configmoodler->server_bdd : ''}}" >
    {!! $errors->first('server_bdd', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nom_bdd') ? 'has-error' : ''}}">
    <label for="nom_bdd" class="control-label">{{ 'Nom Bdd' }}</label>
    <input class="form-control" name="nom_bdd" type="text" id="nom_bdd" value="{{ isset($configmoodler->nom_bdd) ? $configmoodler->nom_bdd : ''}}" >
    {!! $errors->first('nom_bdd', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('user_bdd') ? 'has-error' : ''}}">
    <label for="user_bdd" class="control-label">{{ 'User Bdd' }}</label>
    <input class="form-control" name="user_bdd" type="text" id="user_bdd" value="{{ isset($configmoodler->user_bdd) ? $configmoodler->user_bdd : ''}}" >
    {!! $errors->first('user_bdd', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pwd_bdd') ? 'has-error' : ''}}">
    <label for="pwd_bdd" class="control-label">{{ 'Pwd Bdd' }}</label>
    <input class="form-control" name="pwd_bdd" type="text" id="pwd_bdd" value="{{ isset($configmoodler->pwd_bdd) ? $configmoodler->pwd_bdd : ''}}" >
    {!! $errors->first('pwd_bdd', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('port_bdd') ? 'has-error' : ''}}">
    <label for="port_bdd" class="control-label">{{ 'Port Bdd' }}</label>
    <input class="form-control" name="port_bdd" type="number" id="port_bdd" value="{{ isset($configmoodler->port_bdd) ? $configmoodler->port_bdd : ''}}" >
    {!! $errors->first('port_bdd', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lang_default') ? 'has-error' : ''}}">
    <label for="lang_default" class="control-label">{{ 'Lang Default' }}</label>
    <input class="form-control" name="lang_default" type="text" id="lang_default" value="{{ isset($configmoodler->lang_default) ? $configmoodler->lang_default : ''}}" >
    {!! $errors->first('lang_default', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fichier_default') ? 'has-error' : ''}}">
    <label for="fichier_default" class="control-label">{{ 'Fichier Default' }}</label>
    <input class="form-control" name="fichier_default" type="text" id="fichier_default" value="{{ isset($configmoodler->fichier_default) ? $configmoodler->fichier_default : ''}}" >
    {!! $errors->first('fichier_default', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('format_default') ? 'has-error' : ''}}">
    <label for="format_default" class="control-label">{{ 'Format Default' }}</label>
    <input class="form-control" name="format_default" type="text" id="format_default" value="{{ isset($configmoodler->format_default) ? $configmoodler->format_default : ''}}" >
    {!! $errors->first('format_default', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('type_secure') ? 'has-error' : ''}}">
    <label for="type_secure" class="control-label">{{ 'Type Secure' }}</label>
    <input class="form-control" name="type_secure" type="text" id="type_secure" value="{{ isset($configmoodler->type_secure) ? $configmoodler->type_secure : ''}}" >
    {!! $errors->first('type_secure', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pays') ? 'has-error' : ''}}">
    <label for="pays" class="control-label">{{ 'Pays' }}</label>
    <input class="form-control" name="pays" type="text" id="pays" value="{{ isset($configmoodler->pays) ? $configmoodler->pays : ''}}" >
    {!! $errors->first('pays', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('timezone') ? 'has-error' : ''}}">
    <label for="timezone" class="control-label">{{ 'Timezone' }}</label>
    <input class="form-control" name="timezone" type="datetime-local" id="timezone" value="{{ isset($configmoodler->timezone) ? $configmoodler->timezone : ''}}" >
    {!! $errors->first('timezone', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
