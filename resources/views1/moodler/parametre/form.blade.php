<div class="form-group {{ $errors->has('niv_universite') ? 'has-error' : ''}}">
    <label for="niv_universite" class="control-label">{{ 'Niv Universite' }}</label>
    <input class="form-control" name="niv_universite" type="text" id="niv_universite" value="{{ isset($parametre->niv_universite) ? $parametre->niv_universite : ''}}" required>
    {!! $errors->first('niv_universite', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('niv_entite') ? 'has-error' : ''}}">
    <label for="niv_entite" class="control-label">{{ 'Niv Entite' }}</label>
    <input class="form-control" name="niv_entite" type="text" id="niv_entite" value="{{ isset($parametre->niv_entite) ? $parametre->niv_entite : ''}}" >
    {!! $errors->first('niv_entite', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('niv_cylce') ? 'has-error' : ''}}">
    <label for="niv_cylce" class="control-label">{{ 'Niv Cylce' }}</label>
    <input class="form-control" name="niv_cylce" type="text" id="niv_cylce" value="{{ isset($parametre->niv_cylce) ? $parametre->niv_cylce : ''}}" >
    {!! $errors->first('niv_cylce', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('niv_filiere') ? 'has-error' : ''}}">
    <label for="niv_filiere" class="control-label">{{ 'Niv Filiere' }}</label>
    <input class="form-control" name="niv_filiere" type="text" id="niv_filiere" value="{{ isset($parametre->niv_filiere) ? $parametre->niv_filiere : ''}}" >
    {!! $errors->first('niv_filiere', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('niv_annee') ? 'has-error' : ''}}">
    <label for="niv_annee" class="control-label">{{ 'Niv Annee' }}</label>
    <input class="form-control" name="niv_annee" type="text" id="niv_annee" value="{{ isset($parametre->niv_annee) ? $parametre->niv_annee : ''}}" required>
    {!! $errors->first('niv_annee', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('niv_semestre') ? 'has-error' : ''}}">
    <label for="niv_semestre" class="control-label">{{ 'Niv Semestre' }}</label>
    <input class="form-control" name="niv_semestre" type="text" id="niv_semestre" value="{{ isset($parametre->niv_semestre) ? $parametre->niv_semestre : ''}}" >
    {!! $errors->first('niv_semestre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('niv_cours') ? 'has-error' : ''}}">
    <label for="niv_cours" class="control-label">{{ 'Niv Cours' }}</label>
    <input class="form-control" name="niv_cours" type="text" id="niv_cours" value="{{ isset($parametre->niv_cours) ? $parametre->niv_cours : ''}}" >
    {!! $errors->first('niv_cours', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
