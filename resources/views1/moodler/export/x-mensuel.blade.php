@php
$filieres=$data["filiere"];
$semestres=$data["semestre"];
$matieres=$data["matiere"];
@endphp
<style>
    body {
        font-family: "arial", sans-serif;
        color: #000000;
        font-size: 12px;
            // background:#eeeeee;
        }
        div.page_break + div.page_break{
    page-break-before: always;
}
        .page { width: 100%; height: 100%; }
.page_break { page-break-before: always; }
    .page-break {
        page-break-after: always;
    }

    .tb {
        //border: 1px solid #000;
        border-collapse: collapse;
        width: 100%;
    }

    .tb td,
    .tb>th {
        padding: 5px;
        
    }
    .tb tr th{
        text-align: center
    }

    .tb tr th {
        font-weight: bold;
        background: #eeeeee;
    }

    .tb tr th,
    .tb tr td {
        border: 1px solid #666666;
    }
    .tb tr.no-border td,.tb .no-border,.tb .no-border{
        border: 0px solid #fff !important;
    }
    .text-center,td .text-center{text-align: center}
</style>
<div id="tb_moodler">
<table class="tb" id="tb_moodler_main"   data-cols-width="12,15,70,15,25,25">
    <tr class="no-border hidden">
        <td colspan="6" class="no-border " data-f-name="Times New Roman"  data-f-bold="true"   class="text-center"> <h1>{{ __('Rapport Mensuel') }}</h1><hr class="m-0"></td>
    </tr>
    <tr class="no-border">
        <td colspan="6" class="no-border"  data-b-a-c="FF000000"  data-f-name="Times New Roman" align="center" data-f-bold="true" data-a-h="center"  class="text-center"> <h3>{{ isset($universite)?$universite:"" }}</h3></td>
    </tr>
    <tr class="no-border">
        <td  data-f-name="Times New Roman" align="right" data-f-bold="true" colspan="3">
            {!! isset($lentite)?"<b>ENTITÉ/ETABLISSEMENT : </b>".$lentite:"" !!}
        </td>
        <td data-f-name="Times New Roman" data-f-bold="true" colspan="3">
            {!! isset($lecycle)?"<b>Cycle :</b> ".$lecycle:"" !!}
        </td>
    </tr>
    <tr class="no-border">
        <td colspan="6" data-f-name="Times New Roman" data-f-bold="true" data-a-h="center"  class="text-center">{!! isset($annee_academique)?"<b>Année Académique :</b> ".$annee_academique:"" !!}</td>
    </tr>
    <tr class="no-border">
        <td colspan="6" data-f-name="Times New Roman" data-f-bold="true" data-a-h="center"  class="text-center"></td>
    </tr>
    
    <tr>
        <th data-fill-color="FFEEEEEE" data-f-name="Times New Roman" data-f-bold="true" data-a-h="center"  data-b-a-s="thin" data-b-a-c="FF000000"  >FORMATIONS</th>
        <th data-fill-color="FFEEEEEE" data-f-name="Times New Roman" data-f-bold="true" data-a-h="center"  data-b-a-s="thin" data-b-a-c="FF000000"  > SEMESTRE</th>
        <th data-fill-color="FFEEEEEE" data-f-name="Times New Roman" data-f-bold="true" data-a-h="center"  data-b-a-s="thin" data-b-a-c="FF000000"  > MATIERES</th>
        <th data-fill-color="FFEEEEEE" data-f-name="Times New Roman" data-f-bold="true" data-a-h="center"  data-b-a-s="thin" data-b-a-c="FF000000"  >SEQUENCES</th>
        <th data-fill-color="FFEEEEEE" data-f-name="Times New Roman" data-f-bold="true" data-a-h="center"  data-b-a-s="thin" data-b-a-c="FF000000"  >SEQUENCES EXCECUTEES </th>
        <th data-fill-color="FFEEEEEE" data-f-name="Times New Roman" data-f-bold="true" data-a-h="center"  data-b-a-s="thin" data-b-a-c="FF000000"  >TAUX D'EXECUTION </th>
    </tr>

    @php
        
    @endphp
    @foreach ($filieres as $fil)

        @php
        $mat_all = 0;
        $fil_id=$fil['id'];
        if(isset($semestres[$fil_id])){        
            $d=$semestres[$fil_id];
            $nfil=count($d);
            
            foreach($semestres[$fil_id] as $s){
                $kmat=$s['id'];
                $mat=$matieres[$kmat];
                $nmat= count(array_filter($mat, function($x) { return !empty($x); }));
                $nmat= $nmat>0?$nmat:1;
                $mat_all+= $nmat;
            }

        }
        // $mat_all;
        //dd($mat_all);
        $compteur_sem=0;
        @endphp
        <!---Filiere-->
        @if(1)
       
            <tr>
                <td data-a-text-rotation="90"  data-f-name="Times New Roman" data-f-bold="true"  data-a-v="middle"  data-a-h="center"  data-b-a-s="thin" data-b-a-c="FF000000"   @if ($mat_all> 1) rowspan="{{ $mat_all }}" @endif > {{ $fil['nom'] }}
                </td>
                @if (!isset($semestres[$fil_id]))
                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  ></td>
                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  ></td>
                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  ></td>
                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  ></td>
                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  ></td>
                </tr>
                @else
                @foreach ($semestres[$fil_id] as $s)

                    @php

                    $kmat=$s['id'];
                    $mat=isset($matieres[$kmat])?$matieres[$kmat]:null;

                    //$nmat=(count($mat)>0)?count($mat):1;
                    $nmat=count(array_filter($mat, function($x) { return !empty($x); }));
                    //dd($nmat);
                    $compteur=0;


                    @endphp
                    @if ($compteur_sem++ == 0)
                    <!-------------------------------IFFFFF--------------->
                    <td data-a-text-rotation="90" data-f-bold="true" data-f-name="Times New Roman"  data-a-v="middle"  data-a-h="center" data-b-a-s="thin" data-b-a-c="FF000000"   @if ($nmat> 1) rowspan="{{ $nmat }}" @endif > {{ $s['nom'] }}</td>

                   
                    @if ($nmat)
                    @foreach ($mat as $m)

                        @if ($compteur++ == 0)

                            <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > {{ $m->fullname }}</td>
                            <td data-num-fmt="0" data-b-a-s="thin" data-b-a-c="FF000000"  > {{ $m->newsitems }}</td>
                            <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > -</td>
                            <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > -</td>
                            </tr>
                        @else

                            <tr>
                                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > {{ $m->fullname }}</td>
                                <td  data-f-name="Times New Roman" data-num-fmt="0" data-b-a-s="thin" data-b-a-c="FF000000"  > {{ $m->newsitems }}</td>
                                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > -</td>
                                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > -</td>
                            </tr>
                        @endif

                    @endforeach
                @else
                <!--td @if ($nmat> 1) rowspan="{{ $nmat }}" @endif > {{ $s['nom'] }}
                </td-->
                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  >0</td>
                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  >0</td>
                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  >0</td>
                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  >0</td>
                </tr>

                @endif
                    <!-------------------------------IFFFFF--------------->
                    @else
                    <tr>
                        <td data-a-text-rotation="90" data-f-bold="true" data-a-v="middle" data-a-h="center"  data-a-text-rotation="90" data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"   @if ($nmat> 1) rowspan="{{ $nmat }}" @endif > {{ $s['nom'] }}</td>

                        @if ($nmat == 0)
                            <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  ></td>
                            <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  ></td>
                            <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  ></td>
                            <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  ></td>
                        </tr>
                        @endif
                        @if ($nmat)
                        @foreach ($mat as $m)

                            @if ($compteur++ == 0)

                                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > {{ $m->fullname }}</td>
                                <td  data-f-name="Times New Roman" data-num-fmt="0" data-b-a-s="thin" data-b-a-c="FF000000"  > {{ $m->newsitems }}</td>
                                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > -</td>
                                <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > -</td>
                                </tr>
                            @else

                                <tr>
                                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > {{ $m->fullname }}</td>
                                    <td  data-f-name="Times New Roman" data-num-fmt="0" data-b-a-s="thin" data-b-a-c="FF000000"  > {{ $m->newsitems }}</td>
                                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > -</td>
                                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  > -</td>
                                </tr>
                            @endif

                        @endforeach
                    @else

                    {{-- <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"   @if ($nmat> 1) rowspan="{{ $nmat }}" @endif > {{ $s['nom'] }} --}}
                    {{-- </td>
                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  >0</td>
                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  >0</td>
                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  >0</td>
                    <td  data-f-name="Times New Roman" data-b-a-s="thin" data-b-a-c="FF000000"  >0</td>
                    </tr> --}}

                    @endif

            @endif



                @endforeach
        
            @endif
        @endif


    @endforeach
</table>
</div>