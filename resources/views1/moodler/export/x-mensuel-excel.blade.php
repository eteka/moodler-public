@php
$filieres=$data["filiere"];
$semestres=$data["semestre"];
$matieres=$data["matiere"];
@endphp
<style>
    body {
        font-family: arial, sans-serif;
        color: #000000;
        // background:#eeeeee;
    }

    .page-break {
        page-break-after: always;
    }

    .tb {
        border: 1px solid #000;
        border-collapse: collapse;
        width: 100%;
    }

    .tb td,
    .tb>th {
        padding: 5px;
    }

    .tb tr th {
        font-weight: bold;
        background: #eeeeee;
    }

    .tb tr th,
    .tb tr td {
        border: 1px solid #666666;
    }

</style>

        <table class="tb" id="tb_moodler_main">
            
            <tr>
                <td colspan="6">
                    <h3>{{ isset($universite) ? $universite : '' }}</h3>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    {!! isset($lentite) ? '<b>ENTITÉ/ETABLISSEMENT : </b>' . $lentite : '' !!}
                </td>
                <td colspan="3">
                    {!! isset($lecycle) ? '<b>Cycle :</b> ' . $lecycle : '' !!}
                </td>
            </tr>
            <tr>
                <td colspan="6">{!! isset($annee_academique) ? '<b>Année Académique :</b> ' . $annee_academique : '' !!}
                </td>
            </tr>
            <tr>
                <td colspan="6"></td>
            </tr>
            <tr>
                <th>FORMATIONS</th>
                <th> SEMESTRE</th>
                <th> MATIERES</th>
                <th>SEQUENCES</th>
                <th>SEQUENCES EXCECUTEES </th>
                <th>TAUX D'EXECUTION </th>
            </tr>

            @php

            @endphp
            @foreach ($filieres as $fil)

                @php
                $mat_all = 0;
                $fil_id=$fil['id'];
                if(isset($semestres[$fil_id])){
                $d=$semestres[$fil_id];
                $nfil=count($d);

                foreach($semestres[$fil_id] as $s){
                $kmat=$s['id'];
                $mat=$matieres[$kmat];
                $nmat= count(array_filter($mat, function($x) { return !empty($x); }));
                $nmat= $nmat>0?$nmat:1;
                $mat_all+= $nmat;
                }

                }
                // $mat_all;
                //dd($mat_all);
                $compteur_sem=0;
                @endphp
                <!---Filiere-->
                @if (1)

                    <tr>
                        <td @if ($mat_all > 1) rowspan="{{ $mat_all }}"
                @endif > {{ $fil['nom'] }}
                </td>
                @if (!isset($semestres[$fil_id]))
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>
                @else
                    @foreach ($semestres[$fil_id] as $s)

                        @php

                        $kmat=$s['id'];
                        $mat=isset($matieres[$kmat])?$matieres[$kmat]:null;

                        //$nmat=(count($mat)>0)?count($mat):1;
                        $nmat=count(array_filter($mat, function($x) { return !empty($x); }));
                        //dd($nmat);
                        $compteur=0;


                        @endphp
                        @if ($compteur_sem++ == 0)
                            <!-------------------------------IFFFFF--------------->
                            <td @if ($nmat > 1) rowspan="{{ $nmat }}"
                        @endif > {{ $s['nom'] }}</td>


                        @if ($nmat)
                            @foreach ($mat as $m)

                                @if ($compteur++ == 0)

                                    <td> {{ $m->fullname }}</td>
                                    <td data-num-fmt="0"> {{ $m->newsitems }}</td>
                                    <td> -</td>
                                    <td> -</td>
                                    </tr>
                                @else

                                    <tr>
                                        <td> {{ $m->fullname }}</td>
                                        <td data-num-fmt="0"> {{ $m->newsitems }}</td>
                                        <td> -</td>
                                        <td> -</td>
                                    </tr>
                                @endif

                            @endforeach
                        @else
                            <!--td @if ($nmat > 1) rowspan="{{ $nmat }}" @endif > {{ $s['nom'] }}
                </td-->
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            </tr>

                        @endif
                        <!-------------------------------IFFFFF--------------->
                    @else
                        <tr>
                            <td @if ($nmat > 1) rowspan="{{ $nmat }}"
                    @endif > {{ $s['nom'] }}</td>

                    @if ($nmat == 0)
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                    @endif
                    @if ($nmat)
                        @foreach ($mat as $m)

                            @if ($compteur++ == 0)

                                <td> {{ $m->fullname }}</td>
                                <td data-num-fmt="0"> {{ $m->newsitems }}</td>
                                <td> -</td>
                                <td> -</td>
                                </tr>
                            @else

                                <tr>
                                    <td> {{ $m->fullname }}</td>
                                    <td data-num-fmt="0"> {{ $m->newsitems }}</td>
                                    <td> -</td>
                                    <td> -</td>
                                </tr>
                            @endif

                        @endforeach
                    @else

                        {{-- <td @if ($nmat > 1)
                            rowspan="{{ $nmat }}"
                    @endif > {{ $s['nom'] }} --}}
                    {{-- </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    </tr> --}}

                @endif

            @endif



            @endforeach

            @endif
            @endif


            @endforeach
        </table>
   
