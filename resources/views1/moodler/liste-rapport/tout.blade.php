@extends('layouts.app')
@section('title', 'Mes rapports')

@section('content')
    <div class="bg-soft">
        @include('moodler.include.top')
        <section class="bg-soft">
            <main class="">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                    <div class="d-block mb-4 mb-md-0">                        
                        <h2 class="h3">{{ __('Mes rapports') }}</h2>
                    </div>
                </div>
                <div class="table-settings mb-4">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-12 col-md-6 col-lg-3 align-items-center d-none d-md-flex">
                            <div class="form-group mb-0 mr-3"><select class="custom-select"
                                    id="inlineFormCustomSelectMesages">
                                    <option selected="selected">Tout</option>
                                    <option value="1">{{ __("Rapport hebdomadaire") }}</option>
                                    <option value="2">{{ __("Rapport mensuel") }}</option>
                                    <option value="3">{{ __("Rapport par semestre") }}</option>
                                </select></div>
                            <div><a href="#" class="btn btn-md btn-white border-light">{{ __("Valider") }}</a></div>
                        </div>
                        <div class="col col-md-4 col-lg-3 col-xl-2 ml-auto">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><span
                                            class="fas fa-search"></span></span></div><input class="form-control"
                                    id="searchInputdashboard1" placeholder="Search" type="text"
                                    aria-label="Dashboard user search">
                            </div>
                        </div>
                        <div class="col-2 pl-0 d-none d-lg-flex">
                            <div class="form-group mb-0">
                                <select class="custom-select" id="inlineFormCustomSelectMesages2">
                                    <option selected="selected">Format</option>
                                    <option value="1">PDF</option>
                                    <option value="2">Excel</option>
                                    <option value="3">Word</option>
                                    <option value="3">Dot</option>
                                </select>
                                
                            </div>
                            <div class="form-group mb-0 ml-2 mr-2">
                                <select class="custom-select ml-2" id="">
                                    <option selected="selected">Afficher</option>
                                    <option value="1">10</option>
                                    <option value="2">20</option>
                                    <option value="3">30</option>
                                    <option value="3">40</option>
                                    <option value="3">50</option>
                                </select>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-body border-light shadow-sm table-wrapper table-responsive pt-0">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="border-0">
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck55"> <label class="form-check-label"
                                            for="userCheck55"></label></div>
                                </th>
                                <th class="border-0">Name</th>
                                <th class="border-0">Date Created</th>
                                <th class="border-0">Verified</th>
                                <th class="border-0">Status</th>
                                <th class="border-0">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck1"> <label class="form-check-label"
                                            for="userCheck1"></label>
                                        </div>
                                </td>
                                <td><a href="#" class="d-flex align-items-center"><img
                                            src="../../assets/img/team/profile-picture-1.jpg"
                                            class="user-avatar rounded-circle mr-3" alt="Avatar">
                                        <div class="d-block"><span class="font-weight-bold">Roy Fendley</span>
                                            <div class="small text-gray">info@example.com</div>
                                        </div>
                                    </a></td>
                                <td><span class="font-weight-normal">10 Feb 2020</span></td>
                                <td><span class="font-weight-normal"><span
                                            class="fas fa-check-circle text-success mr-2"></span>Email</span></td>
                                <td><span class="font-weight-normal text-success">Active</span></td>
                                <td>
                                    <div class="btn-group"><button
                                            class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span>
                                            </span><span class="sr-only">Toggle Dropdown</span></button>
                                        <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                    class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View
                                                Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                    class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                    </div><a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                        data-original-title="Delete"><span class="fas fa-times-circle"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck2"> <label class="form-check-label"
                                            for="userCheck2"></label></div>
                                </td>
                                <td><a href="#" class="d-flex align-items-center">
                                        <div class="user-avatar bg-secondary mr-3"><span>SA</span></div>
                                        <div class="d-block"><span class="font-weight-bold">Scott Anderson</span>
                                            <div class="small text-gray">info@example.com</div>
                                        </div>
                                    </a></td>
                                <td><span class="font-weight-normal">10 Feb 2020</span></td>
                                <td><span class="font-weight-normal"><span
                                            class="fas fa-check-circle text-success mr-2"></span>Email</span></td>
                                <td><span class="font-weight-normal text-success">Active</span></td>
                                <td>
                                    <div class="btn-group"><button
                                            class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span>
                                            </span><span class="sr-only">Toggle Dropdown</span></button>
                                        <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                    class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View
                                                Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                    class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                    </div><a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                        data-original-title="Delete"><span class="fas fa-times-circle"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck3"> <label class="form-check-label"
                                            for="userCheck3"></label></div>
                                </td>
                                <td><a href="#" class="d-flex align-items-center"><img
                                            src="../../assets/img/team/profile-picture-2.jpg"
                                            class="user-avatar rounded-circle mr-3" alt="Avatar">
                                        <div class="d-block"><span class="font-weight-bold">George Driskell</span>
                                            <div class="small text-gray">info@example.com</div>
                                        </div>
                                    </a></td>
                                <td><span class="font-weight-normal">10 Feb 2020</span></td>
                                <td><span class="font-weight-normal"><span
                                            class="fas fa-check-circle text-success mr-2"></span>Email</span></td>
                                <td><span class="font-weight-normal text-success">Active</span></td>
                                <td>
                                    <div class="btn-group"><button
                                            class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span>
                                            </span><span class="sr-only">Toggle Dropdown</span></button>
                                        <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                    class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View
                                                Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                    class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                    </div><a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                        data-original-title="Delete"><span class="fas fa-times-circle"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck4"> <label class="form-check-label"
                                            for="userCheck4"></label></div>
                                </td>
                                <td><a href="#" class="d-flex align-items-center"><img
                                            src="../../assets/img/team/profile-picture-3.jpg"
                                            class="user-avatar rounded-circle mr-3" alt="Avatar">
                                        <div class="d-block"><span class="font-weight-bold">Bonnie Green</span>
                                            <div class="small text-gray">info@example.com</div>
                                        </div>
                                    </a></td>
                                <td><span class="font-weight-normal">10 Feb 2020</span></td>
                                <td><span class="font-weight-normal"><span
                                            class="fas fa-check-circle text-success mr-2"></span>Email</span></td>
                                <td><span class="font-weight-normal text-success">Active</span></td>
                                <td>
                                    <div class="btn-group"><button
                                            class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span>
                                            </span><span class="sr-only">Toggle Dropdown</span></button>
                                        <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                    class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View
                                                Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                    class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                    </div><a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                        data-original-title="Delete"><span class="fas fa-times-circle"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck5"> <label class="form-check-label"
                                            for="userCheck5"></label></div>
                                </td>
                                <td><a href="#" class="d-flex align-items-center">
                                        <div class="user-avatar bg-tertiary mr-3"><span>RB</span></div>
                                        <div class="d-block"><span class="font-weight-bold">Ronnie Buchanan</span>
                                            <div class="small text-gray">info@example.com</div>
                                        </div>
                                    </a></td>
                                <td><span class="font-weight-normal">10 Feb 2020</span></td>
                                <td><span class="font-weight-normal"><span
                                            class="fas fa-clock text-info mr-2"></span>Email</span></td>
                                <td><span class="font-weight-normal text-info">Inactive</span></td>
                                <td>
                                    <div class="btn-group"><button
                                            class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span>
                                            </span><span class="sr-only">Toggle Dropdown</span></button>
                                        <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                    class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View
                                                Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                    class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                    </div><a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                        data-original-title="Delete"><span class="fas fa-times-circle"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck6"> <label class="form-check-label"
                                            for="userCheck6"></label></div>
                                </td>
                                <td><a href="#" class="d-flex align-items-center">
                                        <div class="user-avatar bg-secondary mr-3"><span>JR</span></div>
                                        <div class="d-block"><span class="font-weight-bold">Jane Rinehart</span>
                                            <div class="small text-gray">info@example.com</div>
                                        </div>
                                    </a></td>
                                <td><span class="font-weight-normal">10 Feb 2020</span></td>
                                <td><span class="font-weight-normal"><span
                                            class="fas fa-info-circle mr-2"></span>Email</span></td>
                                <td><span class="font-weight-normal text-warning">Pending</span></td>
                                <td>
                                    <div class="btn-group"><button
                                            class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span>
                                            </span><span class="sr-only">Toggle Dropdown</span></button>
                                        <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                    class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View
                                                Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                    class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                    </div><a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                        data-original-title="Delete"><span class="fas fa-times-circle"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck7"> <label class="form-check-label"
                                            for="userCheck7"></label></div>
                                </td>
                                <td><a href="#" class="d-flex align-items-center"><img
                                            src="../../assets/img/team/profile-picture-4.jpg"
                                            class="user-avatar rounded-circle mr-3" alt="Avatar">
                                        <div class="d-block"><span class="font-weight-bold">William Ginther</span>
                                            <div class="small text-gray">info@example.com</div>
                                        </div>
                                    </a></td>
                                <td><span class="font-weight-normal">10 Feb 2020</span></td>
                                <td><span class="font-weight-normal"><span
                                            class="fas fa-info-circle mr-2"></span>Email</span></td>
                                <td><span class="font-weight-normal text-warning">Pending</span></td>
                                <td>
                                    <div class="btn-group"><button
                                            class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span>
                                            </span><span class="sr-only">Toggle Dropdown</span></button>
                                        <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                    class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View
                                                Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                    class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                    </div><a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                        data-original-title="Delete"><span class="fas fa-times-circle"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck8"> <label class="form-check-label"
                                            for="userCheck8"></label></div>
                                </td>
                                <td><a href="#" class="d-flex align-items-center"><img
                                            src="../../assets/img/team/profile-picture-5.jpg"
                                            class="user-avatar rounded-circle mr-3" alt="Avatar">
                                        <div class="d-block"><span class="font-weight-bold">Margaret Dow</span>
                                            <div class="small text-gray">info@example.com</div>
                                        </div>
                                    </a></td>
                                <td><span class="font-weight-normal">10 Feb 2020</span></td>
                                <td><span class="font-weight-normal"><span
                                            class="fas fa-info-circle mr-2"></span>Email</span></td>
                                <td><span class="font-weight-normal text-danger">Suspended</span></td>
                                <td>
                                    <div class="btn-group"><button
                                            class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span>
                                            </span><span class="sr-only">Toggle Dropdown</span></button>
                                        <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                    class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View
                                                Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                    class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                    </div><a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                        data-original-title="Delete"><span class="fas fa-times-circle"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-check dashboard-check"><input class="form-check-input" type="checkbox"
                                            value="" id="userCheck9"> <label class="form-check-label"
                                            for="userCheck9"></label></div>
                                </td>
                                <td><a href="#" class="d-flex align-items-center">
                                        <div class="user-avatar bg-info mr-3"><span>MT</span></div>
                                        <div class="d-block"><span class="font-weight-bold">Michael Hopkins</span>
                                            <div class="small text-gray">info@example.com</div>
                                        </div>
                                    </a></td>
                                <td><span class="font-weight-normal">10 Feb 2020</span></td>
                                <td><span class="font-weight-normal"><span
                                            class="fas fa-info-circle mr-2"></span>Email</span></td>
                                <td><span class="font-weight-normal text-danger">Suspended</span></td>
                                <td>
                                    <div class="btn-group"><button
                                            class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                                class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span>
                                            </span><span class="sr-only">Toggle Dropdown</span></button>
                                        <div class="dropdown-menu"><a class="dropdown-item" href="#"><span
                                                    class="fas fa-user-shield mr-2"></span> Reset Pass</a> <a
                                                class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View
                                                Details</a> <a class="dropdown-item text-danger" href="#"><span
                                                    class="fas fa-user-times mr-2"></span>Suspend</a></div>
                                    </div><a href="#" class="text-danger ml-2" title="" data-toggle="tooltip"
                                        data-original-title="Delete"><span class="fas fa-times-circle"></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="card-footer px-3 d-flex align-items-center justify-content-between">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination mb-0">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>
                        <div class="font-weight-bold small">Showing <b>5</b> out of <b>25</b> entries</div>
                    </div>
                </div>
                <footer class="footer section py-5">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-lg-6 mb-4 mb-lg-0">
                                <p class="mb-0 text-center text-xl-left">Copyright © 2019-<span
                                        class="current-year">2020</span> <a class="text-primary font-weight-normal"
                                        href="https://themesberg.com" target="_blank">Themesberg</a></p>
                            </div>
                            <div class="col-12 col-lg-6">
                                <ul
                                    class="list-inline list-group-flush list-group-borderless text-center text-xl-right mb-0">
                                    <li class="list-inline-item px-0 px-sm-2"><a
                                            href="https://themesberg.com/about">About</a></li>
                                    <li class="list-inline-item px-0 px-sm-2"><a
                                            href="https://themesberg.com/themes">Themes</a></li>
                                    <li class="list-inline-item px-0 px-sm-2"><a href="https://themesberg.com/blog">Blog</a>
                                    </li>
                                    <li class="list-inline-item px-0 px-sm-2"><a
                                            href="https://themesberg.com/contact">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </main>
        </section>
    </div>
@endsection
