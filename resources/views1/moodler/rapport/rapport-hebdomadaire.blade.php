@extends('layouts.app')
@section('title', __('Rapport Hebdomadaire'))

@section('content')
    <div class="bg-soft">
        @include('moodler.include.top')
        <section class="">
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                        <div class="d-block mb-4 mb-md-0">
                            <h2 class="h3  page-header">{{ __('Rapport Hebdomadaire') }}</h2>
                        </div>
                    </div>
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="GET" action="{{ route('rapport-hebdo.create') }}" accept-charset="UTF-8"
                        class="form-horizontal" enctype="multipart/form-data">

                        <div class="card border-light shadow-sm">
                            <div class="card-body__">
                                <div class="table-responsive">
                                    <table class="table table-centered table-nowrap mb-0 rounded">
                                        <thead class="thead-light__">
                                            <tr>
                                                <th class="border-0">
                                                    <select class="custom-select" id="gender">
                                                        <option disabled="disabled" name="univ" selected="selected">
                                                            Université</option>
                                                        <option value="1">Unviversité de Parakou</option>
                                                        <option value="2">Unviversité d'abomey-calavi</option>
                                                    </select>
                                                </th>
                                                <th class="border-0">
                                                    <select name="entity" class="custom-select" id="gender">
                                                        <option disabled="disabled" selected="selected">Entité
                                                        </option>
                                                        <option value="1">Faculté de droit et de sciences Physiques</option>
                                                        <option value="2">IUT Parakou</option>
                                                    </select>
                                                </th>
                                                <th class="border-0">
                                                    <select name="cycle" class="custom-select" id="gender">
                                                        <option disabled="disabled" selected="selected"> Cycle
                                                        </option>
                                                        <option value="1">Licence</option>
                                                        <option value="2">Master</option>
                                                        <option value="2">Doctorat</option>
                                                    </select>
                                                </th>
                                                <th class="border-0">
                                                    <select name="annee" class="custom-select" id="gender">
                                                        <option disabled="disabled" selected="selected"> Année Académque
                                                        </option>
                                                        <option value="1">2017-2018</option>
                                                        <option value="2">2018-2019</option>
                                                        <option value="2">2019-2020</option>
                                                    </select>
                                                </th>
                                                <th class="border-0">
                                                    <select name="filiere" class="custom-select" id="gender">
                                                        <option disabled="disabled" selected="selected">Toutes les filière
                                                        </option>
                                                        <option value="1">Filiere 1</option>
                                                        <option value="2">Filiere 2</option>
                                                        <option value="2">Filiere 3</option>
                                                    </select>
                                                </th>
                                                <th class="border-0">
                                                    <input type="text" placeholder="14/05/2020-21/05/2020" class="form-control">
                                                </th>

                                                <th>
                                                    <button type="submit" class="btn btn-md btn-secondary">{{ __("Générer") }}</button>
                                                </th>

                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-12">
                    <div class="card card-body bg-white border-light shadow-sm mb-4">

                        <div class="h-100vhw-100">
                            <div class="col-sm-12 d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                                @if (isset($html))
                                <div class="btn-toolbars mr-2">

                                    <button class="btn btn-outline-primary font-weight-bold btn-sm mr-2 dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                            class="fas fa-file-export mr-2"></span>{{ __('Exporter') }}</button>
                                    <div class="dropdown-menu dashboard-dropdown dropdown-menu-left mt-2">
                                        <a id="exp_xlsx" class="dropdown-item text-success" onclick="exportExcel()"
                                            href="#"><span
                                                class="fas fa-file-excel mr-2"></span>{{ __('Format Excel') }}</a>
                                        {{-- <a class="dropdown-item font-weight-bold"
                                            href="{{ route('rapport-hebdo.create') }}"><span
                                                class="fa fa-file-excel mr-2"></span>{{ __('Format Excel (CSV)') }}</a>
                                        --}}
                                        <a id="exp_pdf" onclick="demoFromHTML()"
                                            class="dropdown-item font-weight-bold text-danger" href="#"><span
                                                class="fa fa-file-pdf mr-2"></span>{{ __('Format PDF ') }}</a>
                                        <a id="exp_word" class="dropdown-item font-weight-bold text-info" href="#"><span
                                                class="fa fa-file-word mr-2"></span>{{ __('Format Word') }}</a>

                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12">
                                @if (isset($html))
                                    {!! $html !!}
                                @else
                                    <span class="text-muted text-center">
                                        Les rapports appararaissent ici
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>

    </div>
@endsection
@section('scripts')

    <script src="{{ asset('storage/lib/tableToExcel/js/tableToExcel.js') }}"></script>
    <script src="{{ asset('storage/lib/jsPdf/js/jspdf.js') }}"></script>
    <script>
        /******************************
         *
         *
         *
         * */
        function demoFromHTML() {
            var pdf = new jsPDF('l', 'pt', 'letter');
            // source can be HTML-formatted string, or a reference
            // to an actual DOM element from which the text will be scraped.
            source = $('#tb_moodler')[0];

            pdf.setFont("Times New Roman");
            pdf.setFontType("bold");
            pdf.setFontSize(4);



            // we support special element handlers. Register them with jQuery-style
            // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
            // There is no support for any other type of selectors
            // (class, of compound) at this time.
            specialElementHandlers = {
                // element with id of "bypass" - jQuery style selector
                '#bypassme': function(element, renderer) {
                    // true = "handled elsewhere, bypass text extraction"
                    return true
                }
            };
            margins = {
                top: 40,
                bottom: 60,
                left: 40,
                width: 900
            };
            // all coords and widths are in jsPDF instance's declared units
            // 'inches' in this case
            pdf.fromHTML(
                source, // HTML string or DOM elem ref.
                margins.left, // x coord
                margins.top, { // y coord
                    'width': margins.width, // max width of content on PDF
                    'elementHandlers': specialElementHandlers
                },

                function(dispose) {
                    // dispose: object with X, Y of the last line add to the PDF
                    //          this allow the insertion of new lines after html
                    pdf.save('Rapport_Moodler.pdf');
                }, margins);
        }

        /* HTML to Microsoft Word Export Demo
         * This code demonstrates how to export an html element to Microsoft Word
         * with CSS styles to set page orientation and paper size.
         * Tested with Word 2010, 2013 and FireFox, Chrome, Opera, IE10-11
         * Fails in legacy browsers (IE<10) that lack window.Blob object
         */
        function exportExcel() {
            TableToExcel.convert(document.getElementById('tb_moodler_main'));
        }

        window.exp_word.onclick = function() {
            if (!window.Blob) {
                alert('Your legacy browser does not support this action.');
                return;
            }

            var html, link, blob, url, css;

            // EU A4 use: size: 841.95pt 595.35pt;
            // US Letter use: size:11.0in 8.5in;

            css = (
                '<style>' +
                '@page WordSection1{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}' +
                'div.WordSection1 {page: WordSection1;}' +
                'table{border-collapse:collapse;}td{border:1px gray solid;width:5em;padding:2px;}' +
                '</style>'
            );

            html = window.tb_moodler.innerHTML;
            blob = new Blob(['\ufeff', css + html], {
                type: 'application/msword'
            });
            url = URL.createObjectURL(blob);
            link = document.createElement('A');
            link.href = url;
            // Set default file name.
            // Word will append file extension - do not add an extension here.
            link.download = 'Document';
            document.body.appendChild(link);
            if (navigator.msSaveOrOpenBlob) navigator.msSaveOrOpenBlob(blob, 'Rapport_Moodler.doc'); // IE10-11
            else link.click(); // other browsers
            document.body.removeChild(link);
        };

    </script>
@endsection
