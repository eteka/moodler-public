@extends('layouts.app')

@section('title',"Paramètres")

@section('content')
<div class="main">
    <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
           
          </ol>
          <h6 class="slim-pagetitle">
            {{ __("Nouvelle configuration") }}
          </h6>
          
    </div>
   
    <div class="col-lg-8 card">
      <div class="card-body">
            <form >
              <!-- Table of addresses -->
              
              <div class="mt-5">
                
                <!-- New address form -->
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label">{{ __('Nom de la configuration') }}</label>
                      <input class="form-control" type="text" placeholder="Enter your first name">
                    </div>
                  </div>
                 
                </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group focused">
                      <label class="form-control-label">Structure</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="Country" data-select2-id="1" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="3">Sectionner un modèle</option>
                        <option value="1">Université/Faculté/Cycle/Entité/Année Académique</option>
                        <option value="2">Faculté/Cycle/Entité/Année Académique</option>
                        <option value="2">Entité/Année Académique</option>
                        <option value="1">Année Académique</option>
                      </select>
                      
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group focused">
                      <label class="form-control-label">Université</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="City" data-select2-id="4" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="6">Select your city</option>
                        <option value="1">Université de Parakou</option>
                        <option value="2">Université d'Abomey-Calavi</option>
                      </select>
                      </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group focused">
                      <label class="form-control-label">Entité</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="City" data-select2-id="4" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="6">Select your city</option>
                        <option value="1">Faculté 1</option>
                        <option value="2">Ecole 2</option>
                      </select>
                      </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group focused">
                      <label class="form-control-label">Cylce</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="City" data-select2-id="4" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="6">Cycle</option>
                        <option value="1">Licence</option>
                        <option value="2">Master</option>
                        <option value="3">Doctorat</option>
                      </select>
                      </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group focused">
                      <label class="form-control-label">Entité</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="City" data-select2-id="4" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="6">Entité</option>
                        <option value="1">Entité 1</option>
                        <option value="2">Entité 2</option>
                        <option value="3">Entité 3</option>
                      </select>
                      </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group focused">
                      <label class="form-control-label">Entité</label>
                      <select class="form-control select2-hidden-accessible" data-toggle="select" title="City" data-select2-id="4" tabindex="-1" aria-hidden="true">
                        <option selected="" disabled="" data-select2-id="6">Année académique</option>
                        <option value="1">2017-2018</option>
                        <option value="2">2018-2019</option>
                        <option value="3">2019-2020</option>
                      </select>
                      </div>
                  </div>
                  
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label">{{ __('Description') }}</label>
                      <textarea class="form-control"rows="1" placeholder="Also your last name">
                      </textarea>
                    </div>
                  </div>
                </div>
              </div>
              
            </form>
          </div>
          </div>
</div>
@endsection