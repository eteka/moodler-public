@extends('layouts.app')
@section('title', 'Statistiques')

@section('content')
    <div class="bg-soft h-active">
        @include('moodler.include.top')
        <section class="bg-soft">
            <main class="">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center ">
                    <div class="d-block mb-4 mb-md-0">
                        <h2 class="h3">{{ __('Statistiques') }}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-xl-4 mb-4">
                        <div class="card border-light shadow-sm">
                            <div class="card-body d-flex flex-row align-items-center flex-0 border-bottom">
                                <div class="d-block">
                                    <div class="h6 font-weight-normal text-gray mb-2">Traffic Share</div>
                                    <h2 class="h4">Direct</h2>
                                    <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span
                                            class="text-success font-weight-bold">51.5%</span></div>
                                </div>
                                <div class="ml-auto">
                                    <div class="d-flex align-items-center mb-2"><span
                                            class="shape-xs rounded-circle bg-primary mr-2"></span> <span
                                            class="font-weight-normal small mr-3">Direct</span> <span
                                            class="small font-weight-bold text-dark ml-auto">51.50%</span></div>
                                    <div class="d-flex align-items-center mb-2"><span
                                            class="shape-xs rounded-circle bg-secondary mr-2"></span> <span
                                            class="font-weight-normal small mr-3">Referrals</span> <span
                                            class="small font-weight-bold text-dark ml-auto">29.40%</span></div>
                                    <div class="d-flex align-items-center mb-2"><span
                                            class="shape-xs rounded-circle bg-tertiary mr-2"></span> <span
                                            class="font-weight-normal small mr-3">Organic</span> <span
                                            class="small font-weight-bold text-dark ml-auto">9.10%</span></div>
                                    <div class="d-flex align-items-center mb-2"><span
                                            class="shape-xs rounded-circle bg-info mr-2"></span> <span
                                            class="font-weight-normal small mr-3">Social</span> <span
                                            class="small font-weight-bold text-dark ml-auto">6.50%</span></div>
                                    <div class="d-flex align-items-center mb-2"><span
                                            class="shape-xs rounded-circle bg-warning mr-2"></span> <span
                                            class="font-weight-normal small mr-3">Mail</span> <span
                                            class="small font-weight-bold text-dark ml-auto">3.50%</span></div>
                                </div>
                            </div>
                            <div class="card-body p-2 py-5">
                                <div class="ct-chart-traffic-share-2 ct-golden-section ct-series-a">
                                    <div class="chartist-tooltip" style="top: 128px; left: 259px;"><span
                                            class="chartist-tooltip-value">51.5</span></div><svg
                                        xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%"
                                        class="ct-chart-donut" style="width: 100%; height: 100%;">
                                        <g class="ct-series ct-series-a">
                                            <path d="M237.961,283.641A129.608,129.608,0,1,0,250.158,25"
                                                class="ct-slice-donut" ct:value="51.5" style="stroke-width: 40px;"></path>
                                        </g>
                                        <g class="ct-series ct-series-b">
                                            <path d="M129.354,107.654A129.608,129.608,0,0,0,238.412,283.683"
                                                class="ct-slice-donut" ct:value="29.4" style="stroke-width: 40px;"></path>
                                        </g>
                                        <g class="ct-series ct-series-c">
                                            <path d="M173.976,49.753A129.608,129.608,0,0,0,129.191,108.076"
                                                class="ct-slice-donut" ct:value="9.1" style="stroke-width: 40px;"></path>
                                        </g>
                                        <g class="ct-series ct-series-d">
                                            <path d="M221.885,28.121A129.608,129.608,0,0,0,173.611,50.02"
                                                class="ct-slice-donut" ct:value="6.5" style="stroke-width: 40px;"></path>
                                        </g>
                                        <g class="ct-series ct-series-e">
                                            <path d="M250.158,25A129.608,129.608,0,0,0,221.444,28.221"
                                                class="ct-slice-donut" ct:value="3.5" style="stroke-width: 40px;"></path>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-xl-8 mb-4">
                        <div class="card border-light shadow-sm">
                            <div class="card-body d-flex flex-row align-items-center flex-0 border-bottom">
                                <div class="w-100 d-flex justify-content-between align-items-start">
                                    <div>
                                        <div class="h6 font-weight-normal text-gray mb-2">Traffic Volumes by Source</div>
                                        <h2 class="h4">Direct</h2>
                                        <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span
                                                class="text-success font-weight-bold">18 B</span> <span
                                                class="font-weight-bold ml-2"><span class="icon icon-small mr-1"><span
                                                        class="fas fa-globe-europe"></span></span>WorldWide</span></div>
                                    </div>
                                    <div class="d-xl-flex flex-wrap justify-content-end">
                                        <div class="d-flex align-items-center mb-2 mr-3 lh-130"><span
                                                class="shape-xs rounded-circle bg-warning mr-2"></span> <span
                                                class="font-weight-normal small">Direct</span> <span
                                                class="small font-weight-bold text-dark ml-1">18M</span></div>
                                        <div class="d-flex align-items-center mb-2 mr-3 lh-130"><span
                                                class="shape-xs rounded-circle bg-info mr-2"></span> <span
                                                class="font-weight-normal small">Refferals</span> <span
                                                class="small font-weight-bold text-dark ml-1">15M</span></div>
                                        <div class="d-flex align-items-center mb-2 mr-3 lh-130"><span
                                                class="shape-xs rounded-circle bg-tertiary mr-2"></span> <span
                                                class="font-weight-normal small">Organic</span> <span
                                                class="small font-weight-bold text-dark ml-1">12M</span></div>
                                        <div class="d-flex align-items-center mb-2 mr-3 lh-130"><span
                                                class="shape-xs rounded-circle bg-secondary mr-2"></span> <span
                                                class="font-weight-normal small">Social</span> <span
                                                class="small font-weight-bold text-dark ml-1">6M</span></div>
                                        <div class="d-flex align-items-center mb-2 mr-3 lh-130"><span
                                                class="shape-xs rounded-circle bg-primary mr-2"></span> <span
                                                class="font-weight-normal small">Mail</span> <span
                                                class="small font-weight-bold text-dark ml-1">5M</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-2 pb-4">
                                <div class="ct-chart-volumes ct-major-tenth ct-series-a">
                                    <div class="chartist-tooltip" style="top: -29.4px; left: 302.483px;"></div><svg
                                        xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%"
                                        class="ct-chart-line" style="width: 100%; height: 100%;">
                                        <g class="ct-grids">
                                            <line y1="384.4666748046875" y2="384.4666748046875" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="356.0461613581731" y2="356.0461613581731" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="327.62564791165863" y2="327.62564791165863" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="299.2051344651442" y2="299.2051344651442" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="270.7846210186298" y2="270.7846210186298" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="242.3641075721154" y2="242.3641075721154" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="213.94359412560095" y2="213.94359412560095" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="185.52308067908655" y2="185.52308067908655" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="157.1025672325721" y2="157.1025672325721" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="128.68205378605768" y2="128.68205378605768" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="100.26154033954327" y2="100.26154033954327" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="71.84102689302887" y2="71.84102689302887" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="43.420513446514406" y2="43.420513446514406" x1="50"
                                                x2="1033.6666259765625" class="ct-grid ct-vertical"></line>
                                            <line y1="15" y2="15" x1="50" x2="1033.6666259765625"
                                                class="ct-grid ct-vertical"></line>
                                        </g>
                                        <g>
                                            <g class="ct-series ct-series-a">
                                                <path
                                                    d="M50,361.73C104.648,350.362,159.296,327.626,213.944,327.626C268.593,327.626,323.241,361.73,377.889,361.73C432.537,361.73,487.185,354.151,541.833,350.362C596.481,346.573,651.13,344.046,705.778,338.994C760.426,333.941,815.074,323.836,869.722,316.257C924.37,308.679,979.018,301.1,1033.667,293.521"
                                                    class="ct-line"></path>
                                                <line x1="50" y1="361.73026404747594" x2="50.01" y2="361.73026404747594"
                                                    class="ct-point" ct:value="2"></line>
                                                <line x1="213.9444376627604" y1="327.62564791165863" x2="213.9544376627604"
                                                    y2="327.62564791165863" class="ct-point" ct:value="5"></line>
                                                <line x1="377.8888753255208" y1="361.73026404747594" x2="377.8988753255208"
                                                    y2="361.73026404747594" class="ct-point" ct:value="2"></line>
                                                <line x1="541.8333129882812" y1="350.3620586688702" x2="541.8433129882812"
                                                    y2="350.3620586688702" class="ct-point" ct:value="3"></line>
                                                <line x1="705.7777506510416" y1="338.99385329026444" x2="705.7877506510416"
                                                    y2="338.99385329026444" class="ct-point" ct:value="4"></line>
                                                <line x1="869.722188313802" y1="316.2574425330529" x2="869.732188313802"
                                                    y2="316.2574425330529" class="ct-point" ct:value="6"></line>
                                                <line x1="1033.6666259765625" y1="293.5210317758414" x2="1033.6766259765625"
                                                    y2="293.5210317758414" class="ct-point" ct:value="8"></line>
                                            </g>
                                            <g class="ct-series ct-series-b">
                                                <path
                                                    d="M50,327.626C104.648,323.836,159.296,316.257,213.944,316.257C268.593,316.257,323.241,327.626,377.889,327.626C432.537,327.626,487.185,306.513,541.833,293.521C596.481,280.529,651.13,273.311,705.778,248.048C760.426,222.786,815.074,20.684,869.722,20.684C924.37,20.684,979.018,50.999,1033.667,66.157"
                                                    class="ct-line"></path>
                                                <line x1="50" y1="327.62564791165863" x2="50.01" y2="327.62564791165863"
                                                    class="ct-point" ct:value="5"></line>
                                                <line x1="213.9444376627604" y1="316.2574425330529" x2="213.9544376627604"
                                                    y2="316.2574425330529" class="ct-point" ct:value="6"></line>
                                                <line x1="377.8888753255208" y1="327.62564791165863" x2="377.8988753255208"
                                                    y2="327.62564791165863" class="ct-point" ct:value="5"></line>
                                                <line x1="541.8333129882812" y1="293.5210317758414" x2="541.8433129882812"
                                                    y2="293.5210317758414" class="ct-point" ct:value="8"></line>
                                                <line x1="705.7777506510416" y1="248.04821026141826" x2="705.7877506510416"
                                                    y2="248.04821026141826" class="ct-point" ct:value="12"></line>
                                                <line x1="869.722188313802" y1="20.684102689302904" x2="869.732188313802"
                                                    y2="20.684102689302904" class="ct-point" ct:value="32"></line>
                                                <line x1="1033.6666259765625" y1="66.15692420372596" x2="1033.6766259765625"
                                                    y2="66.15692420372596" class="ct-point" ct:value="28"></line>
                                            </g>
                                            <g class="ct-series ct-series-c">
                                                <path
                                                    d="M50,304.889C104.648,285.942,159.296,248.048,213.944,248.048C268.593,248.048,323.241,288.047,377.889,304.889C432.537,321.731,487.185,344.299,541.833,350.362C596.481,356.425,651.13,361.73,705.778,361.73C760.426,361.73,815.074,326.994,869.722,304.889C924.37,282.784,979.018,251.838,1033.667,225.312"
                                                    class="ct-line"></path>
                                                <line x1="50" y1="304.88923715444713" x2="50.01" y2="304.88923715444713"
                                                    class="ct-point" ct:value="7"></line>
                                                <line x1="213.9444376627604" y1="248.04821026141826" x2="213.9544376627604"
                                                    y2="248.04821026141826" class="ct-point" ct:value="12"></line>
                                                <line x1="377.8888753255208" y1="304.88923715444713" x2="377.8988753255208"
                                                    y2="304.88923715444713" class="ct-point" ct:value="7"></line>
                                                <line x1="541.8333129882812" y1="350.3620586688702" x2="541.8433129882812"
                                                    y2="350.3620586688702" class="ct-point" ct:value="3"></line>
                                                <line x1="705.7777506510416" y1="361.73026404747594" x2="705.7877506510416"
                                                    y2="361.73026404747594" class="ct-point" ct:value="2"></line>
                                                <line x1="869.722188313802" y1="304.88923715444713" x2="869.732188313802"
                                                    y2="304.88923715444713" class="ct-point" ct:value="7"></line>
                                                <line x1="1033.6666259765625" y1="225.31179950420673"
                                                    x2="1033.6766259765625" y2="225.31179950420673" class="ct-point"
                                                    ct:value="14"></line>
                                            </g>
                                            <g class="ct-series ct-series-d">
                                                <path
                                                    d="M50,270.785C104.648,251.838,159.296,213.944,213.944,213.944C268.593,213.944,323.241,236.68,377.889,236.68C432.537,236.68,487.185,191.207,541.833,191.207C596.481,191.207,651.13,225.312,705.778,225.312C760.426,225.312,815.074,189.944,869.722,179.839C924.37,169.734,979.018,164.681,1033.667,157.103"
                                                    class="ct-line"></path>
                                                <line x1="50" y1="270.7846210186298" x2="50.01" y2="270.7846210186298"
                                                    class="ct-point" ct:value="10"></line>
                                                <line x1="213.9444376627604" y1="213.94359412560095" x2="213.9544376627604"
                                                    y2="213.94359412560095" class="ct-point" ct:value="15"></line>
                                                <line x1="377.8888753255208" y1="236.6800048828125" x2="377.8988753255208"
                                                    y2="236.6800048828125" class="ct-point" ct:value="13"></line>
                                                <line x1="541.8333129882812" y1="191.20718336838942" x2="541.8433129882812"
                                                    y2="191.20718336838942" class="ct-point" ct:value="17"></line>
                                                <line x1="705.7777506510416" y1="225.31179950420673" x2="705.7877506510416"
                                                    y2="225.31179950420673" class="ct-point" ct:value="14"></line>
                                                <line x1="869.722188313802" y1="179.83897798978364" x2="869.732188313802"
                                                    y2="179.83897798978364" class="ct-point" ct:value="18"></line>
                                                <line x1="1033.6666259765625" y1="157.1025672325721" x2="1033.6766259765625"
                                                    y2="157.1025672325721" class="ct-point" ct:value="20"></line>
                                            </g>
                                            <g class="ct-series ct-series-e">
                                                <path
                                                    d="M50,202.575C104.648,194.997,159.296,179.839,213.944,179.839C268.593,179.839,323.241,179.839,377.889,179.839C432.537,179.839,487.185,157.103,541.833,157.103C596.481,157.103,651.13,157.103,705.778,157.103C760.426,157.103,815.074,157.103,869.722,157.103C924.37,157.103,979.018,134.366,1033.667,122.998"
                                                    class="ct-line"></path>
                                                <line x1="50" y1="202.5753887469952" x2="50.01" y2="202.5753887469952"
                                                    class="ct-point" ct:value="16"></line>
                                                <line x1="213.9444376627604" y1="179.83897798978364" x2="213.9544376627604"
                                                    y2="179.83897798978364" class="ct-point" ct:value="18"></line>
                                                <line x1="377.8888753255208" y1="179.83897798978364" x2="377.8988753255208"
                                                    y2="179.83897798978364" class="ct-point" ct:value="18"></line>
                                                <line x1="541.8333129882812" y1="157.1025672325721" x2="541.8433129882812"
                                                    y2="157.1025672325721" class="ct-point" ct:value="20"></line>
                                                <line x1="705.7777506510416" y1="157.1025672325721" x2="705.7877506510416"
                                                    y2="157.1025672325721" class="ct-point" ct:value="20"></line>
                                                <line x1="869.722188313802" y1="157.1025672325721" x2="869.732188313802"
                                                    y2="157.1025672325721" class="ct-point" ct:value="20"></line>
                                                <line x1="1033.6666259765625" y1="122.99795109675483"
                                                    x2="1033.6766259765625" y2="122.99795109675483" class="ct-point"
                                                    ct:value="23"></line>
                                            </g>
                                        </g>
                                        <g class="ct-labels">
                                            <foreignObject style="overflow: visible;" x="50" y="389.4666748046875"
                                                width="163.9444376627604" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 164px; height: 20px;">Mar 16</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="213.9444376627604"
                                                y="389.4666748046875" width="163.9444376627604" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 164px; height: 20px;">Apr 16</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="377.8888753255208"
                                                y="389.4666748046875" width="163.94443766276044" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 164px; height: 20px;">May 16</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="541.8333129882812"
                                                y="389.4666748046875" width="163.94443766276038" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 164px; height: 20px;">Jun 16</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="705.7777506510416"
                                                y="389.4666748046875" width="163.94443766276038" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 164px; height: 20px;">Jul 16</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="869.722188313802"
                                                y="389.4666748046875" width="163.9444376627605" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 164px; height: 20px;">Aug 16</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="1033.6666259765625"
                                                y="389.4666748046875" width="30" height="20"><span
                                                    class="ct-label ct-horizontal ct-end"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="width: 30px; height: 20px;">Sept 16</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="356.0461613581731" x="10"
                                                height="28.420513446514423" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">0M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="327.6256479116587" x="10"
                                                height="28.420513446514423" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">2.5M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="299.2051344651442" x="10"
                                                height="28.420513446514427" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">5M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="270.7846210186298" x="10"
                                                height="28.42051344651442" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">7.5M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="242.36410757211542" x="10"
                                                height="28.42051344651442" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">10M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="213.94359412560095" x="10"
                                                height="28.420513446514434" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">12.5M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="185.52308067908655" x="10"
                                                height="28.420513446514406" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">15M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="157.1025672325721" x="10"
                                                height="28.420513446514434" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">17.5M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="128.68205378605768" x="10"
                                                height="28.420513446514434" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">20M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="100.26154033954327" x="10"
                                                height="28.420513446514406" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">22.5M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="71.84102689302887" x="10"
                                                height="28.420513446514406" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">25M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="43.420513446514406" x="10"
                                                height="28.420513446514462" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">27.5M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="15" x="10"
                                                height="28.420513446514406" width="30"><span
                                                    class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 28px; width: 30px;">30M</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="-15" x="10" height="30" width="30">
                                                <span class="ct-label ct-vertical ct-start"
                                                    xmlns="http://www.w3.org/2000/xmlns/"
                                                    style="height: 30px; width: 30px;">32.5M</span></foreignObject>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mb-4">
                        <div class="card border-light shadow-sm">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-centered table-nowrap mb-0 rounded">
                                        <thead class="thead-light">
                                            <tr>
                                                <th class="border-0">#</th>
                                                <th class="border-0">Traffic Source</th>
                                                <th class="border-0">Source Type</th>
                                                <th class="border-0">Category</th>
                                                <th class="border-0">Global Rank</th>
                                                <th class="border-0">Traffic Share</th>
                                                <th class="border-0">Change</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="border-0"><a href="#" class="text-primary font-weight-bold">1</a>
                                                </td>
                                                <td class="border-0 font-weight-bold"><span
                                                        class="icon icon-xs icon-gray w-30"><span
                                                            class="fas fa-globe-europe"></span></span>Direct</td>
                                                <td class="border-0">Direct</td>
                                                <td class="border-0">-</td>
                                                <td class="border-0">--</td>
                                                <td class="border-0">
                                                    <div class="row d-flex align-items-center">
                                                        <div class="col-12 col-xl-2 px-0">
                                                            <div class="small font-weight-bold">51%</div>
                                                        </div>
                                                        <div class="col-12 col-xl-10 px-0 px-xl-1">
                                                            <div class="progress progress-lg mb-0">
                                                                <div class="progress-bar bg-primary" role="progressbar"
                                                                    aria-valuenow="51" aria-valuemin="0" aria-valuemax="100"
                                                                    style="width: 51%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="border-0 text-success"><span class="fas fa-angle-up"></span>
                                                    <span class="font-weight-bold">2.45%</span></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="text-primary font-weight-bold">2</a></td>
                                                <td class="font-weight-bold"><span class="icon icon-xs icon-info w-30"><span
                                                            class="fab fa-google"></span></span>Google Search</td>
                                                <td>Search / Organic</td>
                                                <td>-</td>
                                                <td>--</td>
                                                <td>
                                                    <div class="row d-flex align-items-center">
                                                        <div class="col-12 col-xl-2 px-0">
                                                            <div class="small font-weight-bold">18%</div>
                                                        </div>
                                                        <div class="col-12 col-xl-10 px-0 px-xl-1">
                                                            <div class="progress progress-lg mb-0">
                                                                <div class="progress-bar bg-primary" role="progressbar"
                                                                    aria-valuenow="18" aria-valuemin="0" aria-valuemax="100"
                                                                    style="width: 18%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-success"><span class="fas fa-angle-up"></span> <span
                                                        class="font-weight-bold">17.67%</span></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="text-primary font-weight-bold">3</a></td>
                                                <td class="font-weight-bold"><span
                                                        class="icon icon-xs icon-danger w-30"><span
                                                            class="fab fa-youtube"></span></span> youtube.com</td>
                                                <td>Social</td>
                                                <td><a class="small font-weight-bold" href="#">Arts and Entertainment</a>
                                                </td>
                                                <td>#2</td>
                                                <td>
                                                    <div class="row d-flex align-items-center">
                                                        <div class="col-12 col-xl-2 px-0">
                                                            <div class="small font-weight-bold">18%</div>
                                                        </div>
                                                        <div class="col-12 col-xl-10 px-0 px-xl-1">
                                                            <div class="progress progress-lg mb-0">
                                                                <div class="progress-bar bg-primary" role="progressbar"
                                                                    aria-valuenow="18" aria-valuemin="0" aria-valuemax="100"
                                                                    style="width: 18%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="text-primary font-weight-bold">4</a></td>
                                                <td class="font-weight-bold"><span
                                                        class="icon icon-xs icon-purple w-30"><span
                                                            class="fab fa-yahoo"></span></span> yahoo.com</td>
                                                <td>Referral</td>
                                                <td><a class="small font-weight-bold" href="#">News and Media</a></td>
                                                <td>#11</td>
                                                <td>
                                                    <div class="row d-flex align-items-center">
                                                        <div class="col-12 col-xl-2 px-0">
                                                            <div class="small font-weight-bold">8%</div>
                                                        </div>
                                                        <div class="col-12 col-xl-10 px-0 px-xl-1">
                                                            <div class="progress progress-lg mb-0">
                                                                <div class="progress-bar bg-primary" role="progressbar"
                                                                    aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"
                                                                    style="width: 8%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-danger"><span class="fas fa-angle-down"></span> <span
                                                        class="font-weight-bold">9.30%</span></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="text-primary font-weight-bold">5</a></td>
                                                <td class="font-weight-bold"><span class="icon icon-xs icon-info w-30"><span
                                                            class="fab fa-twitter"></span></span> twitter.com</td>
                                                <td>Social</td>
                                                <td><a class="small font-weight-bold" href="#">Social Networks</a></td>
                                                <td>#4</td>
                                                <td>
                                                    <div class="row d-flex align-items-center">
                                                        <div class="col-12 col-xl-2 px-0">
                                                            <div class="small font-weight-bold">4%</div>
                                                        </div>
                                                        <div class="col-12 col-xl-10 px-0 px-xl-1">
                                                            <div class="progress progress-lg mb-0">
                                                                <div class="progress-bar bg-primary" role="progressbar"
                                                                    aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"
                                                                    style="width: 4%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>-</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </main>
        </section>
    </div>
@endsection
