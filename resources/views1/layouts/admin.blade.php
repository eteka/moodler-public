<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
   <head>
      <title>@yield('title',"Administration")  {{ config("app.name")!=""?"| ".config("app.name"):"Beinvenue sur MoodleR" }}</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
      <meta name="title" content="{{  config("app.name") }} - Dashboard">
      <meta name="author" content="{{  config("app.name") }}">
      <meta name="description" content="{{  config("app.description") }}">
      <meta name="keywords" content="{{  config("app.keyword") }}">
      <meta property="og:type" content="website">
      <meta property="og:url" content="{{  url("/") }}">
      <meta property="og:title" content="{{  config("app.name") }} - Dashboard">
      <meta property="og:description" content="{{  config("app.description") }}">
      <meta property="og:image" content="{{  config("app.app-img-preview") }}">
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:url" content="{{  url("/") }}">
      <meta property="twitter:title" content="{{  config("app.name") }} - Dashboard">
      <meta property="twitter:description" content="{{  config("app.description") }}">
      <meta property="twitter:image" content="{{  config("app.app-img-preview") }}">
      <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('./assets/assets/img/favicon/apple-touch-icon.png') }}">
      <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('./assets/assets/img/favicon/favicon-32x32.png') }}">
      <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('./assets/assets/img/favicon/favicon-16x16.png') }}">
      
      <link rel="mask-icon" href="{{ asset('./assets/assets/img/favicon/safari-pinned-tab.svg') }}" color="#ffffff">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="theme-color" content="#ffffff">
      <link type="text/css" href="{{ asset("./storage/lib/fontawesome-free/css/all.min.css") }}" rel="stylesheet">
      <!--link type="text/css" href="./storage/moodler/admin/prism.min.css" rel="stylesheet"-->
      <link rel="stylesheet" href="{{ asset("./storage/moodler/admin/css/jqvmap.min.css") }}">
      <link type="text/css" href="{{ asset("./storage/moodler/admin/css/rocket.css") }}" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset("./storage/lib/perfect-scrollbar/css/perfect-scrollbar.css") }}">
     
   </head>
   <body>
      {{-- <div class="preloader bg-soft flex-column justify-content-center align-items-center"><img class="loader-element" src="./assets/assets/img/brand/dark.svg" height="50" alt="Rocket logo"></div> --}}
      <nav class="navbar navbar-dark navbar-theme-primary col-12 d-md-none">
         <a class="navbar-brand mr-lg-5" href="./assets/index.html">
          <img class="navbar-brand-dark" src="./assets/assets/img/brand/light.svg" alt="Pixel logo"--> <img class="navbar-brand-light" src="./assets/assets/img/brand/dark.svg" alt="Pixel Logo Dark"></a>
         <div class="d-flex align-items-center"><button class="navbar-toggler d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button></div>
      </nav>
      <div class="container-fluid bg-soft">
         <div class="row">
            <div class="col-12">
               <nav id="sidebarMenu" class="sidebar d-md-block bg-primary text-white collapse px-2">
                  <div class="sidebar-sticky pt-4 mx-auto">
                     <div class="user-card d-flex align-items-center justify-content-between justify-content-md-center pb-4">
                        <div class="d-flex align-items-center">
                           <div class="user-avatar lg-avatar mr-4"><img src="{{asset(Auth::user()->photoProfil())}}" alt="{{ Auth::user()->name }}" style="border:1px soolid red" class="card-img-top rounded-circle border-white" alt="Bonnie Green"></div>
                           <div class="d-block">
                              <h2 class="h6">Bonjour, Wilfried</h2>
                              <a href="../sign-in.html" class="btn btn-secondary btn-xs"><span class="mr-2"><span class="fas fa-sign-out-alt"></span></span>{{ __("Se déconnecter") }}</a>
                           </div>
                        </div>
                        <div class="collapse-close d-md-none"><a href="#sidebarMenu" class="fas fa-times" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="true" aria-label="Toggle navigation"></a></div>
                     </div>
                     <ul class="nav flex-column mt-4">
                        <li class="nav-item active"><a href="../dashboard/dashboard.html" class="nav-link"><span class="sidebar-icon"><span class="fas fa-chart-pie"></span></span> <span>{{ __('Tableau de bord') }}</span></a></li>
                        <li class="nav-item"><a href="../dashboard/messages.html" class="nav-link d-flex align-items-center justify-content-between"><span><span class="sidebar-icon"><span class="fas fa-folder"></span></span> {{ __('Rapports') }} </span><span class="badge badge-md badge-danger badge-pill">4</span></a></li>
                        <li class="nav-item"><a href="../dashboard/traffic-sources.html" class="nav-link"><span class="sidebar-icon"><span class="fas fa-chart-line"></span></span> <span>{{ __('Statistiques') }}</span></a></li>
                        <li class="nav-item"><a href="../dashboard/app-analysis.html" class="nav-link"><span class="sidebar-icon"><span class="fas fa-history"></span></span> <span>{{ __('Historiques') }}</span></a></li>
                        <li class="nav-item"><a href="../dashboard/users.html" class="nav-link"><span class="sidebar-icon"><span class="fas fa-users"></span></span> <span>{{ __('Utilisateurs') }}</span></a></li>
                        <li class="nav-item"><a href="../dashboard/transactions.html" class="nav-link"><span class="sidebar-icon"><span class="fas fa-check-double"></span></span> <span>{{ __("Vérification")}}</span></a></li>
                        <li class="nav-item"><a href="../dashboard/tasks.html" class="nav-link"><span class="sidebar-icon"><span class="fas fa-clipboard-list"></span></span> <span>{{ __("Paramètres d'exportation") }}</span></a></li>
                        <li class="nav-item"><a href="../dashboard/settings.html" class="nav-link"><span class="sidebar-icon"><span class="fas fa-cog"></span></span> <span>{{ __("Configurations") }}</span></a></li>
                        <li class="nav-item">
                           <a class="nav-link collapsed d-flex justify-content-between align-items-center" href="#submenu-app" data-toggle="collapse" data-target="#submenu-app"><span><span class="sidebar-icon"><span class="fas fa-user-shield mr-2"></span></span>{{ __('Mon compte') }} </span><span class="link-arrow"><span class="fas fa-chevron-right"></span></span></a>
                           <div class="multi-level collapse" role="list" id="submenu-app" aria-expanded="false">
                              <ul class="flex-column nav small">
                                 <li class="nav-item"><a class="nav-link small" href="#"><span>{{__('Mon profil') }}</span> </a></li>
                                 <li class="nav-item"><a class="nav-link small" href="#"><span>{{__('Changer mot de passe') }}</span></a></li>
                                 <li class="nav-item"><a class="nav-link small" href="#"><span>{{__('Préférences') }}</span></a></li>
                              </ul>
                           </div>
                        </li>
                        <li role="separator" class="dropdown-divider mt-4 mb-3 border-blue"></li>
                        <li class="nav-item"><a href="{{ url('docs') }}" target="_blank" class="nav-link"><span class="sidebar-icon"><span class="fas fa-book"></span></span> <span>{{ __("Documentation ")}} </a></li>
                        <li class="nav-item"><a href="{{ url('contact') }}" target="_blank" class="nav-link"><span class="sidebar-icon"><span class="fas fa-pen"></span></span> <span>{{ __("Signaler un problème")}}</span></a></li>
                        <!--li class="nav-item"><a href="https://themes.getbootstrap.com/product/rocket/" target="_blank" class="nav-link"><span class="sidebar-icon"><span class="fas fa-shopping-cart"></span></span> <span>Buy now</span></a></li>
                        <li class="nav-item"><a href="https://themesberg.com" target="_blank" class="nav-link d-flex align-items-center"><span class="sidebar-icon"><img src="./assets/assets/img/themesberg.svg" height="20" width="20" class="mr-1" alt="Themesberg Logo"> </span><span>Themesberg</span></a></li-->
                     </ul>
                  </div>
               </nav>
               <main class="content">
                  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
                     <div class="btn-toolbar">
                        <button class="btn btn-primary btn-sm mr-2 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-plus mr-2"></span>{{ __("Nouvelle tâche") }}</button>
                        <div class="dropdown-menu dashboard-dropdown dropdown-menu-left mt-2">
                           <a class="dropdown-item font-weight-bold" href="#"><span class="fas fa-calendar-week mr-2"></span>{{ __("Rapport hebdomadaire") }}</a> 
                           <a class="dropdown-item font-weight-bold" href="#"><span class="fas fa-calendar mr-2"></span>{{ __("Rapport Mensuel") }}</a> 
                           <a class="dropdown-item font-weight-bold" href="#"><span class="fas fa-calendar-check mr-2"></span>{{ __("Rapport par semestre") }}</a>
                           <div role="separator" class="dropdown-divider"></div>
                           <a class="dropdown-item font-weight-bold" href="#"><span class="fas fa-clipboard-list text-danger mr-2"></span>{{ __("Paramètres d'exportation") }}</a>
                           <a class="dropdown-item font-weight-bold active" href="#"><span class="fas fa-cog text-danger mr-2"></span>{{ __("Configuration du système ") }}</a>
                        </div>
                     </div>
                     
                     <div class="btn-group mr-2"><button type="button" class="btn btn-sm btn-secondary">{{ __("Français") }}</button> <button type="button" class="btn btn-sm btn-outline-secondary">{{ __("English") }}</button></div>
                  </div>
                  <div class="row justify-content-md-center">
                     <div class="col-12 col-sm-6 col-xl-4 mb-4">
                        <div class="card border-light shadow-sm">
                           <div class="card-body">
                              <div class="row d-block d-xl-flex align-items-center">
                                 <div class="col-12 col-xl-4 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                                    <div class="icon icon-shape icon-md icon-shape-slack rounded mr-4 mr-sm-0"><span class="fas fa-calendar-week"></span></div>
                                    <div class="d-sm-none">
                                       <h2 class="h5">{{ __("Rapport Hebdomadaire") }}</h2>
                                       <h3 class="mb-1">345,678</h3>
                                    </div>
                                 </div>
                                 <div class="col-12 col-xl-8 px-xl-0">
                                    <div class="d-none d-sm-block">
                                       <h2 class="h5">{{ __("Rapport Hebdomadaire") }}</h2>
                                       <h3 class="mb-1">345,678</h3>
                                    </div>
                                    <small>Feb 1 - Apr 1, <span class="icon icon-small"><span class="fas fa-calendar"></span></span> WorldWide</small>
                                    <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span class="text-success font-weight-bold">18.2%</span> Since last month</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-sm-6 col-xl-4 mb-4">
                        <div class="card border-light shadow-sm">
                           <div class="card-body">
                              <div class="row d-block d-xl-flex align-items-center">
                                 <div class="col-12 col-xl-4 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                                    <div class="icon icon-shape icon-md icon-shape-warning rounded mr-4"><span class="fas  fa-calendar"></span></div>
                                    <div class="d-sm-none">
                                       <h2 class="h5">{{ __("Rapport Mensuel") }}</h2>
                                       <h3 class="mb-1">5,342</h3>
                                    </div>
                                 </div>
                                 <div class="col-12 col-xl-8 px-xl-0">
                                    <div class="d-none d-sm-block">
                                       <h2 class="h5">{{ __("Rapport mensuel") }}</h2>
                                       <h3 class="mb-1">5,342</h3>
                                    </div>
                                    <small>Feb 1 - Apr 1, <span class="icon icon-small"><span class="fas fa-calendar-check"></span></span> Worldwide</small>
                                    <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span class="text-success font-weight-bold">28.2%</span> Since last month</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-sm-6 col-xl-4 mb-4">
                        <div class="card border-light shadow-sm">
                           <div class="card-body">
                              <div class="row d-block d-xl-flex align-items-center">
                                 <div class="col-12 col-xl-4 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                                  <div class="icon icon-shape icon-md icon-shape-dropbox rounded mr-4"><span class="fas  fa-calendar-check"></span></div>
                                 </div>
                                 <div class="col-12 col-xl-8 px-xl-0">
                                    <h2 class="h5">{{ __("Rapport par semestre") }}</h2>
                                    <div class="mb-3 small">Feb 1 - Apr 1, <span class="icon icon-small"><span class="fas fa-calendar-check"></span></span> Worldwide</div>
                                    <h6 class="font-weight-normal text-gray"><span class="icon w-20 icon-xs icon-secondary mr-1"><span class="fas fa-desktop"></span></span> Desktop <a href="#" class="h6">70%</a></h6>
                                    <h6 class="font-weight-normal text-gray"><span class="icon w-20 icon-xs icon-primary mr-1"><span class="fas fa-mobile-alt"></span></span> Mobile Web <a href="#" class="h6">30%</a></h6>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12 col-xl-8 mb-4">
                        <div class="row">
                           <!--div class="col-12 mb-4">
                              <div class="card border-light shadow-sm">
                                 <div class="card-body d-flex flex-row align-items-center flex-0 border-bottom">
                                    <div class="d-block">
                                       <div class="h6 font-weight-normal text-gray mb-2">Sales Value</div>
                                       <h2 class="h3">10,567</h2>
                                       <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span class="text-success font-weight-bold">$10.57%</span></div>
                                    </div>
                                    <div class="d-flex ml-auto"><a href="#" class="btn btn-tertiary btn-sm mr-3">Month</a> <a href="#" class="btn btn-white border-light btn-sm mr-3">Week</a></div>
                                 </div>
                                 <div class="card-body p-2">
                                    <div class="ct-chart-sales-value ct-major-tenth ct-series-b"></div>
                                 </div>
                              </div>
                           </div-->
                           <div class="col-12">
                              <div class="card border-light shadow-sm">
                                 <div class="card-header">
                                    <div class="row align-items-center">
                                       <div class="col">
                                          <h2 class="h5">{{ __('Les derniers rapports') }}</h2>
                                       </div>
                                       <div class="col text-right"><a href="#" class="btn btn-sm btn-secondary">{{ __('Voir tout') }}</a></div>
                                    </div>
                                 </div>
                                 <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                       <thead class="thead-light">
                                          <tr>
                                             <th scope="col">{{ __('Type de rapport') }}</th>
                                             <th scope="col">{{ __('Détails') }}</th>
                                             <th scope="col">{{ __('Taille') }}</th>
                                             <th scope="col">{{ __("Format ") }}</th>
                                             <th scope="col">{{ __("Fichier ") }}</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <th scope="row">/demo/admin/index.html</th>
                                             <td>3,225</td>
                                             <td>$20</td>
                                             <td><span class="fas fa-arrow-up text-danger mr-3"></span> 42,55%</td>
                                             <td><span class="fas fa-arrow-up text-danger mr-3"></span> 42,55%</td>
                                          </tr>
                                          <tr>
                                             <th scope="row">/demo/admin/forms.html</th>
                                             <td>2,987</td>
                                             <td>0</td>
                                             <td><span class="fas fa-arrow-down text-success mr-3"></span> 43,52%</td>
                                             <td><span class="fas fa-arrow-down text-success mr-3"></span> 43,52%</td>
                                          </tr>
                                          <tr>
                                             <th scope="row">/demo/admin/util.html</th>
                                             <td>2,844</td>
                                             <td>294</td>
                                             <td><span class="fas fa-arrow-down text-success mr-3"></span> 32,35%</td>
                                             <td><span class="fas fa-arrow-down text-success mr-3"></span> 32,35%</td>
                                          </tr>
                                          <tr>
                                             <th scope="row">/demo/admin/validation.html</th>
                                             <td>2,050</td>
                                             <td>$147</td>
                                             <td><span class="fas fa-arrow-up text-danger mr-3"></span> 50,87%</td>
                                             <td><span class="fas fa-arrow-up text-danger mr-3"></span> 50,87%</td>
                                          </tr>
                                          <tr>
                                             <th scope="row">/demo/admin/modals.html</th>
                                             <td>1,483</td>
                                             <td>$19</td>
                                             <td><span class="fas fa-arrow-down text-success mr-3"></span> 32,24%</td>
                                             <td><span class="fas fa-arrow-down text-success mr-3"></span> 32,24%</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-xl-4 mb-4">
                        <div class="col-12 px-0 mb-4">
                           <div class="card border-light shadow-sm">
                              <div class="card-body">
                                 <h2 class="h5">Acquisition</h2>
                                 <p>Tells you where your visitors originated from, such as search engines, social networks or website referrals.</p>
                                 <div class="d-block">
                                    <div class="d-flex align-items-center pt-3 mr-5">
                                       <div class="icon icon-shape icon-sm icon-shape-primary rounded mr-3"><span class="fas fa-chart-bar"></span></div>
                                       <div class="d-block">
                                          <label class="mb-0">Bounce Rate</label>
                                          <h4 class="mb-0">33.50%</h4>
                                       </div>
                                    </div>
                                    <div class="d-flex align-items-center pt-3">
                                       <div class="icon icon-shape icon-sm icon-shape-secondary rounded mr-3"><span class="fas fa-chart-area"></span></div>
                                       <div class="d-block">
                                          <label class="mb-0">Sessions</label>
                                          <h4 class="mb-0">9,567</h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                  </div>
                  <footer class="footer section py-3">
                     <div class="container-fluid">
                        <div class="row">
                           <div class="col-md-12">
                              <hr>
                           </div>
                           <div class="col-12 col-lg-6 mb-4 mb-lg-0">
                              <p class="mb-0 text-center text-xl-left">Copyright © {{ date('Y') }}-<span class="current-year"></span> <a class="text-primary font-weight-normal" href="{{ url('/') }}" target="_blank">{{ __("Moodle Report") }}</a></p>
                           </div>
                           <div class="col-12 col-lg-6">
                              <ul class="list-inline list-group-flush list-group-borderless text-center text-xl-right mb-0">
                                 <li class="list-inline-item px-0 px-sm-2"><a href="{{ url("about") }}">{{ __("A propos") }}</a></li>
                                 <li class="list-inline-item px-0 px-sm-2"><a href="{{ url("docs") }}">{{ __("Documentation") }}</a></li>
                                 <li class="list-inline-item px-0 px-sm-2"><a href="{{ url("blog") }}">{{ __("Blog") }}</a></li>
                                 <li class="list-inline-item px-0 px-sm-2"><a href="{{ url("contact") }}">{{ __("Contact") }}</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </footer>
               </main>
            </div>
         </div>
      </div>
     
      <script src="./storage/lib/jquery/js/jquery.js"></script>
      <script src="./storage/lib/popper.js/js/popper.js"></script>
      <script src="./storage/lib/bootstrap/js/bootstrap.js"></script>
      <script src="./storage/vendor/headroom.js/dist/headroom.min.js"></script>
      <script src="./storage/vendor/countup.js/dist/countUp.min.js"></script>
      <script src="./storage/vendor/jquery-countdown/dist/jquery.countdown.min.js">
      </script><script src="./storage/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
      <script src="./storage/vendor/prismjs/prism.js"></script>
      <script src="./storage/vendor/chartist/dist/chartist.min.js"></script>
      <script src="./storage/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js">
      </script><script src="./storage/vendor/jqvmap/dist/jquery.vmap.min.js"></script>
      <script src="./storage/vendor/jqvmap/dist/maps/jquery.vmap.world.js"></script>
      <script src="./storage/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
      <script src="./storage/assets/assets/js/rocket.js"></script>
      <script>
        /* const ps = new PerfectScrollbar('#sidebarMenu', {
         wheelSpeed: 2,
         wheelPropagation: true,
         minScrollbarLength: 20
         });*/
         if($.fn.perfectScrollbar) {
           // alert('')
           $('div.sidebar-sticky').perfectScrollbar({
      suppressScrollX: true
    });
  }
      </script>
      
   </body>
</html>

