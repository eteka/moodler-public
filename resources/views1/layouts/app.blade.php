<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <title>@yield('title',"Administration")
        {{ config('app.name') != '' ? '| ' . config('app.name') : 'Beinvenue sur MoodleR' }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="title" content="{{ config('app.name') }} - Dashboard">
    <meta name="author" content="{{ config('app.name') }}">
    <meta name="description" content="{{ config('app.description') }}">
    <meta name="keywords" content="{{ config('app.keyword') }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:title" content="{{ config('app.name') }} - Dashboard">
    <meta property="og:description" content="{{ config('app.description') }}">
    <meta property="og:image" content="{{ config('app.app-img-preview') }}">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="{{ url('/') }}">
    <meta property="twitter:title" content="{{ config('app.name') }} - Dashboard">
    <meta property="twitter:description" content="{{ config('app.description') }}">
    <meta property="twitter:image" content="{{ config('app.app-img-preview') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('./assets/assets/img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('./assets/assets/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('./assets/assets/img/favicon/favicon-16x16.png') }}">

    <link rel="mask-icon" href="{{ asset('./assets/assets/img/favicon/safari-pinned-tab.svg') }}" color="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link type="text/css" href="{{ asset('./storage/lib/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!--link type="text/css" href="./storage/moodler/admin/prism.min.css" rel="stylesheet"-->
    <link rel="stylesheet" href="{{ asset('./storage/moodler/admin/css/jqvmap.min.css') }}">
    <link type="text/css" href="{{ asset('./storage/moodler/admin/css/rocket.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('./storage/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}">

</head>

<body>
    {{-- <div
        class="preloader bg-soft flex-column justify-content-center align-items-center"><img class="loader-element"
            src="./assets/assets/img/brand/dark.svg" height="50" alt="Rocket logo"></div>
    --}}
    <nav class="navbar navbar-dark navbar-theme-primary col-12 d-md-none">
        <a class="navbar-brand mr-lg-5" href="./assets/index.html">
            <img class="navbar-brand-dark" src="./assets/assets/img/brand/light.svg" alt="MoodleR"> <img
                class="navbar-brand-light" src="./assets/assets/img/brand/dark.svg" alt="Pixel Logo Dark"></a>
        <div class="d-flex align-items-center"><button class="navbar-toggler d-md-none collapsed" type="button"
                data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button></div>
    </nav>
    <div class="container-fluid  bg-soft">
        <div class="row">
            <div class="col-12">
                @if (Auth::user())
                    @section('nav')
                        <nav id="sidebarMenu" class="sidebar d-md-block bg-primary text-white collapse px-2">
                            <div class="sidebar-sticky pt-4 mx-auto">
                                <div
                                    class="user-card d-flex align-items-center justify-content-between justify-content-md-center pb-4">
                                    <div class="d-flex align-items-center">
                                        <div class="user-avatar lg-avatar mr-4">
                                            <a href="{{ route('getProfil') }}"><img
                                                src="{{ asset(Auth::user()->photoProfil()) }}"
                                                alt="{{ Auth::user()->name }}"
                                                class="card-img-top rounded-circle border-white" /></a>
                                            </div>
                                        <div class="d-block">
                                            <h2 class="h6 uname">{{ __('Bonjour,') }} {{ Auth::user()->prenom }}</h2>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                                class="btn btn-secondary btn-xs"><span class="mr-0"><span
                                                        class="fas fa-sign-out-alt"></span></span>
                                                {{ __('Se déconnecter') }}</a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                    <div class="collapse-close d-md-none"><a href="#sidebarMenu" class="fas fa-times"
                                            data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu"
                                            aria-expanded="true" aria-label="Toggle navigation"></a></div>
                                </div>
                                <ul class="nav flex-column mt-4">
                                    <li class="nav-item {{ url()->current()==route('dashbord')?"active":"" }}"><a href="{{ route('dashbord') }}" class="nav-link"><span
                                                class="sidebar-icon"><span class="fas fa-chart-pie"></span></span>
                                            <span>{{ __('Tableau de bord') }}</span></a></li>
                                    <li class="nav-item {{ url()->current()==route('liste-rapport')?"active":"" }}"><a href="{{ route('liste-rapport') }}"
                                            class="nav-link d-flex align-items-center justify-content-between"><span><span
                                                    class="sidebar-icon"><span class="fas fa-folder"></span></span>
                                                {{ __('Rapports') }} </span><span
                                                class="badge badge-md badge-danger badge-pill">4</span></a></li>
                                    <li class="nav-item {{ url()->current()==route('getStatistiques')?"active":"" }}"><a href="{{ route('getStatistiques') }}" class="nav-link"><span
                                                class="sidebar-icon"><span class="fas fa-chart-line"></span></span>
                                            <span>{{ __('Statistiques') }}</span></a></li>
                                    <li class="nav-item {{ url()->current()==route('getHistorique')?"active":"" }}"><a href="{{ route('getHistorique') }}" class="nav-link"><span
                                                class="sidebar-icon"><span class="fas fa-history"></span></span>
                                            <span>{{ __('Historiques') }}</span></a></li>
                                    <li class="nav-item {{ url()->current()==route('getUsers')?"active":"" }}"><a href="{{ route('getUsers') }}" class="nav-link"><span
                                                class="sidebar-icon"><span class="fas fa-users"></span></span>
                                            <span>{{ __('Utilisateurs') }}</span></a></li>
                                    <li class="nav-item {{ url()->current()==route('getConsultation')?"active":"" }}"><a href="{{ route('getConsultation') }}" class="nav-link"><span
                                                class="sidebar-icon"><span class="fas fa-check-double"></span></span>
                                            <span>{{ __('Consultation') }}</span></a></li>
                                    <li class="nav-item {{ url()->current()==route('getParamExport')?"active":"" }}"><a href="{{ route('getParamExport') }}" class="nav-link"><span
                                                class="sidebar-icon"><span class="fas fa-clipboard-list"></span></span>
                                            <span>{{ __("Paramètres d'exportation") }}</span></a></li>
                                    <li class="nav-item {{ url()->current()==route('config')?"active":"" }}"><a href="{{ route('config') }}" class="nav-link"><span
                                                class="sidebar-icon"><span class="fas fa-cog"></span></span>
                                            <span>{{ __('Configurations') }}</span></a></li>
                                    <li class="nav-item {{ url()->current()==route('liste-rapport')?"active":"" }}">
                                        <a class="nav-link collapsed d-flex justify-content-between align-items-center"
                                            href="#submenu-app" data-toggle="collapse"
                                            data-target="#submenu-app"><span><span class="sidebar-icon"><span
                                                        class="fas fa-user-shield mr-2"></span></span>{{ __('Mon compte') }}
                                            </span><span class="link-arrow"><span
                                                    class="fas fa-chevron-right"></span></span></a>
                                        <div class="multi-level collapse" role="list" id="submenu-app"
                                            aria-expanded="false">
                                            <ul class="flex-column nav small">
                                                <li class="nav-item"><a class="nav-link small"
                                                        href="{{ route('getProfil') }}"><span>{{ __('Mon profil') }}</span> </a></li>
                                                <li class="nav-item"><a class="nav-link small"
                                                        href="{{ route('changePassword') }}"><span>{{ __('Changer mot de passe') }}</span></a></li>
                                                <li class="nav-item"><a class="nav-link small"
                                                        href="{{ route('userConfig') }}"><span>{{ __('Préférences') }}</span></a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li role="separator" class="dropdown-divider mt-4 mb-3 border-blue"></li>
                                    <li class="nav-item"><a href="{{ route('getDocs') }}" target="_blank" class="nav-link"><span
                                                class="sidebar-icon"><span class="fas fa-book"></span></span>
                                            <span>{{ __('Documentation ') }}</span> </a></li>
                                    <li class="nav-item"><a href="{{ route('getAvis') }}" target="_blank"
                                            class="nav-link"><span class="sidebar-icon"><span
                                                    class="fas fa-pen"></span></span>
                                            <span>{{ __('Signaler un problème') }}</span></a></li>
                                    <!--li class="nav-item"><a href="https://themes.getbootstrap.com/product/rocket/" target="_blank" class="nav-link"><span class="sidebar-icon"><span class="fas fa-shopping-cart"></span></span> <span>Buy now</span></a></li>
                          <li class="nav-item"><a href="https://themesberg.com" target="_blank" class="nav-link d-flex align-items-center"><span class="sidebar-icon"><img src="./assets/assets/img/themesberg.svg" height="20" width="20" class="mr-1" alt="Themesberg Logo"> </span><span>Themesberg</span></a></li-->
                                </ul>
                            </div>
                        </nav>
                    @show
                @endif
                <div class="@yield('ccontent','content')">
                    @yield('content')
                    <hr>
                    <footer class="footer section py-2 mb-4">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 col-lg-6 mb-4 mb-lg-0">
                                    <p class="mb-0 text-center text-xl-left">Copyright © {{ date('Y') }} <a class="ml-2 text-primary font-weight-normal"
                                            href="{{ url('/') }}" target="_blank">{{ __('MoodleR') }}</a></p>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <ul
                                        class="list-inline list-group-flush list-group-borderless text-center text-xl-right mb-0">
                                        <li class="list-inline-item px-0 px-sm-2"><a
                                                href="{{ route("getApropos") }}">{{ __("A propos") }}</a></li>

                                        <li class="list-inline-item px-0 px-sm-2"><a
                                                href="{{ route("getDocs") }}">{{ __("Documentation") }}</a></li>

                                        <li class="list-inline-item px-0 px-sm-2"><a
                                          href="{{ route("blog.index") }}">{{ __("Blog") }}</a></li>
                                        <li class="list-inline-item px-0 px-sm-2"><a
                                                href="{{ route('getContact') }}">{{ __("Contact") }}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>

        </div>

    </div>

    <script data-require="jquery@*" data-semver="2.1.4" src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <script src="{{ asset('./storage/lib/popper.js/js/popper.js') }}"></script>
    <script src="{{ asset('./storage/lib/pjax/pjax.js') }}"></script>
    <script src="{{ asset('./storage/lib/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('./storage/vendor/headroom.js/dist/headroom.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/countup.js/dist/countUp.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/jquery-countdown/dist/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/prismjs/prism.js') }}"></script>
    <script src="{{ asset('./storage/vendor/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}">
    </script>
    <script src="{{ asset('./storage/vendor/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('./storage/vendor/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('./storage/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
    <script src="{{ asset('./storage/assets/assets/js/rocket.js') }}"></script>
    <script>
        /* const ps = new PerfectScrollbar('#sidebarMenu', {
         wheelSpeed: 2,
         wheelPropagation: true,
         minScrollbarLength: 20
         });*/
        if ($.fn.perfectScrollbar) {
            // alert('')
            $('div.sidebar-sticky').perfectScrollbar({
                suppressScrollX: true
            });
            $(document).pjax('a', 'body')
        }

    </script>
    @yield('scripts')

</body>

</html>
