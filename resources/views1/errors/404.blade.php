@extends('layouts.app')
@section('title', __("Ereur 404, cette page n'existe pas"))
@section('nav',"")

@section('content')
<section class="vh-100 d-flex align-items-center justify-content-center">
    <div class="container">
       <div class="row">
          <div class="col-12 text-center d-flex align-items-center justify-content-center">
             <div>
                <a href="{{ url('/') }}"><img class="img-fluid w-75" src="{{ asset("storage/moodler/img/illustrations/404.svg")}}" alt="404 not found"></a>
                <h1 class="mt-5 display-2">{{ __("Page non") }} <span class="font-weight-bolder text-primary">{{ __('trouvé') }}</span></h1>
                <p class="lead mt-1">{{ __("Oops! le lien que vous essayez de joindre n'existe pas ou a été déplacé. ") }}.</p>
                <p><div class="text-muted mt-3">{{ __("Si le problème persiste, veuillez contacter l'administrateur du système.") }}</div></p>
                <a class="btn btn-primary animate-hover" href="{{ url('/') }}"><i class="fas fa-chevron-left mr-3 pl-2 animate-left-3"></i>Retour à l'accueil</a>
             </div>
          </div>
       </div>
    </div>
 </section>
@endsection