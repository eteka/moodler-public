@extends('layouts.app')
@section('title', __("Ereur 500, le serveur ne répond pas"))
@section('nav',"")

@section('content')
 <section class="vh-100 d-flex align-items-center justify-content-center">
    <div class="container">
       <div class="row">
          <div class="col-12 col-lg-6 order-2 order-lg-1 text-center text-lg-left">
             <h1 class="mt-5">{{ __('Quelque chose') }} <span class="text-primary">{{ __("d'important") }}</span> {{ __("s'est mal passé") }}</h1>
             <p class="lead my-4">{{ __("Veuillez attendre quelques instants et réssayer. Nous travaillons à corriger l'erreur survenue.") }}</p>
             <a class="btn btn-primary animate-hover" href="{{ url('/') }}"><i class="fas fa-chevron-left mr-3 pl-2 animate-left-3"></i>{{ __("Aller à l'accueil") }}</a>
          </div>
          <div class="col-12 col-lg-6 order-1 order-lg-2 text-center d-flex align-items-center justify-content-center"><img class="img-fluid w-75" src="{{ asset("storage/moodler/img/illustrations/500.svg")}}" alt="500 Server Error"></div>
       </div>
    </div>
 </section>
@endsection