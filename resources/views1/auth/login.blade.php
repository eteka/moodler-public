@extends('layouts.app')
@section('title',__("Se conncecter"))
@section('nav',__(""))
@section('ccontent', '')

@section('content')
    <div class="bg-soft">
    <section class="vh-100 bg-soft d-flex align-items-center">
        <div class="container">
        <div class="row justify-content-center form-bg-image" data-background="../assets/img/illustrations/signin.svg" style="background-image: url(&quot;../assets/img/illustrations/signin.svg&quot;);">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="signin-inner mt-3 mt-lg-0 bg-white shadow-soft border border-light rounded p-4 p-lg-5 w-100 fmxw-400">
                    <div class="text-center text-md-center mb-4 mt-md-0">
                        <h1 class=""><span class="text-primary">Moodle</span><span class="text-danger">R</span></h1>
                    <h3 class="mb-1 h5">{{ __('Se connecter à votre compte') }}</h3>
                    <p class="text-gray">{{ __("Veuillez saisir vos identifiants de connexion pour continuer") }}</p>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text"><i class="far fa-user"></i></span></div>
                            <input type="email"  autocomplete="off" id="email" placeholder="{{__('Entrez votre adresse email')}}" type="email" class="form-control @error('password') is-invalid @enderror" value="{{old('email')}}" name="email" required autocomplete="current-email" required="">
                        </div>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-unlock-alt"></i></span></div>
                            <input  id="password" type="password" placeholder='{{__("Entrez votre mot de passe")}}' class="form-control @error('password') is-invalid @enderror" name="password" required>
                            <div class="input-group-append"><span class="input-group-text"><i class="far fa-eye"></i></span></div>
                        </div>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        <div class="d-block d-sm-flex justify-content-between align-items-center mt-2">
                            <div class="form-group form-check mt-3"><input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">
                                    {{ __('Rester connecté') }}
                                </label>
                                </div>
                            <div>
                                @if (Route::has('password.request'))
                                <a class="small text-right" href="{{ route('password.request') }}">
                                    {{ __('Mot de passe oublié ?') }}
                                </a>
                            @endif  
                            </div>
                        </div>
                    </div>
                    <div class="mt-3"><button type="submit" class="btn btn-block btn-primary">{{ __("Se connecter") }}</button></div>
                    </form>
                    <div class="mt-3 mb-4 text-center">
                        
                    </div>
                    <div class="d-block d-sm-flex justify-content-center align-items-center mt-4">
                        <span class="font-weight-normal">
                        {{ __('Nouveau ?,') }}<a href="{{url('register')}}"> {{ __('Créer un compte') }}</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    </div>

@endsection
