@extends('layouts.app')
@section('title', 'Gérez mieux vos données sur Moodle')
@section('ccontent', '')

@section('nav')

@endsection
@section('content')
<section class="h-100vh bg-soft  d-flex align-items-center justify-content-center">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12 offset-lg-1 col-lg-4 mt-5 order-2 order-lg-1 text-center text-lg-left">
            <h1 class="display-1 mt-3"><span class="text-primary">Moodle</span><span class="text-danger">R</span></h1>
            <h1 class="mt-3">{!! __('Bienvenue dans votre <span class="text-primary">outil</span> de suivi du  <span class="text-danger">tutorat</span> pour <b>Moodle</b>.') !!} </h1>
            <p class="leads font-weight-light font-smaller my-4">{{ __("Ne perdez plus du temps à gérer manuelement vos rapports de suivi des formations sur la plateforme Moodle . Voici l'outil qui vous fera gagner en productivité.")}}</p>
            @guest
               <a class="mr-3 mb-1 mt-1 bold btn btn-primary btn-lg animate-hover" href="{{ route('login') }}"><i class="fa fa-sign-in-alt mr-3 pl-2 animate-left-3"></i>{{ __('Se connecter') }}</a>
               @else
               <a class="mr-3 bold btn btn-primary mb-1 mt-1 btn-lg animate-hover" href="{{ route('dashbord') }}"><i class="fas fa-tachometer-alt"></i> {{ __('Tableau de bord') }}</a>
            @endguest
            <a class="btn btn-danger btn-lg animate-hover" href="{{ route('login') }}"><i class="fas fa-book-open mr-3 pl-2 animate-left-3"></i>Documentation</a>

            {{-- <a class="ml-3 bold" href="{{ route('login') }}">{{ __('Créer un compte') }}</a> --}}
         </div>
         <div class="col-12 col-lg-7 order-1 order-lg-2  text-center d-flex align-items-center mt-4 justify-content-center">
            <img class="img-fluid w-80" src="{{ asset('storage/moodler/img/home-app.png') }}" alt="{{ __("Application de gestion de suivi des formations à distance") }}"></div>
      </div>

   </div>
</section>
@endsection
