<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    <label for="nom" class="control-label">{{ 'Nom' }}</label>
    <input class="form-control" name="nom" type="text" id="nom" value="{{ isset($rapport->nom) ? $rapport->nom : ''}}" >
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Type' }}</label>
    <input class="form-control" name="type" type="text" id="type" value="{{ isset($rapport->type) ? $rapport->type : ''}}" >
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date_debut') ? 'has-error' : ''}}">
    <label for="date_debut" class="control-label">{{ 'Date Debut' }}</label>
    <input class="form-control" name="date_debut" type="datetime-local" id="date_debut" value="{{ isset($rapport->date_debut) ? $rapport->date_debut : ''}}" >
    {!! $errors->first('date_debut', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date_fin') ? 'has-error' : ''}}">
    <label for="date_fin" class="control-label">{{ 'Date Fin' }}</label>
    <input class="form-control" name="date_fin" type="datetime-local" id="date_fin" value="{{ isset($rapport->date_fin) ? $rapport->date_fin : ''}}" >
    {!! $errors->first('date_fin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($rapport->description) ? $rapport->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
