<div class="form-group {{ $errors->has('lang_default') ? 'has-error' : ''}}">
    <label for="lang_default" class="control-label">{{ 'Lang Default' }}</label>
    <input class="form-control" name="lang_default" type="text" id="lang_default" value="{{ isset($preference->lang_default) ? $preference->lang_default : ''}}" >
    {!! $errors->first('lang_default', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nb_affichage') ? 'has-error' : ''}}">
    <label for="nb_affichage" class="control-label">{{ 'Nb Affichage' }}</label>
    <input class="form-control" name="nb_affichage" type="number" id="nb_affichage" value="{{ isset($preference->nb_affichage) ? $preference->nb_affichage : ''}}" >
    {!! $errors->first('nb_affichage', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('format_default') ? 'has-error' : ''}}">
    <label for="format_default" class="control-label">{{ 'Format Default' }}</label>
    <input class="form-control" name="format_default" type="text" id="format_default" value="{{ isset($preference->format_default) ? $preference->format_default : ''}}" >
    {!! $errors->first('format_default', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nom_default') ? 'has-error' : ''}}">
    <label for="nom_default" class="control-label">{{ 'Nom Default' }}</label>
    <input class="form-control" name="nom_default" type="text" id="nom_default" value="{{ isset($preference->nom_default) ? $preference->nom_default : ''}}" >
    {!! $errors->first('nom_default', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
